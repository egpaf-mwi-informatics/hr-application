from flask import Flask
from flask import request
from ldap3 import Server, Connection, SAFE_SYNC
import os

app = Flask(__name__)


@app.route('/', methods=['POST'])
def login():
    try:
        if request.method == 'POST':
            email = request.form['email']
            password = request.form['password']
            username, _ = email.split('@', 1)
            username = r'PEDAIDS\{}'.format(username)
            server = Server('192.168.100.4')
            conn = Connection(server, user=username, password=password, client_strategy=SAFE_SYNC)
            conn.bind()
            status = conn.bound
            # status, result, response, _ = conn.search('ou=Pedaids LLW USERS,dc=pedaids,dc=local', '(ou=Ext*)')
            return str(status)
        else:
            return 'False'
    except:
        return 'False'


if __name__ == "__main__":
    app.run(debug=True)
