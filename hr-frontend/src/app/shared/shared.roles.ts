export const IT_ROLES = ['Associate IT Officer'];
export const ADMIN_ROLES = [
	'Informatics / Geospatial Manager',
	'Informatics / Geospatial Officer',
	'Data / Informatics Analyst',
	'Database Manager',
];

export const HR_ROLES = [
	'Senior HR & Administration Officer',
	'HR & Administration Officer',
	'Associate HR & Administration Officer',
	'Senior HR & Administration Manager',
];
