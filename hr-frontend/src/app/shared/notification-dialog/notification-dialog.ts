import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
// import { NotificationDialogComponent } from './notification-dialog.component';
export type NotificationData = {
	message: string;
	title: string;
};
@Component({
	selector: 'app-notification-dialog',
	templateUrl: './notification-dialog.html',
	styleUrls: ['./notification-dialog.css'],
})
export class NotificationDialog {
	constructor(
		public dialogRef: MatDialogRef<NotificationDialog>,
		@Inject(MAT_DIALOG_DATA) public data: NotificationData
	) {}

	onNoClick(): void {
		this.dialogRef.close();
	}
}
