import { Injectable } from '@angular/core';
import {
	// ActivatedRouteSnapshot,
	CanActivate,
	// RouterStateSnapshot,
	UrlTree,
} from '@angular/router';
import { Observable, of } from 'rxjs';
import { catchError, map, switchMap, takeUntil, tap } from 'rxjs/operators';
import { User } from 'src/app/interfaces/main.interfaces';
import { ApiService } from 'src/app/services/api.service';
import { DataSharingService } from 'src/app/services/data-sharing.service';
import { DestroyService } from 'src/app/services/destroy.service';

@Injectable({
	providedIn: 'root',
})
export class HrGuard implements CanActivate {
	user: User;
	constructor(
		private dataSharingService: DataSharingService,
		private apiService: ApiService,
		private readonly destroy$: DestroyService
	) {
		this.dataSharingService.authUser$
			.pipe(takeUntil(this.destroy$))
			.subscribe((user) => (this.user = user));
	}
	canActivate(
		// route: ActivatedRouteSnapshot,
		// state: RouterStateSnapshot
	):
		| Observable<boolean | UrlTree>
		| Promise<boolean | UrlTree>
		| boolean
		| UrlTree {
		let userRole = this.apiService.getUserRole(this.user);

		if (userRole) {
			if (userRole === 'hrAccess' || userRole === 'adminAccess') {
				return true;
			} else {
				return false;
			}
		} else {
			return this.apiService
				.onRefresh(localStorage.getItem('associateId'))
				.pipe(
					switchMap(() =>
						this.dataSharingService.authUser$.pipe(
							tap((user) => (this.user = user))
						)
					),
					takeUntil(this.destroy$),
					map((user) => {
						userRole = this.apiService.getUserRole(user);
						if (userRole === 'hrAccess' || userRole === 'adminAccess') {
							return true;
						} else {
							return false;
						}
					}),
					catchError(() => {
						this.apiService.logout();
						return of(false);
					})
				);
		}
	}
}
