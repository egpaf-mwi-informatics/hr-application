import { Injectable } from '@angular/core';
import {
	// ActivatedRouteSnapshot,
	CanActivate,
	Router,
	// RouterStateSnapshot,
	UrlTree,
} from '@angular/router';
import { Observable, of } from 'rxjs';
import {
	switchMap,
	take,
	tap,
	map,
	catchError,
	takeUntil,
} from 'rxjs/operators';
import { User } from 'src/app/interfaces/main.interfaces';
import { ApiService } from 'src/app/services/api.service';
import { DataSharingService } from 'src/app/services/data-sharing.service';
import { DestroyService } from 'src/app/services/destroy.service';

@Injectable({
	providedIn: 'root',
})
export class AuthGuard implements CanActivate {
	user: User;
	constructor(
		private router: Router,
		private apiService: ApiService,
		private dataSharingService: DataSharingService,
		private readonly destroy$: DestroyService
	) {
		this.dataSharingService.authUser$
			.pipe(takeUntil(this.destroy$))
			.subscribe((user) => (this.user = user));
	}
	canActivate(
		// route: ActivatedRouteSnapshot,
		// state: RouterStateSnapshot
	):
		| Observable<boolean | UrlTree>
		| Promise<boolean | UrlTree>
		| boolean
		| UrlTree {
		let userRole = this.apiService.getUserRole(this.user);
		if (userRole) {
			if (['adminAccess', 'itAccess', 'hrAccess'].includes(userRole)) {
				return true;
			} else {
				this.router.navigate(['/']);
				return false;
			}
		} else if (this.user?.isSupervisor) {
			return this.user.isSupervisor;
		} else {
			return this.apiService
				.onRefresh(localStorage.getItem('associateId'))
				.pipe(
					switchMap(() =>
						this.dataSharingService.authUser$.pipe(
							tap((user) => (this.user = user))
						)
					),
					takeUntil(this.destroy$),
					map((user) => {
						userRole = this.apiService.getUserRole(user);
						if (
							['adminAccess', 'itAccess', 'hrAccess'].includes(userRole) ||
							user.isSupervisor
						) {
							return true;
						} else {
							return false;
						}
					}),
					take(1),
					catchError(() => {
						this.apiService.logout();
						return of(false);
					})
				);
		}
	}
}
