import { TestBed } from '@angular/core/testing';

import { ItGuard } from './it.guard';

describe('ItGuard', () => {
  let guard: ItGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    guard = TestBed.inject(ItGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});
