import { Injectable } from '@angular/core';
import {
	ActivatedRouteSnapshot,
	CanActivate,
	RouterStateSnapshot,
	UrlTree,
} from '@angular/router';
import { Observable } from 'rxjs';
import { User } from 'src/app/interfaces/main.interfaces';
import { ApiService } from 'src/app/services/api.service';
import { DataSharingService } from 'src/app/services/data-sharing.service';


@Injectable({
	providedIn: 'root',
})
export class AdminGuard implements CanActivate {
	user: User;
	constructor(
		private apiService: ApiService,
		private dataSharingService: DataSharingService
	) {
		this.dataSharingService.authUser$.subscribe((user) => (this.user = user));
	}
	canActivate(
		route: ActivatedRouteSnapshot,
		state: RouterStateSnapshot
	):
		| Observable<boolean | UrlTree>
		| Promise<boolean | UrlTree>
		| boolean
		| UrlTree {
		const userRole = this.apiService.getUserRole(this.user);
		if (userRole === 'adminAccess') {
			return true;
		} else {
			return false;
		}
	}
}
