import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatCardModule } from '@angular/material/card';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { MatInputModule } from '@angular/material/input';
import { ReactiveFormsModule } from '@angular/forms';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatSelectModule } from '@angular/material/select';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatTableModule } from '@angular/material/table';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { NotificationDialog } from './notification-dialog/notification-dialog';
import { MatDialogModule } from '@angular/material/dialog';
import { MatButtonToggleModule } from '@angular/material/button-toggle';

@NgModule({
	imports: [CommonModule, MatButtonModule, MatDialogModule, MatCardModule],
	exports: [
		MatCardModule,
		MatToolbarModule,
		MatButtonModule,
		MatFormFieldModule,
		MatIconModule,
		MatListModule,
		MatInputModule,
		ReactiveFormsModule,
		MatFormFieldModule,
		FlexLayoutModule,
		MatIconModule,
		MatSelectModule,
		MatAutocompleteModule,
		MatTableModule,
		MatPaginatorModule,
		MatListModule,
		MatProgressSpinnerModule,
		MatDatepickerModule,
		MatSnackBarModule,
		MatButtonToggleModule,
	],
	declarations: [NotificationDialog],
})
export class SharedModule {}
