import { Injectable } from '@angular/core';
import {
	Resolve,
	RouterStateSnapshot,
	ActivatedRouteSnapshot,
} from '@angular/router';
import { Observable } from 'rxjs';
import { Employee } from 'src/app/interfaces/main.interfaces';
import { ApiService } from 'src/app/services/api.service';

@Injectable({
	providedIn: 'root',
})
export class PipListDataResolver implements Resolve<Employee[]> {
	constructor(private apiService: ApiService) {}
	resolve(
		route: ActivatedRouteSnapshot,
		state: RouterStateSnapshot
	): Observable<Employee[]> {
		return this.apiService.findEmployeesOnPip(true);
	}
}
