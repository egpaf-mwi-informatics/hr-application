import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { Observable } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { Supervisor, User } from 'src/app/interfaces/main.interfaces';
import { ApiService } from 'src/app/services/api.service';
import { DataSharingService } from 'src/app/services/data-sharing.service';
import { DestroyService } from 'src/app/services/destroy.service';

@Injectable({
	providedIn: 'root',
})
export class SubordinatesResolver implements Resolve<Supervisor> {
	private user: User;
	constructor(
		private readonly destroy$: DestroyService,
		private apiService: ApiService,
		private dataSharingService: DataSharingService
	) {
		this.dataSharingService.authUser$
			.pipe(takeUntil(this.destroy$))
			.subscribe((user) => (this.user = user));
	}
	resolve(): Observable<Supervisor> {
		return this.apiService.getSupervisedEmployees(this.user.positionId);
	}
}
