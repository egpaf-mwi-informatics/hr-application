import { TestBed } from '@angular/core/testing';

import { PipListDataResolver } from './pip-list-data.resolver';

describe('PipListDataResolver', () => {
  let resolver: PipListDataResolver;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    resolver = TestBed.inject(PipListDataResolver);
  });

  it('should be created', () => {
    expect(resolver).toBeTruthy();
  });
});
