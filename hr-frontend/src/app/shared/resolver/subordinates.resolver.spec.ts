import { TestBed } from '@angular/core/testing';

import { SubordinatesResolver } from './subordinates.resolver';

describe('SubordinatesResolver', () => {
  let resolver: SubordinatesResolver;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    resolver = TestBed.inject(SubordinatesResolver);
  });

  it('should be created', () => {
    expect(resolver).toBeTruthy();
  });
});
