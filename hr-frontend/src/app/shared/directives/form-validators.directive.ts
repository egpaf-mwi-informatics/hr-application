import { AbstractControl, ValidationErrors, ValidatorFn } from '@angular/forms';

export function requireSelection(): ValidatorFn {
	return (control: AbstractControl): ValidationErrors | null => {
		const disallowed = typeof control.value === 'string';
		return disallowed ? { notAllowed: { value: control.value } } : null;
	};
}
