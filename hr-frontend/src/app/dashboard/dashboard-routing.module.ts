import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from '../shared/guards/auth.guard';
import { HrGuard } from '../shared/guards/hr.guard';
import { DashboardContainerComponent } from './dashboard-container/dashboard-container.component';
import { DashboardVizComponent } from './dashboard-viz/dashboard-viz.component';
import { DataTableComponent } from './data-table/data-table.component';

const routes: Routes = [
	{
		path: '',
		component: DashboardContainerComponent,
		children: [
			{ path: '', redirectTo: 'viz', pathMatch: 'full' },
			{
				path: 'table',
				component: DataTableComponent,
				canActivate: [AuthGuard, HrGuard],
			},
			{
				path: 'viz',
				component: DashboardVizComponent,
				canActivate: [AuthGuard, HrGuard],
			},
		],
	},
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule],
})
export class DashboardRoutingModule {}
