import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DataTableComponent } from './data-table/data-table.component';
import { DashboardContainerComponent } from './dashboard-container/dashboard-container.component';
import { DashboardVizComponent } from './dashboard-viz/dashboard-viz.component';
import { DashboardRoutingModule } from './dashboard-routing.module';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { SharedModule } from '../shared/shared.module';
import { NgApexchartsModule } from 'ng-apexcharts';
import { MatTableExporterModule } from 'mat-table-exporter';
import { MatSortModule } from '@angular/material/sort';

@NgModule({
  declarations: [
    DataTableComponent,
    DashboardContainerComponent,
    DashboardVizComponent,
  ],
  imports: [
    CommonModule,
    SharedModule,
    MatGridListModule,
    MatSlideToggleModule,
    DashboardRoutingModule,
    NgApexchartsModule,
    MatTableExporterModule,
    MatSortModule
  ],
})
export class DashboardModule {}
