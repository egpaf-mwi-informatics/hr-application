import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DashboardVizComponent } from './dashboard-viz.component';

describe('DashboardVizComponent', () => {
  let component: DashboardVizComponent;
  let fixture: ComponentFixture<DashboardVizComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DashboardVizComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DashboardVizComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
