import { Component, OnInit, ViewChild } from '@angular/core';
import { ChartComponent } from 'ng-apexcharts';
import { takeUntil, tap } from 'rxjs/operators';
import { ApiService } from 'src/app/services/api.service';
import {
	EmpPerDeptOptions,
	EmpPerDistOptions,
	GenderOptions,
	EndingContractOptions,
	EmpPerDistData,
	EmpPerDisPerDepOptions,
	EmpPerDisPerDepData,
	IData,
} from 'src/app/interfaces/chart.interfaces';
import { DestroyService } from 'src/app/services/destroy.service';

@Component({
	selector: 'app-dashboard-viz',
	templateUrl: './dashboard-viz.component.html',
	styleUrls: ['./dashboard-viz.component.css'],
})
export class DashboardVizComponent implements OnInit {
	// Number of employess per Department
	@ViewChild('empPerDepChart') empPerDepChart: ChartComponent;
	public empPerDepOptions: Partial<EmpPerDeptOptions>;
	public empPerDeptData: IData = { count: [], labels: [] };

	// Ending Contracts per department
	@ViewChild('endingContractsChart') endingContractsChart: ChartComponent;
	public endingContractsOptions: Partial<EndingContractOptions>;
	public endingContractsData: IData = { count: [], labels: [] };

	// Number of employees per district
	@ViewChild('empPerDistChart') empPerDistChart: ChartComponent;
	public empPerDistOptions: Partial<EmpPerDistOptions>;
	public empPerDistData: EmpPerDistData = { data: [] };

	// Gender per department
	@ViewChild('genderChart') genderChart: ChartComponent;
	public genderOptions: Partial<GenderOptions>;
	public genderData: IData = { count: [], labels: [] };

	@ViewChild('empPerDisPerDepChart') empPerDisPerDepChart: ChartComponent;
	public empPerDisPerDepOptions: Partial<EmpPerDisPerDepOptions>;
	public empPerDisPerDepData: Partial<EmpPerDisPerDepData>;

	constructor(
		private apiService: ApiService,
		private readonly destroy$: DestroyService
	) {}

	ngOnInit(): void {
		this.apiService
			.endingContractsApi()
			.pipe(
				tap((data) => {
					this.endingContractsData.count = data.count;
					this.endingContractsData.labels = data.labels;
				}),
				takeUntil(this.destroy$)
			)
			.subscribe(() => {
				this.endingContractsInit();
			});

		this.apiService
			.employeesPerDeptApi()
			.pipe(
				tap((data) => {
					this.empPerDeptData.count = data.count;
					this.empPerDeptData.labels = data.labels;
				}),
				takeUntil(this.destroy$)
			)
			.subscribe(() => this.empPerDeptInit());

		this.apiService
			.employeesPerDistrictApi()
			.pipe(
				tap((data) => {
					this.empPerDistData = data;
					// this.empPerDistData.count = data.count;
					// this.empPerDistData.labels = data.labels;
				}),
				takeUntil(this.destroy$)
			)
			.subscribe(() => this.empPerDistInit());

		// To-Do: When the data is available
		this.apiService
			.genderPerDeptApi()
			.pipe(
				tap((data) => {
					this.genderData.count = data.count;
					this.genderData.labels = data.labels;
				}),
				takeUntil(this.destroy$)
			)
			.subscribe(() => this.genderInit());

		this.apiService
			.employeesPerDisPerDeptApi()
			.pipe(
				tap((data) => {
					this.empPerDisPerDepData = data;
				}),
				takeUntil(this.destroy$)
			)
			.subscribe(() => this.empPerDisPerDeptInit());
	}

	private empPerDeptInit(): void {
		this.empPerDepOptions = {
			chart: {
				type: 'bar',
				height: 400,
				animations: {
					enabled: true,
					easing: 'easeout',
					speed: 800,
					animateGradually: { enabled: true, delay: 150 },
					dynamicAnimation: { enabled: true, speed: 350 },
				},
			},
			title: {
				text: 'Number of employees per department',
				style: {
					fontSize: '16px',
				},
			},
			colors: ['#994d00', '#994d00', '#994d00'],
			dataLabels: { style: { colors: ['#fff', '#fff', '#fff'] } },
			series: [
				{ name: 'Number of employees', data: this.empPerDeptData.count },
			],
			xaxis: { categories: this.empPerDeptData.labels },
		};
	}

	private genderInit(): void {
		this.genderOptions = {
			series: this.genderData.count.map((x) => +x),
			chart: {
				type: 'donut',
				height: '420',
			},
			labels: this.genderData.labels,
			legend: {
				position: 'bottom',
			},
			title: {
				text: 'Employee gender distribution',
				style: {
					fontSize: '16px',
				},
				align: 'center',
			},
			responsive: [
				{
					breakpoint: 480,
					options: {
						chart: {
							width: 200,
						},
						legend: {
							position: 'bottom',
						},
					},
				},
			],
		};
	}

	// Employees per district
	private empPerDistInit(): void {
		this.empPerDistOptions = {
			chart: { height: 400, type: 'treemap' },
			dataLabels:{enabled:true, },
			series: [{ data: this.empPerDistData.data }],
			legend: { show: false },
			title: {
				text: 'Number of employees Per District',
				align: 'left',
				style: {
					fontSize: '16px',
				},
			},
			plotOptions: { treemap: { distributed: true, enableShades: true } },
		};
	}

	private endingContractsInit(): void {
		this.endingContractsOptions = {
			series: [
				{
					name: 'Employees',
					data: this.endingContractsData.count,
				},
			],
			chart: {
				type: 'bar',

				height: 400,
				animations: {
					enabled: true,
					easing: 'easeinout',
					speed: 800,
					animateGradually: {
						enabled: true,
						delay: 150,
					},
					dynamicAnimation: {
						enabled: true,
						speed: 350,
					},
				},
			},
			plotOptions: {
				bar: {
					horizontal: true,
					borderRadius: 4,
				},
			},
			dataLabels: {
				enabled: true,
			},
			xaxis: {
				categories: this.endingContractsData.labels,
			},

			title: {
				text: 'Contracts ending in 30 days per department',
				style: {
					fontSize: '16px',
				},
			},
		};
	}

	private empPerDisPerDeptInit(): void {
		this.empPerDisPerDepOptions = {
			series: this.empPerDisPerDepData.series,
			chart: {
				type: 'bar',
				height: 1080,
				stacked: true,
			},
			plotOptions: {
				bar: {
					horizontal: true,
				},
			},
			stroke: {
				width: 1,
				colors: ['#fff'],
			},
			xaxis: {
				categories: this.empPerDisPerDepData.categories,
			},
			yaxis: {
				title: {
					text: undefined,
				},
			},
			title: {
				text: 'Number of employees per district in each department',
				style: {
					fontSize: '16px',
				},
			},
			fill: {
				opacity: 1,
			},
			dataLabels: {
				textAnchor: 'middle',
				formatter: function (val) {
					if (val === 0) {
						return;
					}
					return +val;
				},
			},
			legend: {
				position: 'right',
				horizontalAlign: 'left',
				offsetY: 30,
			},
		};
	}
}
