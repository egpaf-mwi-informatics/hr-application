import {
  AfterViewInit,
  Component,
  OnDestroy,
  OnInit,
  ViewChild,
} from '@angular/core';
import { FormBuilder, FormGroup, NgForm } from '@angular/forms';
import { MatTableDataSource } from '@angular/material/table';
import { Subscription } from 'rxjs';
import { tap } from 'rxjs/operators';
import { ApiService } from 'src/app/services/api.service';
import { DataSharingService } from 'src/app/services/data-sharing.service';
import * as moment from 'moment';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort, Sort } from '@angular/material/sort';
import { MatSelectChange } from '@angular/material/select';
import { MatDialog } from '@angular/material/dialog';
import { NotificationDialog } from 'src/app/shared/notification-dialog/notification-dialog';

@Component({
  selector: 'app-data-table',
  templateUrl: './data-table.component.html',
  styleUrls: ['./data-table.component.css'],
})
export class DataTableComponent implements OnInit, AfterViewInit, OnDestroy {
  displayedColumns: string[] = [
    'firstName',
    'lastName',
    'sex',
    'email',
    'startDate',
    'endDate',
    'positionName',
    'departmentName',
    'officeName',
  ];
  filterOptions = [
    { value: 'onProbation', label: 'On Probation' },
    { value: 'onPip', label: 'On PIP' },
    { value: 'newHire', label: 'New Hire' },
    { value: 'terminatedContracts', label: 'Terminated Contracts' },
  ];
  dataSource = new MatTableDataSource();
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  tableFiltersForm: FormGroup;
  initSub: Subscription;

  // Toggle properties
  datePickerDisabled = false;

  constructor(
    private formBuilder: FormBuilder,
    private apiService: ApiService,
    private dataSharingService: DataSharingService,
    private dialog: MatDialog
  ) {
    this.tableFiltersForm = this.formBuilder.group({
      filterOption: [{ value: '' }],
      startDate: [{ value: '', disabled: true }],
      endDate: [{ value: '', disabled: true }],
    });
  }

  ngOnInit(): void {
    this.dataSource.filterPredicate = (data, filter: string) => {
      const accumulator = (currentTerm, key) => {
        return this.nestedFilterCheck(currentTerm, data, key);
      };
      const dataStr = Object.keys(data).reduce(accumulator, '').toLowerCase();
      // Transform the filter by converting it to lowercase and removing whitespace.
      const transformedFilter = filter.trim().toLowerCase();
      return dataStr.indexOf(transformedFilter) !== -1;
    };
  }

  nestedFilterCheck(search, data, key) {
    if (typeof data[key] === 'object') {
      for (const k in data[key]) {
        if (data[key][k] !== null) {
          search = this.nestedFilterCheck(search, data[key], k);
        }
      }
    } else {
      search += data[key];
    }
    return search;
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  filteredOption(key: MatSelectChange) {
    switch (key.value) {
      case 'onPip':
      case 'onProbation':
        this.datePickerDisabled = true;
        break;
      default:
        this.datePickerDisabled = false;
        break;
    }
  }

  public get f() {
    return this.tableFiltersForm.controls;
  }

  clearFilters(filterForm: NgForm) {
    this.tableFiltersForm.reset();
    filterForm.resetForm();
    this.datePickerDisabled = true;
    this.f.onProbation.enable();
    this.dataSource.disconnect();
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
  getData() {
    this.dataSource.connect();
    const selectedOption = this.f.filterOption.value;
    const isDateSet = moment(this.f.startDate.value).isValid();

    if (
      !isDateSet &&
      (selectedOption === 'newHire' || selectedOption === 'terminatedContracts')
    ) {
      this.openDialog();
    } else {
      switch (selectedOption) {
        case 'onPip':
          this.initSub = this.apiService
            .findEmployeesOnPip(true)
            .pipe(
              tap((employees) =>
                this.dataSharingService.setEmployees(employees)
              )
            )
            .subscribe((employees) => (this.dataSource.data = employees));
          break;
        case 'newHire':
        case 'terminatedContracts':
        case 'onProbation':
          const filters = {
            startDate: moment(this.f.startDate.value).isValid()
              ? moment(this.f.startDate.value).format('YYYY-MM-DD')
              : '',
            endDate: moment(this.f.endDate.value).isValid()
              ? moment(this.f.endDate.value).format('YYYY-MM-DD')
              : '',
            onProbation: (() => {
              if (this.f.filterOption.value === 'onProbation') {
                return true;
              } else {
                return false;
              }
            })(),
            newHire: (() => {
              if (this.f.filterOption.value === 'newHire') {
                return true;
              } else {
                return false;
              }
            })(),
            onPip: false,
            contractsTerminated: (() => {
              if (this.f.filterOption.value === 'terminatedContracts') {
                return true;
              } else {
                return false;
              }
            })(),
          };

          this.initSub = this.apiService
            .findEmployees(filters)
            .pipe(
              tap((employees) =>
                this.dataSharingService.setEmployees(employees)
              )
            )
            .subscribe((employees) => (this.dataSource.data = employees));
          break;

        default:
          const timeframeSet = moment(this.f.startDate.value).isValid();
          if (!timeframeSet) {
            this.initSub = this.apiService
              .findAllEmployees()
              .pipe(
                tap((employees) =>
                  this.dataSharingService.setEmployees(employees)
                )
              )
              .subscribe((employees) => (this.dataSource.data = employees));
          } else {
            const timeFilters = {
              startDate: moment(this.f.startDate.value).format('YYYY-MM-DD'),
              endDate: moment(this.f.endDate.value).format('YYYY-MM-DD'),
            };
            this.initSub = this.apiService
              .findEmployees(timeFilters)
              .pipe(
                tap((employees) =>
                  this.dataSharingService.setEmployees(employees)
                )
              )
              .subscribe((employees) => (this.dataSource.data = employees));
          }

          break;
      }
    }
  }


  private openDialog(): void {
    this.dialog.open(NotificationDialog, {
      width: '20rem',
      data: {
        title: 'Required Fields',
        message:
          'Selecting a filter for New Hire or Terminated Contracts requires selecting a duration for those filters',
      },
    });
  }

  ngOnDestroy(): void {
    if (!!this.initSub) {
      this.initSub.unsubscribe();
    }
  }
}
