
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from '../shared/guards/auth.guard';
import { ItGuard } from '../shared/guards/it.guard';
import { EmailGroupComponent } from './email-group/email-group.component';
import { ItAdminComponent } from './it-admin/it-admin.component';
import { ItContainerComponent } from './it-container/it-container.component';

const routes: Routes = [
	{
		path: '',
		component: ItContainerComponent,
		children: [
			{
				path: 'new-employee-mailing-list',
				component: ItAdminComponent,
				canActivate: [AuthGuard, ItGuard],
			},
			{
				path: 'add-mail-group',
				component: EmailGroupComponent,
				canActivate: [AuthGuard, ItGuard],
			},
		],
	},
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule],
})
export class ItActionsRoutingModule {}
