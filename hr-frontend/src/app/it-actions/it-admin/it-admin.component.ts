import { Component, ElementRef, OnDestroy, OnInit, ViewChild } from '@angular/core';
import {
	FormBuilder,
	FormControl,
	FormGroup,
	NgForm,
	Validators,
} from '@angular/forms';
import { MatAutocompleteSelectedEvent } from '@angular/material/autocomplete';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Observable, Subject, Subscription } from 'rxjs';
import {
	debounceTime,
	distinctUntilChanged,
	finalize,
	map,
	switchMap,
	takeUntil,
	tap,
} from 'rxjs/operators';
import {
	DistributionList,
	Employee,
	ConfirmedEmployee,
	NewEmployees,
} from 'src/app/interfaces/main.interfaces';
import { ApiService } from 'src/app/services/api.service';
import { DataSharingService } from 'src/app/services/data-sharing.service';
import { DestroyService } from 'src/app/services/destroy.service';
import { HelperService } from 'src/app/services/helper.service';
import { NotificationDialog } from 'src/app/shared/notification-dialog/notification-dialog';

@Component({
	selector: 'app-it-admin',
	templateUrl: './it-admin.component.html',
	styleUrls: ['./it-admin.component.css'],
})
export class ItAdminComponent implements OnInit, OnDestroy {
	// form Properties
	employeeForm: FormGroup;

	// Chips properties
	removable = false;
	selectedGroups = new Set<DistributionList>();

	filteredGroups: DistributionList[] = [];
	groupsSub: Subscription;
	isLoading = false;

	// List properties
	newEmployees: Observable<NewEmployees>;

	paginatedEmployees: Employee[] = [];
	itConfirmedEmployees = new Set<Employee>();
	size = 5;
	page = 0;
	clickedRows = new Set<string>();

	filterNewEmployee = new FormControl('');

	@ViewChild('groupInput') groupInput: ElementRef<HTMLInputElement>;

	constructor(
		private formBuilder: FormBuilder,
		private apiService: ApiService,
		private dataSharingService: DataSharingService,
		private helperService: HelperService,
		public dialog: MatDialog,
		public snackBar: MatSnackBar,
		private destroy$: DestroyService
	) {
		this.employeeForm = formBuilder.group({
			firstName: [{ value: '', disabled: true }],
			lastName: [{ value: '', disabled: true }],
			sex: [{ value: '', disabled: true }],
			dateOfBirth: [{ value: '', disabled: true }],
			email: [
				'',
				[
					Validators.required,
					Validators.email,
					Validators.pattern(/@pedaids.org/),
				],
			],
			associateId: [{ value: '', disabled: true }],
			distributionList: [''],
			contract: this.formBuilder.group({
				startDate: [{ value: '', disabled: true }],
				endDate: [{ value: '', disabled: true }],
				status: [{ value: '', disabled: true }],
				position: [{ value: '', disabled: true }],
				office: [{ value: '', disabled: true }],
				department: [{ value: '', disabled: true }],
			}),
		});

		this.newEmployees = this.dataSharingService.newEmployees$;
	}

	ngOnInit(): void {
		this._filterGroups();
		this.apiService
			.getNewEmployees()
			.subscribe(() =>
				this.getEmployees({ pageIndex: this.page, pageSize: this.size })
			);
		this.applyFilter();
	}

	getEmployees(pageProps: { pageIndex: number; pageSize: number }) {
		let index = 0,
			startingIndex = pageProps.pageIndex * pageProps.pageSize,
			endingIndex = startingIndex + pageProps.pageSize;

		this.newEmployees.subscribe((employees) => {
			this.paginatedEmployees = employees.getValues().filter(() => {
				index++;
				return index > startingIndex && index <= endingIndex ? true : false;
			});
		});
	}
	applyFilter() {
		this.filterNewEmployee.valueChanges
			.pipe(
				debounceTime(300),
				distinctUntilChanged(),
				switchMap((filterText: string) =>
					this.newEmployees.pipe(
						map((employees) =>
							employees
								.getValues()
								.filter((employee) => this._applyFilter(filterText, employee))
						)
					)
				),
				takeUntil(this.destroy$)
			)
			.subscribe((employees) => {
				let index = 0,
					startingIndex = this.page * this.size,
					endingIndex = startingIndex + this.size;

				this.paginatedEmployees = employees.filter(() => {
					index++;
					return index > startingIndex && index <= endingIndex ? true : false;
				});
			});
	}

	private _applyFilter(filterText: string, employee: any) {
		const filter = filterText.trim().toLowerCase();
		const accumulator = (currentItem, key) => {
			return this.nestedFilterCheck(currentItem, employee, key);
		};
		const objKeys = Object.keys(employee).reduce(accumulator, '').toLowerCase();
		return objKeys.indexOf(filter) !== -1;
	}

	private nestedFilterCheck(search, data, key) {
		if (typeof data[key] === 'object') {
			for (const k in data[key]) {
				if (data[key][k] !== null) {
					search = this.nestedFilterCheck(search, data[key], k);
				}
			}
		} else {
			search += data[key];
		}
		return search;
	}

	saveEmployee(form: NgForm) {
		if (this.employeeForm.invalid) {
			this.openDialog({
				message: 'Make sure the email field has a valid email or is not empty ',
			});
			this.employeeForm.markAllAsTouched();
			this.employeeForm.markAsDirty();
		} else if (this.selectedGroups.size === 0) {
			this.openDialog({
				message: 'Please add at least one distribution group to this employee',
			});
		} else {
			const updatedEmployee = this.employeeForm.getRawValue() as Employee;
			updatedEmployee.distributionList = [...this.selectedGroups];
			this.newEmployees.subscribe((employees) => {
				employees.getValues().reduce((filteredValue, employee, i) => {
					if (employee.associateId === updatedEmployee.associateId) {
						const tempEmp = { ...employee, ...updatedEmployee };
						filteredValue.splice(i, 1, tempEmp);
						this.itConfirmedEmployees.add(tempEmp);
						this.dataSharingService.setNewEmployees(
							this.helperService.toNewEmployeeModel(
								employees.delValue(employee.associateId)
							)
						);
						this.getEmployees({ pageIndex: this.page, pageSize: this.size });
					}
					return filteredValue;
				}, []);
			});

			this.paginatedEmployees = this.paginatedEmployees.reduce(
				(filteredValue, employee, i) => {
					if (employee.associateId === updatedEmployee.associateId) {
						const tempEmp = { ...employee, ...updatedEmployee };
						filteredValue.splice(i, 1, tempEmp);
					}
					return filteredValue;
				},
				this.paginatedEmployees
			);
			this.clickedRows.add(updatedEmployee.associateId);
			form.resetForm();
			this.employeeForm.reset();
			this.selectedGroups.clear();
		}
	}

	public get f() {
		return this.employeeForm.controls;
	}

	onSubmit() {
		let apiData: ConfirmedEmployee[] = [];
		this.itConfirmedEmployees.forEach((employee) => {
			const tempEmp: ConfirmedEmployee = {
				associateId: employee.associateId,
				email: employee.email,
				distributionListIds: employee.distributionList.map(
					(group) => group.groupId
				),
			};
			apiData.push(tempEmp);
		});
		this.apiService.ictConfirmedEmployees(apiData).subscribe((resp) => {
			this.itConfirmedEmployees.clear();
			this.successMessage('Successfully submitted', 'Success');
		});
	}

	private _filterGroups() {
		this.employeeForm.controls.distributionList.valueChanges
			.pipe(
				debounceTime(300),
				distinctUntilChanged(),
				tap(() => (this.isLoading = true)),
				switchMap((searchTerm) =>
					this.apiService
						.findDistributionGroups(searchTerm)
						.pipe(finalize(() => (this.isLoading = false)))
				)
			)
			.subscribe((groups) => (this.filteredGroups = groups));
	}

	displayFn(group: DistributionList): string {
		const groupSelected = !!(group && group.groupName);
		return groupSelected ? group.groupName : '';
	}

	fillForm(employee: Employee) {
		this.employeeForm.patchValue(employee);
		this.contractForm.controls.department.patchValue(
			employee.contract.department.departmentName
		);
		this.contractForm.controls.office.patchValue(
			employee.contract.office.officeName
		);
		this.contractForm.controls.position.patchValue(
			employee.contract.position.positionName
		);
	}
	public get contractForm(): FormGroup {
		return this.employeeForm.controls.contract as FormGroup;
	}

	add(event): void {
		const group = event.value as DistributionList;
		if (group) {
			this.selectedGroups.add(group);
		}

		event.chipInput!.clear();
		this.employeeForm.controls.distributionList.setValue(null);
	}

	remove(group: DistributionList): void {
		this.selectedGroups.delete(group);
	}

	selected(event: MatAutocompleteSelectedEvent): void {
		this.selectedGroups.add(event.option.value);
		this.groupInput.nativeElement.value = '';
		this.employeeForm.controls.distributionList.setValue(null);
	}

	private openDialog(err: any): void {
		const dialogRef = this.dialog.open(NotificationDialog, {
			width: '20rem',
			data: { title: err.title, message: err.message },
		});
		dialogRef.afterClosed().subscribe((result) => {
			console.log(result);
		});
	}
	successMessage(message: string, action: string) {
		this.snackBar.open(message, action, { duration: 2000 });
	}

	ngOnDestroy(): void {
		this.destroy$.next();
		this.destroy$.complete();
	}
}
