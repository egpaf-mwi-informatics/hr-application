import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
	selector: 'app-it-container',
	templateUrl: './it-container.component.html',
	styleUrls: ['./it-container.component.css'],
})
export class ItContainerComponent implements OnInit {
	constructor(private router: Router, private route: ActivatedRoute) {}

	ngOnInit(): void {}

	newEmployeeMailingList() {
		this.router.navigate(['new-employee-mailing-list'], {
			relativeTo: this.route,
		});
	}

	newMailGroup() {
		this.router.navigate(['add-mail-group'], { relativeTo: this.route });
	}
}
