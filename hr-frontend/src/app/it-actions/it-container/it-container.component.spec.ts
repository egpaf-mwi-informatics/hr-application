import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ItContainerComponent } from './it-container.component';

describe('ItContainerComponent', () => {
  let component: ItContainerComponent;
  let fixture: ComponentFixture<ItContainerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ItContainerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ItContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
