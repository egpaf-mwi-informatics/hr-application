import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, NgForm, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ApiService } from 'src/app/services/api.service';
import { NotificationDialog } from 'src/app/shared/notification-dialog/notification-dialog';

@Component({
	selector: 'app-email-group',
	templateUrl: './email-group.component.html',
	styleUrls: ['./email-group.component.css'],
})
export class EmailGroupComponent implements OnInit {
	mailingListFormGroup: FormGroup;
	constructor(
		private formBuilder: FormBuilder,
		private apiService: ApiService,
		private snackBar: MatSnackBar,
		public dialog: MatDialog
	) {}

	ngOnInit(): void {
		this.mailingListFormGroup = this.formBuilder.group({
			groupName: ['', Validators.required],
			groupEmail: [
				'',
				[
					Validators.required,
					Validators.email,
					Validators.pattern(/@pedaids.org/),
				],
			],
			groupDescription: ['', Validators.required],
		});
	}

	addMailGroup(mailGroupForm: NgForm) {
		if (this.mailingListFormGroup.valid) {
			this.apiService
				.addMailingGroup(this.mailingListFormGroup.value)
				.subscribe(
					(resp) => {
						this.mailingListFormGroup.reset();
						mailGroupForm.resetForm();
						this.successMessage(resp.messge, 'Success');
					},
					(err) => {
						this.openDialog(err);
					}
				);
		} else {
			console.log('Error action on submission');
		}
	}
	private openDialog(err: any): void {
		const dialogRef = this.dialog.open(NotificationDialog, {
			width: '20rem',
			data: { title: err.title, message: err.message },
		});
		dialogRef.afterClosed().subscribe((result) => {
			console.log(result);
		});
	}

	public get f() {
		return this.mailingListFormGroup.controls;
	}

	successMessage(message: string, action: string) {
		this.snackBar.open(message, action, { duration: 300 });
	}

	getEmailErrMsg() {
		if (this.f.groupEmail.hasError('required')) {
			return 'Name cannot be empty';
		} else if (this.f.groupEmail.hasError('email')) {
			return 'This is not a valid email';
		} else {
			return this.f.groupEmail.hasError('pattern')
				? 'This is not a valid EGPAF email'
				: '';
		}
	}
	getDescriptionErrMsg() {
		return this.f.groupDescription.hasError('required')
			? 'Please provide a simple description about this group'
			: '';
	}
	getNameErrMsg() {
		return this.f.groupName.hasError('required') ? 'Name cannot be empty' : '';
	}
}
