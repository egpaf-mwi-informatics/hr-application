import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ItActionsRoutingModule } from './it-actions-routing.module';
import { ItAdminComponent } from './it-admin/it-admin.component';
import { EmailGroupComponent } from './email-group/email-group.component';
import { ItContainerComponent } from './it-container/it-container.component';
import { SharedModule } from '../shared/shared.module';
import { MatChipsModule } from '@angular/material/chips';


@NgModule({
  declarations: [
    ItAdminComponent,
    EmailGroupComponent,
    ItContainerComponent
  ],
  imports: [
    CommonModule,
    ItActionsRoutingModule,
		MatChipsModule,

    SharedModule
  ]
})
export class ItActionsModule { }
