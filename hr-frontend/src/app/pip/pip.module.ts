import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PipRoutingModule } from './pip-routing.module';
import { PipContainerComponent } from './pip-container/pip-container.component';
import { PipManagerComponent } from './pip-manager/pip-manager.component';
import { SharedModule } from '../shared/shared.module';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatSliderModule } from '@angular/material/slider';
import { SupervisedEmployeesComponent } from './supervised-employees/supervised-employees.component';

@NgModule({
	declarations: [
		PipContainerComponent,
		PipManagerComponent,
  SupervisedEmployeesComponent,
	],
	imports: [
		CommonModule,
		SharedModule,
		MatExpansionModule,
		MatSliderModule,
		PipRoutingModule,
	],
})
export class PipModule {}
