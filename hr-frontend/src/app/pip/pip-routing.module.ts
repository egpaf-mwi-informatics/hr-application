import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from '../shared/guards/auth.guard';
import { SupervisorGuard } from '../shared/guards/supervisor.guard';
import { PipListDataResolver } from '../shared/resolver/pip-list-data.resolver';
import { SubordinatesResolver } from '../shared/resolver/subordinates.resolver';
import { PipContainerComponent } from './pip-container/pip-container.component';
import { PipManagerComponent } from './pip-manager/pip-manager.component';
import { SupervisedEmployeesComponent } from './supervised-employees/supervised-employees.component';

const routes: Routes = [
	{
		path: '',
		component: PipContainerComponent,
		children: [
			{
				path: 'manager/subordinates-list',
				component: SupervisedEmployeesComponent,
				canActivate: [AuthGuard, SupervisorGuard],
				resolve: { data: SubordinatesResolver },
			},
			{
				path: 'second-level-manager/find-supervisee',
				component: SupervisedEmployeesComponent,
				canActivate: [AuthGuard, SupervisorGuard],
				resolve: { data: SubordinatesResolver },
			},
			{
				path: 'hr/find-supervisee',
				component: SupervisedEmployeesComponent,
				canActivate: [AuthGuard, SupervisorGuard],
				resolve: { data: PipListDataResolver },
			},
			{
				path: ':id',
				component: PipManagerComponent,
				canActivate: [AuthGuard, SupervisorGuard],
			},
		],
	},
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule],
})
export class PipRoutingModule {}
