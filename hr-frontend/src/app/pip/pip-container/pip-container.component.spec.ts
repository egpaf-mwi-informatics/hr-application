import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PipContainerComponent } from './pip-container.component';

describe('PipContainerComponent', () => {
  let component: PipContainerComponent;
  let fixture: ComponentFixture<PipContainerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PipContainerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PipContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
