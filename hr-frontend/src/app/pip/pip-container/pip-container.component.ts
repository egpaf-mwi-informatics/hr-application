import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-pip-container',
  templateUrl: './pip-container.component.html',
  styleUrls: ['./pip-container.component.css'],
})
export class PipContainerComponent {
  title: string = null;
  constructor(private router: Router, private route: ActivatedRoute) {}

  asManager() {
    this.title = 'As Supervisor';
    this.router.navigate(['manager/subordinates-list'], {
      relativeTo: this.route,
    });
  }
  asSecondLevelManager() {
    this.title = 'As Second Level Supervisor';
    this.router.navigate(['second-level-manager/find-supervisee'], {
      relativeTo: this.route,
    });
  }
  asHr() {
    this.title = 'As HR';
    this.router.navigate(['hr/find-supervisee'], { relativeTo: this.route });
  }
}
