import { AfterContentInit, Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Employee, Supervisor } from 'src/app/interfaces/main.interfaces';
import { ApiService } from 'src/app/services/api.service';
import { DataSharingService } from 'src/app/services/data-sharing.service';

@Component({
	selector: 'app-supervised-employees',
	templateUrl: './supervised-employees.component.html',
	styleUrls: ['./supervised-employees.component.css'],
})
export class SupervisedEmployeesComponent implements OnInit, AfterContentInit {
	filterSupervisedEmployee = new FormControl('');
	paginatedSubordinates: Employee[] = [];
	isFirstLevelManager: Observable<boolean>;
	page = 0;
	size = 5;
	supervisedEmployees: Observable<Employee[]>;
	role: string;

	constructor(
		private route: ActivatedRoute,
		private router: Router,
		private dataSharingService: DataSharingService,
		private apiService: ApiService
	) {
		this.role = this.route.snapshot.url.toString().split(',')[0];
		this.isFirstLevelManager = this.route.url.pipe(
			map((value) => {
				const role = value[0].toString();
				if (role === 'second-level-manager' || role === 'hr') {
					return false;
				} else {
					return true;
				}
			})
		);
	}

	ngOnInit(): void {
		if (this.role === 'manager' || this.role === 'second-level-manager') {
			this.supervisedEmployees = this.route.data.pipe(
				map((supervisor: { data: Supervisor }) => {
					return supervisor.data.subordinates;
				})
			);
		} else {
			this.supervisedEmployees = this.route.data.pipe(
				map((employees) => employees.data)
			);
		}
	}

	ngAfterContentInit(): void {
		this.getEmployees({ pageIndex: this.page, pageSize: this.size });
	}

	getEmployees(pageProps: { pageIndex: number; pageSize: number }) {
		let index = 0,
			startingIndex = pageProps.pageIndex * pageProps.pageSize,
			endingIndex = startingIndex + pageProps.pageSize;

		this.supervisedEmployees.subscribe((employees) => {
			this.paginatedSubordinates = employees.filter(() => {
				index++;
				return index > startingIndex && index <= endingIndex ? true : false;
			});
		});
	}

	toPipManagement(supervisee: Employee) {
		if (this.role === 'hr') {
			this.apiService
				.getPipData(supervisee.contract.contractId)
				.subscribe((pip) => {
					supervisee.onPip = true;
					supervisee.pip = pip;
					this.dataSharingService.setEmployee(supervisee);
					this.router.navigate(['../'], { relativeTo: this.route });
				});
		} else {
			this.dataSharingService.setEmployee(supervisee);
			this.router.navigate(['../'], { relativeTo: this.route });
		}
	}
}
