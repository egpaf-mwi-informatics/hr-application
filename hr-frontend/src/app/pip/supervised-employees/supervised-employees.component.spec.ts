import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SupervisedEmployeesComponent } from './supervised-employees.component';

describe('SupervisedEmployeesComponent', () => {
  let component: SupervisedEmployeesComponent;
  let fixture: ComponentFixture<SupervisedEmployeesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SupervisedEmployeesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SupervisedEmployeesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
