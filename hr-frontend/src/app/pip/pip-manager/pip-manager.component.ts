import { Component, OnInit, AfterViewInit } from '@angular/core';
import {
	FormArray,
	FormBuilder,
	FormControl,
	FormGroup,
	NgForm,
	Validators,
} from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import * as moment from 'moment';
import { Observable } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { Employee } from 'src/app/interfaces/main.interfaces';
import { ApiService } from 'src/app/services/api.service';
import { DataSharingService } from 'src/app/services/data-sharing.service';
import { NotificationDialog } from 'src/app/shared/notification-dialog/notification-dialog';

@Component({
	selector: 'app-pip-manager',
	templateUrl: './pip-manager.component.html',
	styleUrls: ['./pip-manager.component.css'],
})
export class PipManagerComponent implements OnInit, AfterViewInit {
	// forms
	employeeForm: FormGroup;
	priorityForm: FormGroup;
	pipForm: FormGroup;

	toggleExpansion = true;
	pipEmployee: Observable<Employee>;

	// data
	sexes = ['Female', 'Male'];
	statuses = ['Active', 'Not active'];

	constructor(
		private formBuilder: FormBuilder,
		private apiService: ApiService,
		private dataSharingService: DataSharingService,
		private router: Router,
		private route: ActivatedRoute,
		private snackBar: MatSnackBar,
		public dialog: MatDialog
	) {
		this.employeeForm = this.formBuilder.group({
			associateId: [{ value: '', disabled: true }],
			firstName: [{ value: '', disabled: true }],
			lastName: [{ value: '', disabled: true }],
			dateOfBirth: [{ value: '', disabled: true }],
			email: [{ value: '', disabled: true }],
			sex: [{ value: '', disabled: true }],
			contract: this.formBuilder.group({
				startDate: [{ value: null, disabled: true }],
				endDate: [{ value: null, disabled: true }],
				status: [{ value: '', disabled: true }],
				position: [{ value: '', disabled: true }],
				office: [{ value: '', disabled: true }],
				department: [{ value: '', disabled: true }],
			}),
		});

		this.pipForm = this.formBuilder.group({
			priorities: this.formBuilder.array([
				this.formBuilder.group({
					priorityName: ['', Validators.required],
					priorityStatus: ['', Validators.required],
					priorityStartDate: [
						{ value: null, disabled: true },
						Validators.required,
					],
					priorityEndDate: [
						{ value: null, disabled: true },
						Validators.required,
					],
				}),
			]),
			pipComments: ['', Validators.required],
			pipSecondLevelManagerComments: [
				{ value: '', disabled: true },
				Validators.required,
			],
			pipHrComments: [{ value: '', disabled: true }, Validators.required],
			progress: ['', Validators.required],
			pipStartDate: [{ value: '', disabled: true }, Validators.required],
			pipEndDate: [{ value: '', disabled: true }, Validators.required],
		});

		this.priorityForm = this.formBuilder.group({
			priorityName: ['', Validators.required],
			priorityStatus: ['', Validators.required],
			priorityStartDate: [{ value: '', disabled: true }, Validators.required],
			priorityEndDate: [{ value: '', disabled: true }, Validators.required],
		});
	}

	ngOnInit(): void {
		this.dataSharingService.employee$.subscribe((employee) => {
			const contract = {
				...employee?.contract,
				...{
					position: employee?.contract.position.positionName,
					department: employee?.contract.department.departmentName,
					office: employee?.contract.office.officeName,
				},
			};
			const prefillData = { ...employee, contract: contract };
			this.employeeForm.patchValue(prefillData);
			if (employee.onPip) {
				const pip = employee.pip[employee.pip.length - 1];
				this.pipForm.patchValue({
					pipStartDate: pip.startDate,
					pipEndDate: pip.endDate,
					pipComments: pip.pipComments,
				});
				this.priorities.removeAt(0);
				pip.priorities.forEach((priority) => {
					const tempForm = new FormGroup({
						priorityName: new FormControl('', Validators.required),
						priorityStatus: new FormControl('', Validators.required),
						priorityStartDate: new FormControl(
							{ disabled: true },
							Validators.required
						),
						priorityEndDate: new FormControl(
							{ disabled: true },
							Validators.required
						),
					});
					tempForm.patchValue(priority);
					this.priorities.push(tempForm);
				});
			}
		});
		this.pipEmployee = this.dataSharingService.employee$;
	}

	ngAfterViewInit(): void {
		const url = this.route.snapshot.params.id;

		switch (url) {
			case 'second-level-manager':
				setTimeout(() => {
					this.priorityForm.disable();
					this.pipForm.disable();
					this.pipControls.pipSecondLevelManagerComments.enable();
				}, 0);
				break;
			case 'hr':
				setTimeout(() => {
					this.priorityForm.disable();
					this.pipForm.disable();
					this.pipControls.pipHrComments.enable();
				}, 0);
				break;
		}
	}

	public get priorities() {
		return this.pipForm.controls.priorities as FormArray;
	}

	public get pipControls() {
		return this.pipForm.controls;
	}

	public addPriority() {
		const tempForm = this.priorityForm;
		this.priorities.push(tempForm);
	}
	public removePriority(i: number) {
		this.priorities.removeAt(i);
	}

	formartSliderLabel(value: number) {
		return value + '%';
	}

	toggle() {
		this.toggleExpansion = !this.toggleExpansion;
	}

	onSubmit(pipTempForm: NgForm) {
		let userId = null;
		this.dataSharingService.authUser$.subscribe(
			(user) => (userId = user.associateId)
		);

		const url = this.route.snapshot.params.id;

		if (pipTempForm.submitted && this.employeeForm.invalid) {
			this.openDialog({
				message:
					'Not all required fields are filled yet,\
					 please make sure all the fields are filled',
			});
		} else {
			switch (url) {
				case 'manager':
					const pipFormData = this.pipForm.getRawValue();
					this.dataSharingService.employee$.subscribe((employee) => {
						const apiData = {
							contractId: employee.contract.contractId,
							pip: pipFormData,
						};
						this.apiService.setEmployeePip(apiData).subscribe(
							() => {
								this.pipForm.reset();
								pipTempForm.resetForm;
								this.successMessage('Successfully submitted', 'Success');
								this.router.navigate(['find-supervisee'], {
									relativeTo: this.route,
								});
							},
							(err) => {
								this.openDialog(err);
							}
						);
					});

					break;

				default:
					const comment = !!this.pipForm.value.pipHrComments
						? this.pipForm.value.pipHrComments
						: this.pipForm.value.pipSecondLevelManagerComments;

					const apiData = {
						comment: comment,
						date: moment().format('YYYY-MM-DD hh:mm:ss'),
						comment_from: url,
					};
					const observable$ = this.dataSharingService.authUser$.pipe(
						switchMap((user) =>
							this.apiService.addPipComment({
								...apiData,
								...{ employeeId: user.associateId },
							})
						)
					);
					observable$.subscribe(
						() => {
							this.pipForm.reset();
							pipTempForm.resetForm;
							this.successMessage('Successfully submitted', 'Success');
							this.router.navigate(['find-supervisee'], {
								relativeTo: this.route,
							});
						},
						(err) => {
							this.openDialog(err);
						}
					);

					break;
			}
		}
	}

	private openDialog(err: any): void {
		const dialogRef = this.dialog.open(NotificationDialog, {
			width: '20rem',
			data: { title: err.title, message: err.message },
		});
		dialogRef.afterClosed().subscribe((result) => {
			console.log(result);
		});
	}
	successMessage(message: string, action: string) {
		this.snackBar.open(message, action, { duration: 300 });
	}
}
