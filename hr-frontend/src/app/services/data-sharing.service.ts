import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import {
	EmpPerDisPerDepData,
	EmpPerDistData,
	IData,
} from '../interfaces/chart.interfaces';
import { Employee, NewEmployees, User } from '../interfaces/main.interfaces';

@Injectable({
	providedIn: 'root',
})
export class DataSharingService {
	private _employee = new BehaviorSubject<Employee>(null);
	employee$ = this._employee.asObservable();

	private _employees = new BehaviorSubject<Employee[]>(null);
	employees$ = this._employees.asObservable();

	private _endingContractsData = new BehaviorSubject<IData>(null);
	endingContractsData$ = this._endingContractsData.asObservable();

	private _genderData = new BehaviorSubject<IData>(null);
	genderData$ = this._genderData.asObservable();

	private _empPerDistData = new BehaviorSubject<EmpPerDistData>(null);
	empPerDistData$ = this._empPerDistData.asObservable();

	private _empPerDeptData = new BehaviorSubject<IData>(null);
	empPerDeptData$ = this._empPerDeptData.asObservable();

	private _empPerDisPerDeptData = new BehaviorSubject<EmpPerDisPerDepData>(
		null
	);
	empPerDisPerDeptData$ = this._empPerDisPerDeptData.asObservable();

	private _resourceControlsActivated = new BehaviorSubject<boolean>(false);
	resourceMgmtActvated = this._resourceControlsActivated.asObservable();

	private _newEmployees = new BehaviorSubject<NewEmployees>(null);
	newEmployees$ = this._newEmployees.asObservable();

	private _employeeMgmtActivated = new BehaviorSubject<boolean>(false);
	employeeMgmtActivated$ = this._employeeMgmtActivated.asObservable();

	private _authUser = new BehaviorSubject<User>(null);
	authUser$ = this._authUser.asObservable();

	setAuthUser(user: User) {
		this._authUser.next(user);
	}

	setEmpPerDisPerDept(data: any) {
		this._empPerDisPerDeptData.next(data);
	}
	setEmployee(employee: Employee) {
		this._employee.next(employee);
	}
	setEmployees(employees: Employee[]) {
		this._employees.next(employees);
	}
	setEndingContractsData(data: IData) {
		this._endingContractsData.next(data);
	}

	setGenderData(data: any) {
		this._genderData.next(data);
	}

	setEmpPerDistData(data: any) {
		this._empPerDistData.next(data);
	}

	setEmpPerDeptData(data: any) {
		this._empPerDeptData.next(data);
	}

	activateResources(val: boolean) {
		this._resourceControlsActivated.next(val);
	}
	activateEmployees(val: boolean) {
		this._employeeMgmtActivated.next(val);
	}
	setNewEmployees(newEmployees: NewEmployees): void {
		this._newEmployees.next(newEmployees);
	}
}
