import { Injectable } from '@angular/core';
import {
	EmpPerDistData,

	IData,
} from '../interfaces/chart.interfaces';
import { Employee, NewEmployees } from '../interfaces/main.interfaces';

@Injectable({
	providedIn: 'root',
})
export class HelperService {
	constructor() {}
	modelToSnakeCase(model) {
		const apiModel = {};
		if (model) {
			for (const key of Object.keys(model)) {
				if (
					!!model[key] &&
					typeof model[key] === 'object' &&
					!(model[key] instanceof Date)
				) {
					apiModel[this.camelToSnakeCase(key)] = this.modelToSnakeCase(
						model[key]
					);
				} else if (!!model[key] && model[key] instanceof Array) {
					apiModel[this.camelToSnakeCase(key)] = model[key].map((value) =>
						this.modelToSnakeCase(value)
					);
				} else {
					apiModel[this.camelToSnakeCase(key)] = model[key];
				}
			}
		}
		return apiModel;
	}

	modelToCamelCase(apiModel) {
		if (!apiModel) {
			return;
		}
		const modelData = {};
		for (const key of Object.keys(apiModel)) {
			if (apiModel[key] === null && apiModel[key] === undefined) {
				return;
			}
			if (Array.isArray(apiModel[key])) {
				const array = [];
				for (const item of apiModel[key]) {
					switch (typeof item) {
						case 'string':
							array.push(this.snakeToCamelCase(item));
							break;
						case 'number':
							array.push(item);
							break;
						default:
							array.push(this.modelToCamelCase(item));
					}
				}
				modelData[this.snakeToCamelCase(key)] = array;
			} else {
				modelData[this.snakeToCamelCase(key)] =
					typeof apiModel[key] === 'object'
						? this.modelToCamelCase(apiModel[key])
						: apiModel[key];
			}
		}
		return modelData;
	}

	private camelToSnakeCase(key: string) {
		return key.replace(/[A-Z]/g, (letter) => `_${letter.toLowerCase()}`);
	}
	private snakeToCamelCase(key: string) {
		return key
			.replaceAll(/[-_][a-z]/g, (letter) => letter.toUpperCase())
			.replaceAll('_', '');
	}

	toEmployeeObject(employee: any): Employee {
		return <Employee>{
			associateId: employee.associateId,
			firstName: employee.firstName,
			lastName: employee.lastName,
			dateOfBirth: employee.dateOfBirth ? employee.dateOfBirth : '',
			sex: employee.sex ? employee.sex : '',
			onPip: employee.pip ? !!employee.pip : false,
			pip: employee.pip,
			email: employee.email,
			contract: {
				contractId: employee.contractId,
				position: {
					positionId: employee.positionId,
					positionName: employee.positionName,
				},
				department: {
					departmentId: employee.departmentId,
					departmentName: employee.departmentName,
				},
				office: {
					officeId: employee.officeId,
					officeName: employee.officeName,
				},
				startDate: employee.startDate,
				endDate: employee.endDate,
				status: employee.status ? employee.status : '',
			},
		};
	}

	toNewEmployeeModel(employees: Employee[]): NewEmployees {
		let newEmployees: NewEmployees = {
			getValues: function () {
				return employees;
			},
			delValue: function (associateId: string) {
				return employees.reduce((newEmployees, currEmployee) => {
					if (currEmployee.associateId !== associateId) {
						newEmployees.push(currEmployee);
					}
					return newEmployees;
				}, []);
			},
		};

		employees.map((employee) => {
			newEmployees[employee.associateId] = employee;
		});
		return newEmployees;
	}

	chartDataConvertor(
		data: Array<{ count: number; label: string }>,
		chartType: 'pie' | 'stack' | 'treemap' | 'bar'
	): IData | EmpPerDistData {
		if (chartType === 'treemap') {
			const datax: EmpPerDistData = {
				data: data.map((value) => {
					return { x: value.label, y: value.count };
				}),
			};
			return datax;
		} else {
			const chartData: IData = {
				count: [],
				labels: [],
			};
			data.forEach((datum) => {
				chartData.count.push(datum.count);
				chartData.labels.push(datum.label);
			});
			return chartData;
		}
	}
}
