import { Injectable } from '@angular/core';
import { DataSharingService } from './data-sharing.service';
import {
	Position,
	Credentials,
	Department,
	Employee,
	Office,
	DistributionList,
	ConfirmedEmployee,
	Project,
	NewEmployees,
	DependantType,
	Auth,
	User,
	Supervisor,
	Status,
} from './../interfaces/main.interfaces';
import { Observable } from 'rxjs';
import {
	HttpClient,
	HttpErrorResponse,
	HttpResponse,
} from '@angular/common/http';
import { concatMap, map, tap } from 'rxjs/operators';
import { HelperService } from './helper.service';
import {
	EmpPerDisPerDepData,
	EmpPerDistData,
	IData,
} from '../interfaces/chart.interfaces';
import { ADMIN_ROLES, HR_ROLES, IT_ROLES } from '../shared/shared.roles';
import { Router } from '@angular/router';
import { ParseResult } from 'ngx-papaparse';

@Injectable({
	providedIn: 'root',
})
export class ApiService {
	dataAvailable = true;
	constructor(
		private http: HttpClient,
		private helperService: HelperService,
		private dataSharingService: DataSharingService,
		private router: Router
	) {}

	// construct the actual object here
	login(credentials: Credentials): Observable<Auth | any> {
		return this.http.post<Auth>('employee/login', credentials).pipe(
			map((auth) => this.helperService.modelToCamelCase(auth) as Auth),
			tap((auth) => console.log(auth)),
			map((auth) => {
				if (auth.status === 200) {
					return auth;
				} else if (auth.status === 401) {
					throw new HttpErrorResponse({ status: auth.status });
				}
			}),
			tap((auth: Auth) => {
				console.log(auth);
				localStorage.setItem('token', auth.token);
				localStorage.setItem('associateId', auth.data.associateId);
			}),
			concatMap((auth) =>
				this.isSupervisor(auth.data.positionId).pipe(
					map((position) => {
						auth.data.isSupervisor = position.supervirsor;
						return auth;
					})
				)
			),
			tap((auth: Auth) => {
				this.dataSharingService.setAuthUser(auth.data);
			})
		);
	}

	logout() {
		localStorage.clear();
		this.router.navigate(['/']);
		this.dataSharingService.setAuthUser(null);
	}

	addEmployees(data: ParseResult): Observable<any> {
		console.log(data);
		return this.http.post<any>('employee/fileImport', data);
	}

	onRefresh(associateId: string): Observable<User> {
		return this.http
			.post<User>('employee/reload', { associateId: associateId })
			.pipe(
				map((user) => this.helperService.modelToCamelCase(user) as User),
				concatMap((user) =>
					this.isSupervisor(user.positionId).pipe(
						map((position) => {
							user.isSupervisor = position.supervirsor;
							return user;
						})
					)
				),
				tap((user) => this.dataSharingService.setAuthUser(user))
			);
	}

	private isSupervisor(positionId: number): Observable<Position> {
		return this.http.post<Position>('matrix/findSupervisor', {
			positionId: positionId,
		});
	}

	getSupervisedEmployees(positionId: number): Observable<Supervisor> {
		return this.http
			.post<any>('matrix/findSupervisedEmployees', {
				positionId: positionId,
			})
			.pipe(
				// to correct a data object, nested priorities object
				map((supervisor) => {
					supervisor.subordinates.map((subordinate) => {
						if (!!subordinate.pip) {
							subordinate.pip[subordinate.pip.length - 1].priorities =
								subordinate.pip[
									subordinate.pip.length - 1
								].priorities.priorities;
						}
						return subordinate;
					});
					return supervisor;
				}),
				map((supervisor) => {
					const subordinates = supervisor.subordinates.map(
						(subordinate) =>
							this.helperService.modelToCamelCase(subordinate) as any
					);
					supervisor.subordinates = subordinates;
					return supervisor;
				}),
				map((supervisor) => {
					const subordinates = supervisor.subordinates.map(
						(subordinate) =>
							this.helperService.toEmployeeObject(subordinate) as Employee
					);
					supervisor.subordinates = subordinates;
					return supervisor;
				})
			);
	}

	getUserRole(user: User): string {
		if (IT_ROLES.includes(user?.positionName)) {
			return 'itAccess';
		} else if (ADMIN_ROLES.includes(user?.positionName)) {
			return 'adminAccess';
		} else if (HR_ROLES.includes(user?.positionName)) {
			return 'hrAccess';
		} else {
			return null;
		}
	}

	addEmployee(employee: any) {
		return this.http.post('employee/insert', employee);
	}

	// After this an employee will be sent to HQ
	ictConfirmedEmployees(employee: ConfirmedEmployee[]) {
		return this.http.post('employeeDistribution/insert', employee);
	}

	addDependants(dependants: any) {
		return this.http.post('dependant/insert', dependants);
	}

	renewContract(renewData: any) {
		return this.http.post('contract/update', renewData);
	}

	editEmployee(editData: any) {
		return this.http.post('employee/update', editData);
	}

	findAllEmployees(): Observable<Employee[]> {
		return this.http.get<any[]>('employee/findAll').pipe(
			map((snakeCasedEmployees) =>
				snakeCasedEmployees.map((snakeCasedEmployee) =>
					this.helperService.modelToCamelCase(snakeCasedEmployee)
				)
			),
			map((camelCasedEmployees) =>
				camelCasedEmployees.map((camelCasedEmployee) =>
					this.helperService.toEmployeeObject(camelCasedEmployee)
				)
			)
		);
	}

	searchEmployees(term: string): Observable<Employee[]> {
		return this.http
			.get<any[]>('employee/search', {
				params: { searchText: term },
			})
			.pipe(
				map((snakeCasedEmployees) =>
					snakeCasedEmployees.map((snakeCasedEmployee) =>
						this.helperService.modelToCamelCase(snakeCasedEmployee)
					)
				),
				map((camelCasedEmployees) =>
					camelCasedEmployees.map((camelCasedEmployee) =>
						this.helperService.toEmployeeObject(camelCasedEmployee)
					)
				)
			);
	}

	findEmployees(param: any): Observable<Employee[]> {
		return this.http
			.get<any[]>('employee/filter', {
				params: {
					startDate: param?.startDate,
					endDate: param?.endDate,
					onProbation: param?.onProbation,
					onPip: param?.onPip,
					newHire: param?.newHire,
					terminatedContracts: param?.terminatedContracts,
				},
			})
			.pipe(
				map((snakeCasedEmployees) =>
					snakeCasedEmployees.map((snakeCasedEmployee) =>
						this.helperService.modelToCamelCase(snakeCasedEmployee)
					)
				),
				map((camelCasedEmployees) =>
					camelCasedEmployees.map((camelCasedEmployee) =>
						this.helperService.toEmployeeObject(camelCasedEmployee)
					)
				)
			);
	}

	findEmployeesOnPip(param: boolean): Observable<Employee[]> {
		return this.http
			.get<any[]>('employee/filter', {
				params: {
					onPip: param,
				},
			})
			.pipe(
				map((snakeCasedEmployees) =>
					snakeCasedEmployees.map(
						(snakeCasedEmployee) =>
							<any>this.helperService.modelToCamelCase(snakeCasedEmployee)
					)
				),
				map((camelCasedEmployees) =>
					camelCasedEmployees.map((camelCasedEmployee) =>
						this.helperService.toEmployeeObject(camelCasedEmployee)
					)
				)
			);
	}

	findRelations(term: string): Observable<DependantType[]> {
		return this.http
			.get<DependantType[]>('dependantRelation/search', {
				params: { searchText: term },
			})
			.pipe(
				map((relationTypes) =>
					relationTypes.map(
						(relationType) =>
							this.helperService.modelToCamelCase(relationType) as DependantType
					)
				)
			);
	}

	findPositions(term: string): Observable<Position[]> {
		return this.http
			.get<Position[]>('position/search', {
				params: { searchText: term },
			})
			.pipe(
				map((positions) =>
					positions.map(
						(position) =>
							<Position>this.helperService.modelToCamelCase(position)
					)
				)
			);
	}

	addPosition(position: any) {
		return this.http.post('position/insert', position);
	}

	updatePosition(position: Position) {
		return this.http.put('position/update', position);
	}

	findDepartments(term: string): Observable<Department[]> {
		return this.http
			.get<Department[]>('department/search', {
				params: { searchText: term },
			})
			.pipe(
				map((depts) =>
					depts.map(
						(dept) => <Department>this.helperService.modelToCamelCase(dept)
					)
				)
			);
	}

	addDepartment(department: any) {
		return this.http.post('department/insert', department);
	}

	updateDepartment(department: Department) {
		return this.http.put('department/update', department);
	}

	findStatuses(term: string): Observable<Status[]> {
		return this.http
			.get<Status[]>('status/findAll')
			.pipe(
				map((statuses) =>
					statuses.map(
						(status) => <Status>this.helperService.modelToCamelCase(status)
					)
				)
			);
	}

	findOffices(term: string): Observable<Office[]> {
		return this.http
			.get<Office[]>('office/search', {
				params: { searchText: term },
			})
			.pipe(
				map((offices) =>
					offices.map(
						(office) => <Office>this.helperService.modelToCamelCase(office)
					)
				)
			);
	}

	addOffice(office: any) {
		return this.http.post('department/insert', office);
	}

	updateOffice(office: Office) {
		return this.http.put('office/update', office);
	}

	findProjects(term: string): Observable<Project[]> {
		return this.http
			.get<Project[]>('project/search', {
				params: { searchText: term },
			})
			.pipe(
				map((projects) =>
					projects.map(
						(project) => <Project>this.helperService.modelToCamelCase(project)
					)
				)
			);
	}

	addMailingGroup(data: any): Observable<any> {
		return this.http.post('addGroupList', data);
	}

	addProject(project: any) {
		return this.http.post('department/insert', project);
	}

	updateProject(project: Project) {
		return this.http.put('project/update', project);
	}

	findDistributionGroups(searchTerm: string): Observable<DistributionList[]> {
		return this.http
			.get<DistributionList[]>('distributionList/search', {
				params: { searchText: searchTerm },
			})
			.pipe(
				map((groups) =>
					groups.map(
						(group) =>
							<DistributionList>this.helperService.modelToCamelCase(group)
					)
				)
			);
	}

	getNewEmployees(): Observable<NewEmployees> {
		return this.http.get<any>('employee/findNew').pipe(
			map((snakeCasedEmployees) =>
				snakeCasedEmployees.map(
					(snakeCasedEmployee) =>
						<any>this.helperService.modelToCamelCase(snakeCasedEmployee)
				)
			),
			map((camelCasedEmployees) =>
				camelCasedEmployees.map((camelCasedEmployee) =>
					this.helperService.toEmployeeObject(camelCasedEmployee)
				)
			),
			map((employeeModel) =>
				this.helperService.toNewEmployeeModel(employeeModel)
			),
			tap((newEmployees) => {
				this.dataSharingService.setNewEmployees(newEmployees);
			})
		);
	}

	endingContractsApi(): Observable<IData> {
		this.dataSharingService.endingContractsData$.subscribe(
			(data) => (this.dataAvailable = !!data)
		);
		if (this.dataAvailable) {
			return this.dataSharingService.endingContractsData$;
		} else {
			return this.http
				.get<any>('employee/findDepartmentExpiredContractChart')
				.pipe(
					map(
						(data) =>
							this.helperService.chartDataConvertor(data, 'bar') as IData
					),
					tap((data) => this.dataSharingService.setEndingContractsData(data))
				);
		}
	}

	setEmployeePip(pipData: any) {
		return this.http.post('pip/insert', pipData);
	}

	addPipComment(commentData: any) {
		console.log(commentData);
		return this.http.post('comment/insert', commentData);
	}

	getPipData(contractId: number): Observable<any> {
		return this.http
			.post<any>('pip/findByContractId', {
				contractId: contractId,
			})
			.pipe(
				map((pipData) =>
					pipData.map((eachPip) => this.helperService.modelToCamelCase(eachPip))
				),
				map((pipData) => {
					pipData[pipData.length - 1].priorities =
						pipData[pipData.length - 1].priorities.priorities;
					return pipData;
				})
			);
	}

	// To-Do: When the data is available
	genderPerDeptApi(): Observable<IData> {
		// To-Do
		if (this.dataAvailable) {
			return this.dataSharingService.genderData$;
		} else {
			return this.http.get<any>('employee/findGenderOrganizationalChart').pipe(
				map(
					(data) => this.helperService.chartDataConvertor(data, 'pie') as IData
				),
				tap((data) => this.dataSharingService.setGenderData(data))
			);
		}
	}

	employeesPerDistrictApi(): Observable<EmpPerDistData> {
		if (this.dataAvailable) {
			return this.dataSharingService.empPerDistData$;
		} else {
			return this.http.get<any>('employee/FindEmployeeDistrictChart').pipe(
				map(
					(data) =>
						this.helperService.chartDataConvertor(
							data,
							'treemap'
						) as EmpPerDistData
				),
				tap((data) => this.dataSharingService.setEmpPerDistData(data))
			);
		}
	}

	employeesPerDeptApi(): Observable<IData> {
		if (this.dataAvailable) {
			return this.dataSharingService.empPerDeptData$;
		} else {
			return this.http.get<any>('employee/FindEmployeeDepartmentChart').pipe(
				map(
					(data) =>
						this.helperService.chartDataConvertor(data, 'stack') as IData
				),
				tap((data) => this.dataSharingService.setEmpPerDeptData(data))
			);
		}
	}

	employeesPerDisPerDeptApi(): Observable<EmpPerDisPerDepData> {
		if (this.dataAvailable) {
			return this.dataSharingService.empPerDisPerDeptData$;
		} else {
			return this.http
				.get<EmpPerDisPerDepData>('employee/findEmployeeDistrictStackedChart')
				.pipe(tap((data) => this.dataSharingService.setEmpPerDisPerDept(data)));
		}
	}
}
