import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './auth/login/login.component';
import { NavBarComponent } from './nav/nav-bar/nav-bar.component';

const routes: Routes = [
	{ path: '', component: LoginComponent },
	{
		path: 'home',
		component: NavBarComponent,
		loadChildren: () => import('./nav/nav.module').then((m) => m.NavModule),
	},
];

@NgModule({
	imports: [
		RouterModule.forRoot(routes, {
			relativeLinkResolution: 'corrected',
			// enableTracing: true,
		}),
	],
	exports: [RouterModule],
})
export class AppRoutingModule {}
