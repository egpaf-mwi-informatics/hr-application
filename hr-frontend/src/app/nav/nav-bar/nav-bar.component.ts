import { Component, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { ApiService } from 'src/app/services/api.service';

@Component({
	selector: 'app-nav-bar',
	templateUrl: './nav-bar.component.html',
	styleUrls: ['./nav-bar.component.css'],
})
export class NavBarComponent {
	constructor(private router: Router, private apiService: ApiService) {}

	manageEmployee() {
		this.router.navigate(['home/hr-actions']);
	}

	logout() {
		this.apiService.logout();
	}
	tabulate() {
		this.router.navigate(['home/dashboard/table']);
	}
}
