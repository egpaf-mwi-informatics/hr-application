import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NavBarComponent } from './nav-bar/nav-bar.component';
import { SharedModule } from '../shared/shared.module';
import { MatMenuModule } from '@angular/material/menu';
import { NavBarRoutingModule } from './nav-bar-routing.module';
import { A11yModule } from '@angular/cdk/a11y';

import {
	MatChipsModule,
	MAT_CHIPS_DEFAULT_OPTIONS,
} from '@angular/material/chips';
import { ENTER, COMMA } from '@angular/cdk/keycodes';
import { MatDatepickerModule } from '@angular/material/datepicker';

@NgModule({
	declarations: [NavBarComponent],
	imports: [
		CommonModule,
		MatMenuModule,
		MatChipsModule,
		MatDatepickerModule,
		SharedModule,
		NavBarRoutingModule,
		A11yModule,
	],
	providers: [
		{
			provide: MAT_CHIPS_DEFAULT_OPTIONS,
			useValue: {
				separatorKeyCodes: [ENTER, COMMA],
			},
		},
	],
})
export class NavModule {}
