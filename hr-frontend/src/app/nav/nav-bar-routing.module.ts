
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardContainerComponent } from '../dashboard/dashboard-container/dashboard-container.component';
import { AuthGuard } from '../shared/guards/auth.guard';
import { HrGuard } from '../shared/guards/hr.guard';
import { ItGuard } from '../shared/guards/it.guard';
import { SupervisorGuard } from '../shared/guards/supervisor.guard';

const routes: Routes = [
	{
		path: '',
		redirectTo: 'dashboard',
		pathMatch: 'full',
	},
	{
		path: 'dashboard',
		component: DashboardContainerComponent,
		loadChildren: () =>
			import('../dashboard/dashboard.module').then((m) => m.DashboardModule),
		canActivate: [AuthGuard, HrGuard],
	},
	{
		path: 'hr-actions',
		loadChildren: () =>
			import('../hr-actions/hr-actions.module').then((m) => m.HrActionsModule),
		canActivate: [AuthGuard, HrGuard],
	},
	{
		path: 'it-actions',
		loadChildren: () =>
			import('../it-actions/it-actions.module').then((m) => m.ItActionsModule),
		canActivate: [AuthGuard, ItGuard],
	},
	{
		path: 'pip-mngmt',
		loadChildren: () => import('../pip/pip.module').then((m) => m.PipModule),
		canActivate: [AuthGuard, SupervisorGuard],
	},
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule],
})
export class NavBarRoutingModule {}
