import { TestBed } from '@angular/core/testing';

import { ApiMiddlewareInterceptor } from './api-middleware.interceptor';

describe('ApiMiddlewareInterceptor', () => {
  beforeEach(() => TestBed.configureTestingModule({
    providers: [
      ApiMiddlewareInterceptor
      ]
  }));

  it('should be created', () => {
    const interceptor: ApiMiddlewareInterceptor = TestBed.inject(ApiMiddlewareInterceptor);
    expect(interceptor).toBeTruthy();
  });
});
