import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatStepperModule } from '@angular/material/stepper';
import { EmployeeComponent } from './employee/employee.component';
import { StaffProfileComponent } from './staff-profile/staff-profile.component';
import { HrContainerComponent } from './hr-container/hr-container.component';
import { HrActionsRoutingModule } from './hr-actions-routing.module';
import { SharedModule } from '../shared/shared.module';
import { MatMenuModule } from '@angular/material/menu';
import { MatSliderModule } from '@angular/material/slider';
import { FindEmployeeComponent } from './find-employee/find-employee.component';
import { ResourceManagementComponent } from './resource-management/resource-management.component';
import { MatRippleModule } from '@angular/material/core';
import { MatExpansionModule } from '@angular/material/expansion';

@NgModule({
	declarations: [
		EmployeeComponent,
		StaffProfileComponent,
		HrContainerComponent,
		FindEmployeeComponent,
		ResourceManagementComponent,
		
	],
	imports: [
		CommonModule,
		MatStepperModule,
		MatMenuModule,
		MatRippleModule,
		SharedModule,
		HrActionsRoutingModule,
		MatExpansionModule,
		MatSliderModule,
	],
})
export class HrActionsModule {}
