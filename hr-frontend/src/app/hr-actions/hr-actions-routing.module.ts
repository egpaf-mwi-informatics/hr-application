import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FindEmployeeComponent } from './find-employee/find-employee.component';
import { HrContainerComponent } from './hr-container/hr-container.component';
import { EmployeeComponent } from './employee/employee.component';
import { StaffProfileComponent } from './staff-profile/staff-profile.component';
import { ResourceManagementComponent } from './resource-management/resource-management.component';
import { AuthGuard } from '../shared/guards/auth.guard';
import { HrGuard } from '../shared/guards/hr.guard';


const routes: Routes = [
	{
		path: '',
		component: HrContainerComponent,
		children: [
			{
				path: 'employee-mngmt/:id/find',
				component: FindEmployeeComponent,
				canActivate: [HrGuard, AuthGuard],
			},
			{
				path: 'employee-mngmt/:id',
				component: EmployeeComponent,
				canActivate: [HrGuard, AuthGuard],
			},
			{
				path: 'profile',
				component: StaffProfileComponent,
				canActivate: [HrGuard, AuthGuard],
			},
			{
				path: 'resource-mngmt/:id',
				component: ResourceManagementComponent,
				canActivate: [HrGuard, AuthGuard],
			},
		],
	},
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule],
})
export class HrActionsRoutingModule {}

