import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatAutocompleteSelectedEvent } from '@angular/material/autocomplete';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import {
	debounceTime,
	distinctUntilChanged,
	finalize,
	switchMap,
	tap,
} from 'rxjs/operators';
import { Employee } from 'src/app/interfaces/main.interfaces';
import { ApiService } from 'src/app/services/api.service';
import { DataSharingService } from 'src/app/services/data-sharing.service';

@Component({
	selector: 'app-find-staff',
	templateUrl: './find-employee.component.html',
	styleUrls: ['./find-employee.component.css'],
})
export class FindEmployeeComponent implements OnInit {
	employees: Employee[];
	searchEmpSub: Subscription;
	isLoading: boolean = false;
	searchEmp = new FormControl('');
	constructor(
		private dataSharingService: DataSharingService,
		private router: Router,
		private route: ActivatedRoute,
		private apiService: ApiService
	) {}

	ngOnInit(): void {
		this.dataSharingService.activateResources(false);
		this._filterEmp();
	}

	reRoute() {
		this.router.navigate(['../'], { relativeTo: this.route });
	}
	displayEmp(employee: Employee) {
		const employeeSelected = !!(
			employee &&
			employee.firstName &&
			employee.lastName &&
			employee.associateId
		);
		return employeeSelected ? employee.firstName + ' ' + employee.lastName : '';
	}

	private _filterEmp() {
		this.searchEmpSub = this.searchEmp.valueChanges
			.pipe(
				debounceTime(300),
				distinctUntilChanged(),
				tap(() => (this.isLoading = true)),
				switchMap((searchTerm: string) =>
					this.apiService
						.searchEmployees(searchTerm)
						.pipe(finalize(() => (this.isLoading = false)))
				)
			)
			.subscribe((employees) => (this.employees = employees));
	}
	selectedEmp(empSelectedEvent: MatAutocompleteSelectedEvent) {
		const staff = empSelectedEvent.option.value;
		this.dataSharingService.setEmployee(staff);
	}
}
