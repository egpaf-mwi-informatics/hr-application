import { AfterContentInit, Component, OnInit } from '@angular/core';
import {
  FormArray,
  FormBuilder,
  FormControl,
  FormGroup,
  NgForm,
  Validators,
} from '@angular/forms';
import {
  Position,
  Department,
  Employee,
  Office,
  Dependant,
  DependantType,
  Status,
} from '../../interfaces/main.interfaces';
import {
  debounceTime,
  distinctUntilChanged,
  finalize,
  switchMap,
  takeUntil,
  tap,
} from 'rxjs/operators';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute, Router } from '@angular/router';
import { DataSharingService } from 'src/app/services/data-sharing.service';
import { ApiService } from 'src/app/services/api.service';
import * as moment from 'moment';
import { MatDatepickerInputEvent } from '@angular/material/datepicker';
import { NotificationDialog } from 'src/app/shared/notification-dialog/notification-dialog';
import { requireSelection } from 'src/app/shared/directives/form-validators.directive';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { DestroyService } from 'src/app/services/destroy.service';
import { Papa, ParseResult } from 'ngx-papaparse';

@Component({
  selector: 'app-register-staff',
  templateUrl: './employee.component.html',
  styleUrls: ['./employee.component.css'],
})
export class EmployeeComponent implements OnInit, AfterContentInit {
  employeeForm: FormGroup;
  dependantForm: FormGroup;
  isLinear = true;
  dependantAdded = false;
  title = 'Register new employee';
  employee: Employee;
  fileName = '';
  fileData: ParseResult;

  // Table attributes
  displayedColumns: string[] = [
    'firstName',
    'lastName',
    'dateOfBirth',
    'nationalId',
    'action',
  ];
  dataSource = new MatTableDataSource<Dependant>();

  // spinner properties
  positionLoading: boolean = false;
  deptLoading: boolean = false;
  officeLoading: boolean = false;
  relationTypeLoading: boolean = false;
  statusLoading: boolean = false;

  // Generate email for this endividual
  initial: string = '';

  // list of objects
  positions = [];
  offices: Office[];
  departments: Department[];
  dependantRelations: DependantType[];
  statuses: Status[];

  sexes = ['Female', 'Male'];

  add = true;
  addBtnId = this.add ? 'add' : 'update';
  addBtnTxt = this.add ? 'Add dependent' : 'Update dependent';

  maxBirthDate = moment().subtract(18, 'y');
  maxContractEndDate = moment().add(1, 'y');

  constructor(
    private papa: Papa,
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private readonly destroy$: DestroyService,
    private dataSharingService: DataSharingService,
    private apiService: ApiService,
    private snackBar: MatSnackBar,
    public dialog: MatDialog
  ) {
    this.dataSharingService.employee$
      .pipe(takeUntil(this.destroy$))
      .subscribe((employee) => (this.employee = employee));
  }
  ngOnInit(): void {
    // get rid of the resource controls
    this.dataSharingService.activateResources(false);
    this.employeeForm = this.formBuilder.group({
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      sex: ['', Validators.required],
      dateOfBirth: [{ value: '', disabled: true }, Validators.required],
      email: [''],
      associateId: ['', Validators.required],
      contract: this.formBuilder.group({
        startDate: [{ value: '', disabled: true }, Validators.required],
        endDate: [{ value: '', disabled: true }, Validators.required],
        status: ['', Validators.required],
        position: ['', [Validators.required, requireSelection()]],
        office: ['', [Validators.required, requireSelection()]],
        department: ['', [Validators.required, requireSelection()]],
      }),
      dependants: this.formBuilder.array([]),
    });
    this.dependantForm = this.formBuilder.group({
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      dateOfBirth: [{ value: null, disabled: true }, Validators.required],
      nationalId: ['', Validators.required],
      relationType: ['', [Validators.required, requireSelection]],
    });

    this.dependantsFormArray.valueChanges
      .pipe(takeUntil(this.destroy$))
      .subscribe(
        () => (this.dataSource.data = this.dependantsFormArray.getRawValue())
      );

    this._filterDepartments();
    this._filterOffices();
    this._filterPositions();
    this._filtterRelationTypes();
    this._filterStatuses();
  }
  // To continue from here
  ngAfterContentInit(): void {
    const url = this.route.snapshot.url.toString().split(',')[1];

    switch (url) {
      case 'renew':
        this.renewInit();
        break;
      case 'add-dependant':
        this.addDependantInit();
        break;
      case 'edit':
        this.editInit();
        break;
      default:
        this.addEmployeeInit();
        break;
    }
  }

  // Include
  onFileSelected(event) {
    const file: File = event.target.files[0];

    if (file) {
      this.fileName = file.name;
      if (file.name.endsWith('.csv') || file.name.endsWith('.xlsx')) {
        let csvData = file;
        let options = {
          complete: (results, file) => {
            this.fileData = results;
          },
        };
        this.papa.parse(csvData, options);
      } else {
        console.log('Only .csv files are allowed for upload');
      }
    }
  }

  submitBulkEmployees() {
    if (this.fileData.data) {
      this.apiService
        .addEmployees(this.fileData)
        .subscribe((val) => console.log('Upload successful'));
    }
  }

  // Init functions
  private addEmployeeInit() {
    this.f.email.disable();
    this.f.lastName.valueChanges.subscribe((lastName: string) => {
      const firstName: string = this.f.firstName.value;
      const initial: string = firstName.trim().toLowerCase().charAt(0);
      if (this.f.firstName.dirty && this.f.lastName.dirty) {
        this.f.email.patchValue(
          initial + lastName.toLowerCase() + '@pedaids.org'
        );
      }
    });

    this.f.firstName.valueChanges.subscribe((firstName: string) => {
      const lastNameValue: string = this.f.lastName.value;
      const lastName = lastNameValue.trim().toLowerCase();
      const initial = firstName.trim().toLowerCase().charAt(0);
      if (this.f.firstName.dirty && this.f.lastName.dirty) {
        this.f.email.patchValue(initial + lastName + '@pedaids.org');
      }
    });
  }
  private renewInit(): void {
    this.dependantForm.disable();
    this.title =
      'Renew ' +
      this.employee?.firstName +
      ' ' +
      this.employee?.lastName +
      "'s Contract";
    this.employeeForm.patchValue(this.employee);
    this.employeeForm.disable();
    this.contractForm.enable();
    this.contractCtrls.startDate.disable();
    this.contractCtrls.endDate.disable();
  }
  private addDependantInit(): void {
    this.employeeForm.disable();
    this.title =
      'Add/Remove ' +
      this.employee?.firstName +
      ' ' +
      this.employee?.lastName +
      "'s Dependents";
    this.employeeForm.patchValue(this.employee);
  }
  private editInit(): void {
    this.contractForm.disable();
    this.dependantForm.disable();
    this.f.email.disable();
    this.title =
      'Update ' +
      this.employee?.firstName +
      ' ' +
      this.employee?.lastName +
      "'s Personal Details";
    this.employeeForm.patchValue(this.employee);
  }

  // Beneficiary action buttons from the table
  editDependentInfo(id: number) {
    const dependentToBeEdited = this.dataSource.data.filter(
      (dependent) => (dependent.id = id)
    )[0];
    this.dependantForm.patchValue({
      firstName: dependentToBeEdited.firstName,
      lastName: dependentToBeEdited.lastName,
      dateOfBirth: dependentToBeEdited.dateOfBirth,
      nationalId: dependentToBeEdited.nationalId,
      relationTyppe: dependentToBeEdited.relationType,
    });
  }

  deleteDependent(id: number) {
    this.dataSource.data = this.dataSource.data.filter(
      (row) => !(row.id === id)
    );
  }

  // Send data to backend
  public submit(empTemplateForm: NgForm) {
    const url = this.route.snapshot.url.toString().split(',')[1];
    const employeeFormData = this.employeeForm.getRawValue() as Employee;
    if (this.employeeForm.valid) {
      switch (url) {
        case 'renew':
          const renewData = {
            associateId: employeeFormData.associateId,
            contractId: this.employee.contract.contractId,
            positionId: employeeFormData.contract.position.positionId,
            status: employeeFormData.contract.status.id,
            departmentId: employeeFormData.contract.department.departmentId,
            officeId: employeeFormData.contract.office.officeId,
            startDate: moment(employeeFormData.contract.startDate).isValid()
              ? moment(employeeFormData.contract.startDate).format('YYYY-MM-DD')
              : '',
            endDate: moment(employeeFormData.contract.endDate).isValid()
              ? moment(employeeFormData.contract.endDate).format('YYYY-MM-DD')
              : '',
          };
          this.apiService.renewContract(renewData).subscribe(
            () => {
              empTemplateForm.resetForm();
              this.employeeForm.markAsPristine();
              this.successMessage('Successfully renewed contract', 'Success');
              this.router.navigate(['../'], { relativeTo: this.route });
            },
            (err) => {
              this.openDialog(err);
            }
          );
          break;
        case 'add-dependant':
          if (employeeFormData.dependants.length > 0) {
            const dependantData = {
              associateId: employeeFormData.associateId,
              dependants: employeeFormData.dependants,
            };
            this.apiService.addDependants(dependantData).subscribe(
              (resp) => {
                this.successMessage('Successfully added dependant', 'Success');
                empTemplateForm.resetForm();
                this.employeeForm.markAsPristine();
                this.router.navigate(['../'], { relativeTo: this.route });
              },
              (err) => {
                this.openDialog(err);
              }
            );
          } else {
            return;
          }

          break;
        case 'edit':
          const editData = {
            oldAssociateId: this.employee.associateId,
            associateId: employeeFormData.associateId,
            firstName: employeeFormData.firstName,
            lastName: employeeFormData.lastName,
            email: employeeFormData.email,
            sex: employeeFormData.sex,
            dateOfBirth: moment(employeeFormData.dateOfBirth).isValid()
              ? moment(employeeFormData.dateOfBirth).format('YYYY-MM-DD')
              : '',
          };
          this.apiService.editEmployee(editData).subscribe(
            (resp) => {
              empTemplateForm.resetForm();
              this.employeeForm.markAsPristine();
              this.successMessage('Successfully updated employee', 'Success');
              this.router.navigate(['../'], { relativeTo: this.route });
            },
            (err) => {
              this.openDialog(err);
            }
          );
          break;

        default:
          const employeeBackendData = {
            associateId: employeeFormData.associateId,
            firstName: employeeFormData.firstName,
            lastName: employeeFormData.lastName,
            email: employeeFormData.email,
            sex: employeeFormData.sex,
            dateOfBirth: moment(employeeFormData.dateOfBirth).format(
              'YYYY-MM-DD'
            ),
            dependants: employeeFormData.dependants,
            contract: {
              startDate: employeeFormData.contract.startDate,
              endDate: employeeFormData.contract.endDate,
              status: employeeFormData.contract.status.id,
              officeId: employeeFormData.contract.office.officeId,
              positionId: employeeFormData.contract.position.id,
              departmentId: employeeFormData.contract.department.departmentId,
            },
          };
          this.apiService.addEmployee(employeeBackendData).subscribe(
            (resp) => {
              empTemplateForm.resetForm();
              this.employeeForm.reset();
              this.addEmployeeInit();
              this.successMessage('Successfully added employee', 'Success');
            },
            (err) => {
              this.openDialog(err);
            }
          );
          break;
      }
    } else {
      this.openDialog({
        message:
          'Please make sure all the fields are filled with the correct values',
      });
      this.employeeForm.markAllAsTouched();
      this.employeeForm.markAsDirty();
    }
  }

  private openDialog(err: any): void {
    const dialogRef = this.dialog.open(NotificationDialog, {
      width: '20rem',
      data: { title: err.title, message: err.message },
    });
    dialogRef.afterClosed().subscribe((result) => {
      console.log(result);
    });
  }
  successMessage(message: string, action: string) {
    this.snackBar.open(message, action, { duration: 2000 });
  }

  // The event will be used to switch the function of the button when adding the dependant
  public addDependant(depTemplateForm: NgForm, event) {
    const dependantFormData = this.dependantForm.getRawValue() as Dependant;
    if (
      this.dependantForm.valid &&
      moment(dependantFormData.dateOfBirth).isValid()
    ) {
      // changed from let to const
      const tempForm = new FormGroup({
        firstName: new FormControl(''),
        lastName: new FormControl(''),
        dateOfBirth: new FormControl(''),
        nationalId: new FormControl(''),
      });

      const firstTemp = this.dependantForm.getRawValue();
      const secondTemp = {
        ...firstTemp,
        ...{ dateOfBirth: moment(firstTemp.dateOfBirth).format('YYYY-MM-DD') },
      };
      tempForm.patchValue(secondTemp);

      this.dependantsFormArray.push(tempForm);
      this.dependantForm.reset();
      depTemplateForm.resetForm();
      this.dependantForm.markAsUntouched();
      this.dependantForm.markAsPristine();
    } else {
      // To-Do: All fields are required dialog
    }
  }

  // A more corrent naming would be clearDependents() as it clears the added dependents
  public delDependants(): void {
    this.dependantsFormArray.clear();
  }

  public get f() {
    return this.employeeForm.controls;
  }
  private get contractForm() {
    return this.f.contract as FormGroup;
  }

  private get dependantsFormArray() {
    return this.f.dependants as FormArray;
  }
  public get contractCtrls() {
    return this.contractForm.controls;
  }

  public get dependantCtrls() {
    return this.dependantForm.controls;
  }

  setMaxDate(event: MatDatepickerInputEvent<Date>) {
    this.route.url.subscribe((urlPath) => {
      const path = urlPath[1].path;
      switch (path) {
        case 'add-employee':
          this.maxContractEndDate = moment(event.value).add(1, 'y');
          break;
        default:
          this.maxContractEndDate = moment(event.value).add(2, 'y');
          break;
      }
    });
  }

  // Filters to be re-implemented
  private _filterDepartments() {
    this.contractCtrls.department.valueChanges
      .pipe(
        debounceTime(300),
        distinctUntilChanged(),
        tap(() => (this.deptLoading = true)),
        switchMap((searchTerm) =>
          this.apiService
            .findDepartments(searchTerm)
            .pipe(finalize(() => (this.deptLoading = false)))
        ),
        takeUntil(this.destroy$)
      )
      .subscribe((dept) => (this.departments = dept));
  }
  displayDept(dept: Department) {
    const departmentSelected = !!(dept && dept.departmentName);
    return departmentSelected ? dept.departmentName : '';
  }

  private _filtterRelationTypes() {
    this.dependantCtrls.relationType.valueChanges
      .pipe(
        debounceTime(300),
        distinctUntilChanged(),
        tap(() => (this.relationTypeLoading = true)),
        switchMap((searchTerm) =>
          this.apiService
            .findRelations(searchTerm)
            .pipe(finalize(() => (this.relationTypeLoading = false)))
        ),
        takeUntil(this.destroy$)
      )
      .subscribe((relationTypes) => (this.dependantRelations = relationTypes));
  }

  displayRelationTypeFn(dependantType: DependantType) {
    const typeSelected = !!(dependantType && dependantType.relationType);
    return typeSelected ? dependantType.relationType : '';
  }

  private _filterPositions() {
    this.contractCtrls.position.valueChanges
      .pipe(
        debounceTime(300),
        distinctUntilChanged(),
        tap(() => (this.positionLoading = true)),
        switchMap((searchTerm) =>
          this.apiService
            .findPositions(searchTerm)
            .pipe(finalize(() => (this.positionLoading = false)))
        ),
        takeUntil(this.destroy$)
      )
      .subscribe((positions) => (this.positions = positions));
  }
  displayPosition(position: Position) {
    const positionSelected = !!(position && position.positionName);
    return positionSelected ? position.positionName : '';
  }

  private _filterOffices() {
    this.contractCtrls.office.valueChanges
      .pipe(
        debounceTime(300),
        distinctUntilChanged(),
        tap(() => (this.officeLoading = true)),
        switchMap((searchTerm) =>
          this.apiService
            .findOffices(searchTerm)
            .pipe(finalize(() => (this.officeLoading = false)))
        ),
        takeUntil(this.destroy$)
      )
      .subscribe((offices) => (this.offices = offices));
  }
  displayOffice(office: Office) {
    const officeSelected = !!(office && office.officeName);
    return officeSelected ? office.officeName : '';
  }

  private _filterStatuses() {
    this.contractCtrls.status.valueChanges
      .pipe(
        debounceTime(300),
        distinctUntilChanged(),
        tap(() => (this.statusLoading = true)),
        switchMap((searchTerm) =>
          this.apiService
            .findStatuses(searchTerm)
            .pipe(finalize(() => (this.statusLoading = false)))
        ),
        tap((val) => console.log(val)),
        takeUntil(this.destroy$)
      )
      .subscribe((statuses: Status[]) => (this.statuses = statuses));
  }

  displayStatus(status: Status) {
    const statusSelected = !!(status && status.status);
    return statusSelected ? status.status : '';
  }
}
