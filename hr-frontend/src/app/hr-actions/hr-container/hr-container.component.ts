import { Component, OnInit } from '@angular/core';
import { MatButtonToggleGroup } from '@angular/material/button-toggle';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { DataSharingService } from 'src/app/services/data-sharing.service';

@Component({
	selector: 'app-hr-container',
	templateUrl: './hr-container.component.html',
	styleUrls: ['./hr-container.component.css'],
})
export class HrContainerComponent implements OnInit {
	resourceMgmtActivated: Observable<boolean>;
	employeeMgmtActivated: Observable<boolean>;

	constructor(
		private router: Router,
		private dataSharingService: DataSharingService,
		private route: ActivatedRoute
	) {}

	ngOnInit(): void {
		this.resourceMgmtActivated = this.dataSharingService.resourceMgmtActvated;
		this.employeeMgmtActivated = this.dataSharingService.employeeMgmtActivated$;
	}

	employeeMgmt(group: MatButtonToggleGroup) {
		this.dataSharingService.activateEmployees(true);
		this.router.navigate([
			`home/hr-actions/employee-mngmt/${group.value}/find`,
		]);
	}

	activateEmployeeResource(val: boolean) {
		this.router.navigate([{ outlets: { primary: null } }], {
			skipLocationChange: true,
			relativeTo: this.route.parent,
		});
		this.dataSharingService.activateResources(!val);
		this.dataSharingService.activateEmployees(val);
	}

	addEmployee() {
		this.dataSharingService.activateEmployees(true);
		this.router.navigate(['home/hr-actions/employee-mngmt/add-employee']);
	}

	manageResources(group: MatButtonToggleGroup) {
		this.dataSharingService.activateResources(true);
		this.router.navigate([`home/hr-actions/resource-mngmt/${group.value}`]);
	}
	activateResourceControls(val: boolean) {
		this.dataSharingService.activateEmployees(!val);
		this.dataSharingService.activateResources(val);
		this.router.navigate(['home/hr-actions']);
	}
}
