import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, NgForm } from '@angular/forms';
import { MatAutocompleteSelectedEvent } from '@angular/material/autocomplete';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import {
	debounceTime,
	distinctUntilChanged,
	finalize,
	switchMap,
	tap,
} from 'rxjs/operators';
import {
	Department,
	Office,
	Position,
	Project,
} from 'src/app/interfaces/main.interfaces';
import { ApiService } from 'src/app/services/api.service';
import { DataSharingService } from 'src/app/services/data-sharing.service';

@Component({
	selector: 'app-resource-management',
	templateUrl: './resource-management.component.html',
	styleUrls: ['./resource-management.component.css'],
})
export class ResourceManagementComponent implements OnInit, OnDestroy {
	searchText = new FormControl('');
	searchTextLabel: string;
	header: string;
	searchSub: Subscription;
	isLoading = false;
	inputLabel: string;
	isResourceUpdate: boolean;

	options = [];
	routeId: string;

	// Used to store searched parameters
	department: Department;
	office: Office;
	project: Project;
	position: Position;

	resourceForm: FormGroup;

	constructor(
		private formBuilder: FormBuilder,
		private apiService: ApiService,
		private route: ActivatedRoute,
		private dataSharingService: DataSharingService
	) {
		this.dataSharingService.activateResources(true);
		this.isResourceUpdate = false;
	}

	ngOnInit(): void {
		this.route.params.subscribe((param) => {
			this.routeId = param.id;
			this.initComponent();
		});
	}

	initComponent() {
		this.header =
			'Manage ' +
			this.routeId.charAt(0).toUpperCase() +
			this.routeId.slice(1) +
			's';
		this.searchTextLabel = 'Find ' + this.routeId + ' to update';
		this.inputLabel = 'Add new ' + this.routeId;

		switch (this.routeId) {
			case 'department':
				this.resourceForm = this.formBuilder.group({
					departmentName: [''],
				});
				this.searchSub = this.searchText.valueChanges
					.pipe(
						debounceTime(300),
						distinctUntilChanged(),
						tap(() => (this.isLoading = true)),
						switchMap((searchTerm: string) =>
							this.apiService
								.findDepartments(searchTerm)
								.pipe(finalize(() => (this.isLoading = false)))
						)
					)
					.subscribe((departments) => (this.options = departments));
				break;
			case 'office':
				this.resourceForm = this.formBuilder.group({
					officeName: [''],
				});
				this.searchSub = this.searchText.valueChanges
					.pipe(
						debounceTime(300),
						distinctUntilChanged(),
						tap(() => (this.isLoading = true)),
						switchMap((searchTerm: string) =>
							this.apiService
								.findOffices(searchTerm)
								.pipe(finalize(() => (this.isLoading = false)))
						)
					)
					.subscribe((offices) => (this.options = offices));
				break;
			case 'position':
				this.resourceForm = this.formBuilder.group({
					positionName: [''],
				});
				this.searchSub = this.searchText.valueChanges
					.pipe(
						debounceTime(300),
						distinctUntilChanged(),
						tap(() => (this.isLoading = true)),
						switchMap((searchTerm: string) =>
							this.apiService
								.findPositions(searchTerm)
								.pipe(finalize(() => (this.isLoading = false)))
						)
					)
					.subscribe((positions) => (this.options = positions));
				break;
			case 'project':
				this.resourceForm = this.formBuilder.group({
					projectName: [''],
				});
				this.searchSub = this.searchText.valueChanges
					.pipe(
						debounceTime(300),
						distinctUntilChanged(),
						tap(() => (this.isLoading = true)),
						switchMap((searchTerm: string) =>
							this.apiService
								.findProjects(searchTerm)
								.pipe(finalize(() => (this.isLoading = false)))
						)
					)
					.subscribe((projects) => (this.options = projects));
				break;
		}
	}

	displayResource(resource: any) {
		switch (this.routeId) {
			case 'department':
				return resource.departmentName ? resource.departmentName : '';
			case 'position':
				return resource.positionName ? resource.positionName : '';
			case 'office':
				return resource.officeName ? resource.officeName : '';
			case 'project':
				return resource.projectName ? resource.projectName : '';
		}
	}

	selectedOption(optionSelectedEvent: MatAutocompleteSelectedEvent) {
		this.isResourceUpdate = true;
		switch (this.routeId) {
			case 'department':
				this.department = optionSelectedEvent.option.value;
				break;
			case 'position':
				this.position = optionSelectedEvent.option.value;
				break;
			case 'office':
				this.office = optionSelectedEvent.option.value;
				break;
			case 'project':
				this.project = optionSelectedEvent.option.value;
				break;
		}
	}

	onSubmit(tempForm: NgForm) {
		const newData = this.resourceForm.value;

		if (this.resourceForm.valid) {
			switch (this.routeId) {
				case 'department':
					if (!this.isResourceUpdate) {
						this.apiService
							.addDepartment(newData)
							.subscribe((val) => console.log(val));
					} else {
						const updateData = {
							...this.department,
							...this.resourceForm.value,
						};
						this.apiService
							.updateDepartment(updateData)
							.subscribe((val) => console.log(val));
					}
					break;
				case 'position':
					if (!this.isResourceUpdate) {
						this.apiService
							.addPosition(newData)
							.subscribe((val) => console.log(val));
					} else {
						const updateData = { ...this.position, ...this.resourceForm.value };
						this.apiService
							.updatePosition(updateData)
							.subscribe((val) => console.log(val));
					}
					break;
				case 'office':
					if (!this.isResourceUpdate) {
						this.apiService
							.addOffice(newData)
							.subscribe((val) => console.log(val));
					} else {
						const updateData = { ...this.office, ...this.resourceForm.value };
						this.apiService
							.updateOffice(updateData)
							.subscribe((val) => console.log(val));
					}
					break;
				case 'project':
					if (!this.isResourceUpdate) {
						this.apiService
							.addProject(newData)
							.subscribe((val) => console.log(val));
					} else {
						const updateData = { ...this.project, ...this.resourceForm.value };
						this.apiService
							.updateProject(updateData)
							.subscribe((val) => console.log(val));
					}
					break;
			}
			this.resourceForm.reset();
			tempForm.resetForm();
		} else {
			console.log('Not Valid form');
		}
	}

	ngOnDestroy(): void {
		this.dataSharingService.activateResources(false);
		this.searchSub.unsubscribe();
	}
}
