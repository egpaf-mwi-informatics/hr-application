import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AuthModule } from './auth/auth.module';
import { NavModule } from './nav/nav.module';
import { MAT_DATE_FORMATS } from '@angular/material/core';
import {
	MatMomentDateModule,
	MAT_MOMENT_DATE_ADAPTER_OPTIONS,
} from '@angular/material-moment-adapter';
import { ApiMiddlewareInterceptor } from './api-middleware.interceptor';
import { PipModule } from './pip/pip.module';
import { ItActionsModule } from './it-actions/it-actions.module';
import { MAT_DIALOG_DEFAULT_OPTIONS } from '@angular/material/dialog';
import { APP_BASE_HREF } from '@angular/common';

// Formats for momentjs
const MY_FORMATS = {
	parse: {
		dateInput: 'YYYY-MM-DD',
	},
	display: {
		dateInput: 'LL',
		monthYearLabel: 'MMM YYYY',
		dateA11yLabel: 'LL',
		monthYearA11yLabel: 'MMMM YYYY',
	},
};
// Moment has to be imported at root module
// Documentation for momentjs use in angular material is described in https://material.angular.io/components/datepicker/overview 

@NgModule({
	declarations: [AppComponent],
	imports: [
		BrowserModule,
		AppRoutingModule,
		HttpClientModule,
		BrowserAnimationsModule,
		MatMomentDateModule,
		AuthModule,
		NavModule,
		PipModule,
		ItActionsModule,
	],
	providers: [
		{
			provide: HTTP_INTERCEPTORS,
			useClass: ApiMiddlewareInterceptor,
			multi: true,
		},
		{ provide: APP_BASE_HREF, useValue: '/hr' },
		{ provide: MAT_DIALOG_DEFAULT_OPTIONS, useValue: { hasBackdrop: false } },
		{ provide: MAT_DATE_FORMATS, useValue: MY_FORMATS },
		{ provide: MAT_MOMENT_DATE_ADAPTER_OPTIONS, useValue: { useUtc: true } },
		MatMomentDateModule,
	],

	bootstrap: [AppComponent],
})
export class AppModule {}
