import { APP_BASE_HREF } from '@angular/common';
import { HttpErrorResponse } from '@angular/common/http';
import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { tap } from 'rxjs/operators';
import { Auth } from 'src/app/interfaces/main.interfaces';
import { ApiService } from 'src/app/services/api.service';
import { NotificationDialog } from 'src/app/shared/notification-dialog/notification-dialog';

@Component({
	selector: 'app-login',
	templateUrl: './login.component.html',
	styleUrls: ['./login.component.css'],
})
export class LoginComponent implements OnInit {
	loginForm: FormGroup;
	baseHrefInstance: string;
	isLoading: boolean;

	constructor(
		private formBuilder: FormBuilder,
		private router: Router,
		private apiService: ApiService,
		public dialog: MatDialog,
		@Inject(APP_BASE_HREF) private baseHref: string
	) {
		this.baseHrefInstance = this.baseHref;
	}

	ngOnInit(): void {
		this.isLoading = false;
		this.loginForm = this.formBuilder.group({
			email: [
				'',
				[
					Validators.required,
					Validators.email,
					Validators.pattern(/pedaids.org/),
				],
			],
			password: ['', Validators.required],
		});
	}

	public get f() {
		return this.loginForm.controls;
	}

	public login() {
		if (this.loginForm.valid) {
			this.isLoading = true;
			this.apiService
				.login(this.loginForm.value)
				.pipe(tap(() => (this.isLoading = false)))
				.subscribe(
					(authData:Auth) => {
						const userRole = this.apiService.getUserRole(authData.data);
						if (userRole === 'itAccess') {
							this.router.navigate(['home/it-actions']);
						} else if (['adminAccess', 'hrAccess'].includes(userRole)) {
							this.router.navigate(['home']);
						} else if (authData.data.isSupervisor) {
							this.router.navigate(['home/pip-mngmt']);
						}
					},
					(err) => this.openDialog(err)
				);
		}
	}

	private openDialog(err: HttpErrorResponse): void {
		switch (err.status) {
			case 500:
				this.dialog.open(NotificationDialog, {
					width: '20rem',
					data: {
						title: 'Server Down',
						message:
							'Please check your internet connection or contact the system administrator',
					},
				});
				break;
			case 401:
				this.dialog.open(NotificationDialog, {
					width: '20rem',
					data: {
						title: 'Unauthorized Acces',
						message: 'Please check your credentials, the ones provided are wrong',
					},
				});

				break;
			default:
				this.dialog.open(NotificationDialog, {
					width: '20rem',
					data: { title: err.message, message: err.message },
				});
				break;
		}
		this.dialog.afterAllClosed.subscribe(() => {
			this.isLoading = false;
		});
	}
}
