import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginComponent } from './login/login.component';
import { SharedModule } from '../shared/shared.module';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatTabsModule } from '@angular/material/tabs';
import { MatProgressBarModule } from '@angular/material/progress-bar';

@NgModule({
	declarations: [LoginComponent],
	imports: [
		CommonModule,
		SharedModule,
		MatTabsModule,
		MatFormFieldModule,
		MatInputModule,
		MatProgressBarModule,
	],
})
export class AuthModule {}
