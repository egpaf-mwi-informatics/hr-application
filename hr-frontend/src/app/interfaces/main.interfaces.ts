export interface Employee {
  associateId: string;
  firstName: string;
  lastName: string;
  dateOfBirth?: string;
  onPip?: boolean;
  pip?: PerformanceImprovementPlan[];
  sex?: string;
  email?: string;
  contract?: Contract;
  dependants?: Dependant[];
  distributionList?: DistributionList[];
}

export interface PerformanceImprovementPlan {
  contractId: number;
  endDate: string;
  startDate: string;
  id: number;
  pipComments?: string;
  progress?: number;
  pipSupervisorComments?: string;
  pipSecondLevelManagerComments?: string;
  pipHrComments?: string;
  priorities: PipPriority[];
}

export interface PipPriority {
  priorityEndDate: string;
  priorityName: string;
  priorityStartDate: string;
  priorityStatus: string;
}

export interface NewEmployees {
  [associateId: string]: Employee | Function;
  getValues(): Employee[];
  delValue(associateId: string): Employee[];
}

export interface ConfirmedEmployee {
  associateId: string;
  email: string;
  distributionListIds: number[];
}

export interface Contract {
  contractId?: number;
  employeeId?: string;
  status: Status;
  startDate: string;
  endDate: string;
  position: Position;
  office: Office;
  department: Department;
}

export interface Status {
  id: number;
  status: string;
}

export interface Dependant {
  id?: number;
  employeeId?: string;
  firstName: string;
  lastName: string;
  dateOfBirth: string;
  nationalId?: string;
  relationType?: number;
}

export interface DependantType {
  id: number;
  relationType: string;
}

export interface Position {
  positionId?: number;
  id?: number;
  positionName: string;
  supervirsor?: boolean;
}

export interface Office {
  officeId?: number;
  officeName: string;
}

export interface Department {
  departmentId?: number;
  departmentName: string;
}

export interface Project {
  id: number;
  projectName: string;
}

export interface DistributionList {
  groupId: number;
  groupName: string;
  groupDescription: string;
  groupEmail: string;
}

export interface SelectableAttributes {
  position: Position;
  department: Department;
  office: Office;
}

export interface Credentials {
  email: string;
  password: string;
}

export interface Auth {
  data: User;
  message: string;
  status: number;
  token: string;
}

export interface User {
  associateId: string;
  fistname: string;
  lastname: string;
  email: string;
  contractId: number;
  positionId: number;
  positionName: string;
  isSupervisor?: boolean;
}

export interface Supervisor {
  positionId: number;
  positionName: string;
  subordinates: Employee[];
  supervirsor: boolean;
}
