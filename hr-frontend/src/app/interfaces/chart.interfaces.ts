import {
	ApexAxisChartSeries,
	ApexChart,
	ApexDataLabels,
	ApexXAxis,
	ApexPlotOptions,
	ApexNonAxisChartSeries,
	ApexResponsive,
	ApexYAxis,
	ApexGrid,
	ApexLegend,
	ApexTitleSubtitle,
	ApexStroke,
	ApexTooltip,
	ApexFill,
} from 'ng-apexcharts';

// Ending contracts
export type EndingContractOptions = {
	series: ApexAxisChartSeries;
	chart: ApexChart;
	title: ApexTitleSubtitle;
	dataLabels: ApexDataLabels;
	plotOptions: ApexPlotOptions;
	xaxis: ApexXAxis;
	tooltip:ApexTooltip
};


export interface IData {
	count: number[];
	labels: string[];
}

// Gender Distribution
export type GenderOptions = {
	series: ApexNonAxisChartSeries;
	chart: ApexChart;
	responsive: ApexResponsive[];
	labels: string[];
	legend: ApexLegend;
	title: ApexTitleSubtitle;
};

// Employee per District
export type EmpPerDistOptions = {
	series: ApexAxisChartSeries;
	chart: ApexChart;
	dataLabels: ApexDataLabels;
	plotOptions: ApexPlotOptions;
	legend: ApexLegend;
	title: ApexTitleSubtitle;
	colors: string[];
};
export interface EmpPerDistData {
	name?: string;
	data: TotalPerDistrict[];
}
export interface TotalPerDistrict {
	x: string;
	y: number;
}

// Employee per departments
export type EmpPerDeptOptions = {
	series: ApexAxisChartSeries;
	chart: ApexChart;
	dataLabels: ApexDataLabels;
	plotOptions: ApexPlotOptions;
	yaxis: ApexYAxis;
	xaxis: ApexXAxis;
	grid: ApexGrid;
	colors: string[];
	legend: ApexLegend;
	title: ApexTitleSubtitle;
};

export type EmpPerDisPerDepOptions = {
	series: ApexAxisChartSeries;
	chart: ApexChart;
	dataLabels: ApexDataLabels;
	plotOptions: ApexPlotOptions;
	xaxis: ApexXAxis;
	yaxis: ApexYAxis;
	stroke: ApexStroke;
	title: ApexTitleSubtitle;
	tooltip: ApexTooltip;
	fill: ApexFill;
	legend: ApexLegend;
};

export interface EmpPerDisPerDepData {
	series: Department[];
	categories: string[];
}

export interface Department {
	name: string;
	data: number[];
}
