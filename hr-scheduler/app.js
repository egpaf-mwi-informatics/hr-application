/*
|-------------------------------------------|
|SOFTWARE ENGINEER: Robin Brian Mkuwira     |
|Date Created: 08/06/2021                   |
|Project Name: Human Resources Application  |
|                                           |
|This app.js file is used to run the scripts|
|that automates the sending of emails and   |
|the updating of the HR application database| 
|-------------------------------------------|
*/


// getting the required packages
const schedule = require('node-schedule') 
const { default: axios }  = require('axios');

// run the job at 7:30 everyday
schedule.scheduleJob('30 5 * * *', () => {
  endContractCheck()
  dependentsCheck()
  birthdayCheck()

  // triggers the back-end to make updates to the database
  // pipUpdate()
  dependentsUpdate()
  contractUpdate()
  birthdayMonthNotification()
  anniversaryNotification()
})


// run at 11:30 every day
schedule.scheduleJob('30 9 * * *', () => {
  endContractCheck()
  dependentsCheck()
  birthdayCheck()

  // triggers the back-end to make updates to the database
  // pipUpdate()
  dependentsUpdate()
  contractUpdate()
  birthdayMonthNotification()
  anniversaryNotification()
})



/*
|--------------------------------------------|
|Functions for triggering sending emails to  |
|supervirsors, the HR and the employee.      | 
|--------------------------------------------|
*/



// trigers the sending of end of contract emails
function endContractCheck() {
  const URL = 'http://hr-backend:7200/employee/endContractCheck'
  axios.get(URL)
    .then((response) => {
    })
    .catch((error) => {
      console.log(error)
    })
}



// trigers the sending of end of masm support for dependents
function dependentsCheck() {
  const URL = 'http://hr-backend:7200/dependant/dependentsCheck'
  axios.post(URL)
    .then((response) => {
    })
    .catch((error) => {
      console.log(error)
    })
}


// triggers the sending of birthday emails
function birthdayCheck() {
  const URL = 'http://hr-backend:7200/employee/birthdayCheck'
  axios.get(URL)
    .then((response) => {
    })
    .catch((error) => {
      console.log(error)
    })
}


/*
|--------------------------------------------|
|Functions for triggering the updates of the |
|dependents data, the contract data and the  |
|PIP data                                    | 
|--------------------------------------------|
*/

// triggers the update of the PIP data
function pipUpdate() {
  const URL = 'http://hr-backend:7200/pip/update'
  axios.post(URL)
    .then((response) => {
    })
    .catch((error) => {
      console.log(error)
    })
}


// triggers the update of the dependents data 
function dependentsUpdate() {
  const URL = 'http://hr-backend:7200/dependant/removeNonSupported'
  axios.post(URL)
    .then((response) => {
    })
    .catch((error) => {
      console.log(error)
    })
}


// triggers the update of the contract data
function contractUpdate() {
  const URL = 'http://hr-backend:7200/contract/contractUpdate'
  axios.get(URL)
    .then((response) => {
    })
    .catch((error) => {
      console.log(error)
    })
}


// triggers the birthday month notifications
function birthdayMonthNotification() {
  const URL = 'http://hr-backend:7200/employee/birthdayMonthCheck'
  axios.get(URL)
    .then((response) => {
    })
    .catch((error) => {
      console.log(error)
    })
}


// triggers the anniversary notifications
function anniversaryNotification() {
  const URL = 'http://hr-backend:7200/employee/anniversaryCheck'
  axios.get(URL)
    .then((response) => {
    })
    .catch((error) => {
      console.log(error)
    })
}

