/*
|---------------------------------------|
|Software Engineer: Robin B. MKuwira    |
|Date Created: 02/02/2021               |
|Project Name: HR Application           |
|                                       |
|The connection database module is used |
|to provide the connection pool for     |
|miltiple asynchronous connections to   |
|postgres database                      |
|---------------------------------------|
*/
const { Pool } = require('pg')

const pool = new Pool({
    user: process.env.PGUSER,
    host: process.env.PGHOST,
    database: process.env.PGDATABASE,
    password: process.env.PGPASSWORD,
    port: process.env.PGPORT
});


pool.on('error', (err, client) => {
    console.error('Error:', err);
});


// pool.connect();

module.exports = pool;