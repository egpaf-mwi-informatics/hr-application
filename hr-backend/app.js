/*
|-----------------------------------------|
|SOFTWARE ENGINEER: Robin Brian Mkuwira   |
|Date Created: 02/06/2021                 |
|Project Name: Human Resources Application|
|                                         |
|The app.js file is used to serve all     |
|of the requests that are made to this    |
|server                                   | 
|-----------------------------------------|
*/


//getting the required packages
const express = require("express");
const cors = require("cors");
const bodyParser = require("body-parser");
const morgan = require("morgan");
require('dotenv').config();


//initialializing an express app
const app = express();


//using the packages in e`xpress
app.use(cors());
app.use(bodyParser.json({limit:'100MB'}));
app.use(bodyParser.urlencoded({limit:'100MB', extended:true}))
app.use(morgan("tiny"));

//setting the base directory as a global variable
global.__basedir = __dirname;

//importing the routes
const employeeRoute = require("./routes/employee");
const contractRoute = require("./routes/contract");
const departmentRoute = require("./routes/department");
const dependantRoute = require("./routes/dependant");
const distributionListRoute = require('./routes/distribution_list');
const employeeDistributionRoute = require('./routes/employee_distribution');
const matrixRoute = require('./routes/matrix');
const officeRoute = require('./routes/office');
const positionRoute = require('./routes/position');
const dependantsRecordsRoute = require('./routes/dependants_records');
const pip = require('./routes/pip');
const project = require('./routes/project');
const birthdayNotification = require('./routes/birthday_notification');
const contractNotification = require('./routes/contract_notification');
const contractProject = require('./routes/contract_project');
const dependantRelation = require('./routes/dependant_relation');
const section = require('./routes/section');
const comment = require('./routes/comment');
const status = require('./routes/status');
const anniversaryType = require('./routes/anniversary_type');
const anniversary = require('./routes/anniversary');
const birthdayMonth = require('./routes/birthday_month');

//routing to various routes
app.use("/employee", employeeRoute);
app.use("/contract", contractRoute);
app.use("/department", departmentRoute);
app.use("/dependant", dependantRoute);
app.use("/distributionList", distributionListRoute);
app.use("/employeeDistribution", employeeDistributionRoute);
app.use("/matrix", matrixRoute);
app.use('/office', officeRoute);
app.use('/position', positionRoute);
app.use('/dependantsRecords', dependantsRecordsRoute);
app.use('/pip', pip);
app.use('/birthdayNotification', birthdayNotification);
app.use('/contractNotification', contractNotification);
app.use('/contractProject', contractProject);
app.use('/dependantRelation', dependantRelation);
app.use('/section', section);
app.use('/comment', comment);
app.use('/status', status)
app.use('/anniversaryType', anniversaryType)
app.use('/anniversary', anniversary)
app.use('/birthdayMonth', birthdayMonth)

//getting the port from the env
const port = process.env.PORT;
app.get('/', (req, res) =>{
    res.send('The HR application`s API');
})

//listening to a port
app.listen(port, ()=>{
    console.log(`The Server is running on port ${port}`);
});

