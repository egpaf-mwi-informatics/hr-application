/*
|-----------------------------------------------|
|SOFTWARE ENGINEER: Robin B. Mkuwira            |
|PROJECT NAME: HR Application                   |
|DATE: 02/06/2021                               |
|                                               |
|Description:                                   |
|Below is the code for the dependants model that|
|is used to read, write, update and delete the  |
|dependants table in the HR application database|
|system.                                        |
|-----------------------------------------------|
*/

// getting the psql database connection
var pool = require("../database/connection");

// finding all the dependants
function findAll() {

    return new Promise(async(resolve, reject) => {
        const psql= "SELECT * " +
                    "FROM dependant";

        pool.connect()
        .then(client => {
            return client
            .query(psql)
            .then(res => {
                client.release()
                return resolve(res.rows)
            })
            .catch(err => {
                client.release()
                return reject(err.stack)
            })
        })
    })
}





// finding all the dependants who are going to be over 21 years old
//  next month
function findToExpireNextMonth() {

    return new Promise(async(resolve, reject) => {
        // refactor to exclude spouses
        const psql= "SELECT DISTINCT(dependant.id) AS id, national_id_no, employee_tbl.first_name AS employee_firstname, employee_tbl.last_name AS employee_surname, dependant.date_of_birth,  department_name, office_name, associate_id AS employee_id, LOWER(dependant_relation.relation_type) AS relation_type, age, " +
                    "LOWER(dependant.first_name) AS first_name, email, LOWER(dependant.last_name) AS last_name, LOWER(TO_CHAR(TO_DATE (EXTRACT(MONTH FROM DATE_TRUNC('month', CURRENT_DATE) + INTERVAL '1 MONTH')::text, 'MM'), 'MONTH')) AS expiry_month, " +
                    "TO_CHAR(dependant.date_of_birth::DATE,'dd Month') AS exact_expiry_date " +
                    "FROM dependant " +
                    "JOIN dependant_relation " +
                    "ON (dependant.relation_type =  dependant_relation.id) " +  
                    "JOIN employee_tbl " +
                    "ON (employee_tbl.associate_id = dependant.employee_id) " +
                    "JOIN contract " +
                    "ON (employee_tbl.associate_id = contract.employee_id) " +
                    "JOIN sections " +
                    "ON (contract.section_id = sections.id) " +
                    "JOIN department " +
                    "ON (department.department_id = sections.dept_id) " +
                    "JOIN office " +
                    "ON (office.office_id = contract.office_id) " +
                    "WHERE EXTRACT(year from age((date_trunc('month', current_timestamp::date + interval '1 month') + interval '1 month' - interval '1 day')::date, dependant.date_of_birth)) >= 21 " +
                    "AND dependant_relation.relation_type NOT LIKE '%WIFE%' AND dependant_relation.relation_type NOT LIKE '%HUSBAND%'";

        pool.connect()
        .then(client => {
            return client
            .query(psql)
            .then(res => {
                client.release()
                return resolve(res.rows)
            })
            .catch(err => {
                client.release()
                return reject(err.stack)
            })
        })
    })
}









// finding all the non supported dependants over 21 years old
function findNonSupported() {

    return new Promise(async(resolve, reject) => {
        // refactor to exclude spouses
        const psql= "SELECT dependant.id AS id, national_id_no, date_of_birth, employee_id, dependant.relation_type AS relation_type, age, first_name, last_name " +
                    "FROM dependant " +
                    "JOIN dependant_relation " +
                    "ON (dependant.relation_type =  dependant_relation.id) " +
                    "WHERE EXTRACT(YEAR FROM AGE(date_of_birth)) >= 21 " +
                    "AND dependant_relation.relation_type NOT LIKE '%WIFE%' AND dependant_relation.relation_type NOT LIKE '%HUSBAND%'";

        pool.connect()
        .then(client => {
            return client
            .query(psql)
            .then(res => {
                client.release()
                return resolve(res.rows)
            })
            .catch(err => {
                client.release()
                return reject(err.stack)
            })
        })
    })
}


// finding a specific dependant by the national id
function find(nationalId) {
    return new Promise(async(resolve, reject) => {
        const psql= "SELECT * " +
                    "FROM dependant " +
                    "WHERE national_id_no = $1 ";
        const values = [nationalId]

        pool.connect()
        .then(client => {
            return client
            .query(psql, values)
            .then(res => {
                client.release()
                return resolve(res.rows)
            })
            .catch(err => {
                client.release()
                return reject(err.stack)
            })
        })
    })
}


// finding a specific contract by the employee id
function findByEmployeeId(employeeId) {
    return new Promise(async(resolve, reject) => {
        const psql= "SELECT * " +
                    "FROM dependant " +
                    "WHERE employeeId = $1 ";

        const values = [employeeId]

        pool.connect()
        .then(client => {
            return client
            .query(psql, values)
            .then(res => {
                client.release()
                return resolve(res.rows)
            })
            .catch(err => {
                client.release()
                return reject(err.stack)
            })
        })
    })
}

// inserting a dependant
function insert(nationalId, firstname, lastname, dateOfBirth, employeeId, masm_id) {
    return new Promise(async(resolve, reject) => {
        const psql = "INSERT INTO dependant " +
                    "(national_id_no, first_name, last_name, date_of_birth, employee_id, relation_type, masm_id)" +
                    "VALUES ($1, $2, $3, $4, $5) ";
        const values = [nationalId, firstname, lastname, dateOfBirth, employeeId]

        pool.connect()
        .then(client => {
            return client
            .query(psql, values)
            .then(res => {
                client.release()
                return resolve(res)
            })
            .catch(err => {
                client.release()
                return reject(err)
            })
        })
    })
}

// updating the matrix
function update(nationalId, firstname, lastname, dateOdBirth, employeeId){

    return new Promise(async(resolve, reject) => {
        const psql ="UPDATE dependant " +
                    "SET first_name = $2, " +
                    "last_name = $3, " +
                    "date_of_birth = $4, " +
                    "employee_id = $5 " +  
                    "WHERE national_id_no = $1 ";
        const values = [nationalId, firstname, lastname, dateOdBirth, employeeId]

        pool.connect()
        .then(client => {
            return client
            .query(psql, values)
            .then(res => {
                client.release()
                return resolve(res.rows)
            })
            .catch(err => {
                client.release()
                return reject(err.stack)
            })
        })
    })

}
function deleteDependant(id) {
    return new Promise(async(resolve, reject) => {
        const psql= "DELETE " +
                    "FROM dependant " +
                    "WHERE id = $1 ";
        const values = [id]

        pool.connect()
        .then(client => {
            return client
            .query(psql, values)
            .then(res => {
                client.release()
                return resolve(res.rows)
            })
            .catch(err => {
                client.release()
                return reject(err.stack)
            })
        })
    })

}


module.exports = {findAll, find, findByEmployeeId, findToExpireNextMonth, findNonSupported, insert, update, deleteDependant}