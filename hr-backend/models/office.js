// getting the psql database connection
var pool = require("../database/connection");




// finding all the employees
function search(searchText) {

    return new Promise(async(resolve, reject) => {

        const psql= "SELECT * " +
                    "FROM office " +
                    "WHERE office_name  ILIKE $1 ";
        const values = ['%' + searchText + '%']
        pool.connect()
        .then(client => {
            client
            .query(psql, values)
            .then(res => {
                client.release()
                // return res.rows
                return resolve(res.rows)
            })
            .catch(err => {
                client.release()
                return reject(err.stack)
            })
        })
    })
}



// finding all the offices
function findAll() {

    return new Promise(async(resolve, reject) => {
        const psql= "SELECT * " +
                    "FROM office";

        pool.connect()
        .then(client => {
            return client
            .query(psql)
            .then(res => {
                client.release()
                return resolve(res.rows)
            })
            .catch(err => {
                client.release()
                return reject(err.stack)
            })
        })
    })
}


// finding a specific office by its id
function find(officeId) {

    return new Promise(async(resolve, reject) => {
        const psql = "SELECT * " +
                    "FROM office " +
                    "WHERE office_id = $1 ";
        const values = [officeId]

        pool.connect()
        .then(client => {
            return client
            .query(psql, values)
            .then(res => {
                client.release()
                return resolve(res.rows)
            })
            .catch(err => {
                client.release()
                return reject(err.stack)
            })
        })
    })
}



// finding a specific office by its name
function findByName(officeName) {
    return new Promise(async(resolve, reject) => {
        const psql = "SELECT * " +
                    "FROM office " +
                    "WHERE office_name = $1 ";
        const values = [officeName]

        pool.connect()
        .then(client => {
            return client
            .query(psql, values)
            .then(res => {
                client.release()
                return resolve(res.rows)
            })
            .catch(err => {
                client.release()
                return reject(err.stack)
            })
        })
    })
}




// inserting the office
function insert(officeId, officeName) {
    return new Promise(async(resolve, reject) => {
        const psql = "INSERT INTO office " +
                    "(office_id, office_name) " +
                    "VALUES ($1, $2) ";
        const values = [officeId, officeName]

        pool.connect()
        .then(client => {
            return client
            .query(psql, values)
            .then(res => {
                client.release()
                return resolve(res.rows)
            })
            .catch(err => {
                client.release()
                return reject(err.stack)
            })
        })
    })
}

// updating the office
function update(officeId, officeName){
    return new Promise(async(resolve, reject) => {
        const psql ="UPDATE office " +
                    "SET office_name = $2 " +
                    "WHERE = department_id = $1";
        const values = [ officeId, officeName ]

        pool.connect()
        .then(client => {
            return client
            .query(psql, values)
            .then(res => {
                client.release()
                return resolve(res.rows)
            })
            .catch(err => {
                client.release()
                return reject(err.stack)
            })
        })
    })
}


// deleting the office
function deleteOffice(officeId) {
    return new Promise(async(resolve, reject) => {
        const psql =  "DELETE " +
                    "FROM office " +
                    "WHERE office_id = $1 ";
        const values = [officeId]

        pool.connect()
        .then(client => {
            return client
            .query(psql, values)
            .then(res => {
                client.release()
                return resolve(res.rows)
            })
            .catch(err => {
                client.release()
                return reject(err.stack)
            })
        })
    })
}

module.exports = {search, findAll, find, findByName, insert, update, deleteOffice}