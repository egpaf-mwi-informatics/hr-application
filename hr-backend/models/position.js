// getting the psql database connection
var pool = require("../database/connection");



// finding all the employees
function search(searchText) {

    return new Promise(async(resolve, reject) => {

        const psql= "SELECT * " +
                    "FROM positions " +
                    "WHERE position_name  ILIKE $1 ";
        const values = ['%' + searchText + '%']
        pool.connect()
        .then(client => {
            client
            .query(psql, values)
            .then(res => {
                client.release()
                // return res.rows
                return resolve(res.rows)
            })
            .catch(err => {
                client.release()
                return reject(err.stack)
            })
        })
    })
}



// finding all the positions
function findAll() {

    return new Promise(async(resolve, reject) => {
        const psql= "SELECT * " +
                    "FROM positions";

        pool.connect()
        .then(client => {
            return client
            .query(psql)
            .then(res => {
                client.release()
                return resolve(res.rows)
            })
            .catch(err => {
                client.release()
                return reject(err.stack)
            })
        })
    })
}


// finding a specific position its id
function find(id) {

    return new Promise(async(resolve, reject) => {
        const psql = "SELECT * " +
                    "FROM positions " +
                    "WHERE id = $1 ";
        const values = [id]

        pool.connect()
        .then(client => {
            return client
            .query(psql, values)
            .then(res => {
                client.release()
                return resolve(res.rows)
            })
            .catch(err => {
                client.release()
                return reject(err.stack)
            })
        })
    })
}



// finding a specific position by its name
function findByName(positionName) {

    return new Promise(async(resolve, reject) => {
        const psql = "SELECT * " +
                    "FROM office " +
                    "WHERE position_name = $1 ";
        const values = [positionName]

        pool.connect()
        .then(client => {
            return client
            .query(psql, values)
            .then(res => {
                client.release()
                return resolve(res.rows)
            })
            .catch(err => {
                client.release()
                return reject(err.stack)
            })
        })
    })

}




// inserting the  position
function insert(positionId, positionName) {
    return new Promise(async(resolve, reject) => {
        const psql = "INSERT INTO position " +
                    "(position_id, position_name) " +
                    "VALUES ($1, $2) ";
        const values = [positionId, positionName]

        pool.connect()
        .then(client => {
            return client
            .query(psql, values)
            .then(res => {
                client.release()
                return resolve(res.rows)
            })
            .catch(err => {
                client.release()
                return reject(err.stack)
            })
        })
    })
}

// updating the position
function update(positionId, positionName){
    return new Promise(async(resolve, reject) => {
        const psql ="UPDATE position " +
                    "SET position_name = $2 " +
                    "WHERE = position_id = $1";
        const values = [ positionId, positionName ]

        pool.connect()
        .then(client => {
            return client
            .query(psql, values)
            .then(res => {
                client.release()
                return resolve(res.rows[0])
            })
            .catch(err => {
                client.release()
                return reject(err.stack)
            })
        })
    })
}


// deleting the position
function deletePosition(positionId) {
    return new Promise(async(resolve, reject) => {
        const psql =  "DELETE " +
                    "FROM position " +
                    "WHERE position_id = $1 ";
        const values = [positionId]

        pool.connect()
        .then(client => {
            return client
            .query(psql, values)
            .then(res => {
                client.release()
                return resolve(res.rows)
            })
            .catch(err => {
                client.release()
                return reject(err.stack)
            })
        })
    })
}

module.exports = {search, findAll, find, findByName, insert, update, deletePosition}