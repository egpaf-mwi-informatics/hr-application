// getting the psql database connection
var pool = require("../database/connection");




// finding all the anniversary types
function findAll() {

    return new Promise(async(resolve, reject) => {
        const psql= "SELECT * " +
                    "FROM anniversary_type";

        pool.connect()
        .then(client => {
            return client
            .query(psql)
            .then(res => {
                client.release()
                return resolve(res.rows)
            })
            .catch(err => {
                client.release()
                return reject(err.stack)
            })
        })
    })
}


// finding an anniversary by its id
function find(id) {

    return new Promise(async(resolve, reject) => {
        const psql = "SELECT * " +
                    "FROM anniversary_type " +
                    "WHERE id = $1 ";
        const values = [id]

        pool.connect()
        .then(client => {
            return client
            .query(psql, values)
            .then(res => {
                client.release()
                return resolve(res.rows)
            })
            .catch(err => {
                client.release()
                return reject(err.stack)
            })
        })
    })
}



// finding an anniversary type by its years
function findByYears(years) {
    return new Promise(async(resolve, reject) => {
        const psql = "SELECT * " +
                    "FROM anniversary_type " +
                    "WHERE years = $1 ";

        const values = [years]

        pool.connect()
        .then(client => {
            return client
            .query(psql, values)
            .then(res => {
                client.release()
                return resolve(res.rows)
            })
            .catch(err => {
                client.release()
                return reject(err.stack)
            })
        })
    })
}




// inserting the anniversary type
function insert(years) {
    return new Promise(async(resolve, reject) => {
        const psql = "INSERT INTO anniversary_type " +
                    "(years) " +
                    "VALUES ($1) ";
        const values = [years]

        pool.connect()
        .then(client => {
            return client
            .query(psql, values)
            .then(res => {
                client.release()
                return resolve(res.rows)
            })
            .catch(err => {
                client.release()
                return reject(err.stack)
            })
        })
    })
}

// updating the anniversary_type
function update (id, years) {
    return new Promise(async(resolve, reject) => {
        const psql ="UPDATE anniversary_type " +
                    "SET office_name = $2 " +
                    "WHERE years = $1";
        const values = [id, years]

        pool.connect()
        .then(client => {
            return client
            .query(psql, values)
            .then(res => {
                client.release()
                return resolve(res.rows)
            })
            .catch(err => {
                client.release()
                return reject(err.stack)
            })
        })
    })
}


// deleting the anniversay type
function deleteAnniversaryType (id) {
    return new Promise(async(resolve, reject) => {
        const psql =  "DELETE " +
                    "FROM anniversary_type " +
                    "WHERE id = $1 ";
        const values = [id]

        pool.connect()
        .then(client => {
            return client
            .query(psql, values)
            .then(res => {
                client.release()
                return resolve(res.rows)
            })
            .catch(err => {
                client.release()
                return reject(err.stack)
            })
        })
    })
}

module.exports = {findAll, find, findByYears, insert, update, deleteAnniversaryType}