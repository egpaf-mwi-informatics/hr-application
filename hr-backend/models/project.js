/*
|----------------------------------------------|
|SOFTWARE ENGINEER: Robin B. Mkuwira           |
|PROJECT NAME: HR Application                  |
|DATE: 02/06/2021                              |
|                                              |
|Description:                                  |
|Below is the code for the project model that  |
|is used to read, write, update and delete the |
|project table in the HR application database  |
|system.                                       |
|----------------------------------------------|
*/

// getting the psql database connection
var pool = require("../database/connection");

// finding all the projects
function findAll() {
    return new Promise(async(resolve, reject) => {
        const psql= "SELECT * " +
                    "FROM project";

        pool.connect()
        .then(client => {
            client
            .query(psql)
            .then(res => {
                client.release()
                return resolve(res.rows)
            })
            .catch(err => {
                client.release()
                return reject(err.stack)
            })
        })
    })
}


// finding a specific project by id
function find(id) {

    return new Promise(async(resolve, reject) => {
        const psql= "SELECT * " +
                    "FROM project " +
                    "WHERE contract_id = $1 ";
        const values = [contractId]

        pool.connect()
        .then(client => {
            return client
            .query(psql, values)
            .then(res => {
                client.release()
                return resolve(res.rows)
            })
            .catch(err => {
                client.release()
                return reject(err.stack)
            })
        })
    })
}


// finding a specific contract by the project name
function findByName(name) {
    return new Promise(async(resolve, reject) => {
        const psql= "SELECT * " +
                    "FROM project " +
                    "WHERE name = $1 ";

        const values = [name]

        pool.connect()
        .then(client => {
            return client
            .query(psql, values)
            .then(res => {
                client.release()
                return resolve(res.rows)
            })
            .catch(err => {
                client.release()
                return reject(err)
            })
        })
    })

}











// inserting a project
function insert(name) {
    return new Promise(async(resolve, reject) => {
        const psql = "INSERT INTO contract " +
                     "(project_name) " +
                     "VALUES ($1) ";
        const values = [name]

        pool.connect()
        .then(client => {
            return client
            .query(psql, values)
            .then(res => {
                client.release()
                return resolve(res.rows[0])
            })
            .catch(err => {
                client.release()
                return reject(err.stack)
            })
        })
    })
}

// updating a project
function update(id, name){

    return new Promise(async(resolve, reject) => {
        const psql ="UPDATE project " +
                    "SET project_name = $2 " +
                    "WHERE id = $1 ";
        const values = [id , name]

        pool.connect()
        .then(client => {
            return client
            .query(psql, values)
            .then(res => {
                client.release()
                return resolve(res)
            })
            .catch(err => {
                client.release()
                return reject(err)
            })
        })
    })

}




function deleteProject(id) {
    return new Promise(async(resolve, reject) => {
        const psql= "DELETE " +
                    "FROM project " +
                    "WHERE id = $1 ";
        const values = [id]

        pool.connect()
        .then(client => {
            return client
            .query(psql, values)
            .then(res => {
                client.release()
                return resolve(res.rows[0])
            })
            .catch(err => {
                client.release()
                return reject(err.stack)
            })
        })
    })

}


module.exports = {findAll, find, findByName, insert, update, deleteProject}