/*
|-----------------------------------------------|
|SOFTWARE ENGINEER: Robin B. Mkuwira            |
|PROJECT NAME: HR Application                   |
|DATE: 02/06/2021                               |
|                                               |
|Description:                                   |
|Below is the code for the pip                  |
|model that is used to read, write, update and  |
|delete the pip table in the application        |
|database system.                               |
|-----------------------------------------------|
*/

// getting the psql database connection
var pool = require("../database/connection");

// finding all the pips
function findAll() {

    return new Promise(async(resolve, reject) => {
        const psql= "SELECT * " +
                    "FROM pip";

        pool.connect()
        .then(client => {
            return client
            .query(psql)
            .then(res => {
                client.release()
                return resolve(res.rows)
            })
            .catch(err => {
                client.release()
                return reject(err.stack)
            })
        })
    })
}


// finding a specific pip by its id
function find(id) {
    return new Promise(async(resolve, reject) => {
        const psql= "SELECT * " +
                    "FROM pip " +
                    "WHERE id = $1 ";
        const values = [id]

        pool.connect()
        .then(client => {
            return client
            .query(psql, values)
            .then(res => {
                client.release()
                return resolve(res.rows)
            })
            .catch(err => {
                client.release()
                return reject(err.stack)
            })
        })
    })
}



// finding a specific pip by its id
function findByContractId(contractId) {
    return new Promise(async(resolve, reject) => {
        const psql= "SELECT * " +
                    "FROM pip " +
                    "WHERE contract_id = $1 ";
        const values = [contractId]

        pool.connect()
        .then(client => {
            return client
            .query(psql, values)
            .then(res => {
                client.release()
                return resolve(res.rows)
            })
            .catch(err => {
                client.release()
                return reject(err.stack)
            })
        })
    })
}







// inserting a pip
function insert(priorities, pipComments, startDate, endDate, contractId, status) {
    return new Promise(async(resolve, reject) => {
        const psql = "INSERT INTO pip " +
                    "(priorities, pip_comments, start_date, end_date, contract_id, status)" +
                    "VALUES ($1, $2, $3, $4, $5, $6) ";
        const values = [priorities, pipComments, startDate, endDate, contractId, status]

        pool.connect()
        .then(client => {
            return client
            .query(psql, values)
            .then(res => {
                client.release()
                return resolve(res)
            })
            .catch(err => {
                client.release()
                return reject(err)
            })
        })
    })
}

// updating a pip
function update(id, priorities, pipComments, startDate, endDate, contractId, status){

    return new Promise(async(resolve, reject) => {
        const psql ="UPDATE pip " +
                    "SET priorities = $2, " +
                    "pip_comments = $3, " +
                    "start_date = $4, " +
                    "end_date = $5, " +  
                    "contract_id = $6, " +
                    "status = $7 " +  
                    "WHERE id = $1 ";
        const values = [id,  priorities, pipComments, startDate, endDate, contractId, status]

        pool.connect()
        .then(client => {
            return client
            .query(psql, values)
            .then(res => {
                client.release()
                return resolve(res.rows)
            })
            .catch(err => {
                client.release()
                return reject(err.stack)
            })
        })
    })

}
function deletePip(id) {
    return new Promise(async(resolve, reject) => {
        const psql= "DELETE " +
                    "FROM pip " +
                    "WHERE id = $1 ";
        const values = [id]

        pool.connect()
        .then(client => {
            return client
            .query(psql, values)
            .then(res => {
                client.release()
                return resolve(res.rows)
            })
            .catch(err => {
                client.release()
                return reject(err.stack)
            })
        })
    })

}


module.exports = {findAll, find, findByContractId, insert, update, deletePip}