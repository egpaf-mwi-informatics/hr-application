const jwt = require("jsonwebtoken")

module.exports = () => {
    return (req, res, next) => {

        if (req.headers.authorization != undefined && req.headers.authorization != null && req.headers.authorization.trim() !== "") {
            const [bearer, token] = req.headers.authorization.split(" ")
            
            // check if its the inhouse systems token
            if (token == process.env.API_KEY) {
                next()
            } else {
                jwt.verify(token, process.env.JWT_SECRET_KEY, (err, decoded) => {
                    if (err) {
                        console.log('invalid token')
                        res.status(401).send("Invalid token, access denied...")
                    } else {
                        next()
                    }
                })
            }
        } else {
            console.log('empty token')
            res.status(401).send("Empty token, access denied...")
        }
   
    }
}