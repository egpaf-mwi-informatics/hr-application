// getting the psql database connection
var pool = require("../database/connection");




// finding all the anniversaries
function findAll() {

    return new Promise(async(resolve, reject) => {
        const psql= "SELECT * " +
                    "FROM anniversary";

        pool.connect()
        .then(client => {
            return client
            .query(psql)
            .then(res => {
                client.release()
                return resolve(res.rows)
            })
            .catch(err => {
                client.release()
                return reject(err.stack)
            })
        })
    })
}


// finding an anniversary by its id
function find(id) {

    return new Promise(async(resolve, reject) => {
        const psql = "SELECT * " +
                    "FROM anniversary " +
                    "WHERE id = $1 ";
        const values = [id]

        pool.connect()
        .then(client => {
            return client
            .query(psql, values)
            .then(res => {
                client.release()
                return resolve(res.rows)
            })
            .catch(err => {
                client.release()
                return reject(err.stack)
            })
        })
    })
}


// find if the anniversary already exists
function exists (employeeId, years) {

    return new Promise(async(resolve, reject) => {
        const psql = "SELECT * " +
                     "FROM anniversary " +
                     "JOIN anniversary_type " +
                     "ON (anniversary.anniversary_type_id = anniversary_type.id) " +
                     "WHERE employee_id = $1 AND years = $2";

        const values = [employeeId, years]

        pool.connect()
        .then(client => {
            return client
            .query(psql, values)
            .then(res => {
                client.release()
                return resolve(res.rows)
            })
            .catch(err => {
                client.release()
                return reject(err.stack)
            })
        })
    })
}



// finding an anniversary type by its years
function findByYears(years) {
    return new Promise(async(resolve, reject) => {
        const psql = "SELECT anniversary.id, anniversary.date, anniversary.employee_id, years, anniversary_type_id " +
                    "FROM anniversary " +
                    "JOIN anniversary_type " +
                    "ON (anniversary.anniversary_type_id = anniversary_type.id) " +
                    "WHERE anniversary_type.years = $1 ";

        const values = [years]

        pool.connect()
        .then(client => {
            return client
            .query(psql, values)
            .then(res => {
                client.release()
                return resolve(res.rows)
            })
            .catch(err => {
                client.release()
                return reject(err.stack)
            })
        })
    })
}




// inserting the anniversary type
function insert(employeeId, anniversaryTypeId) {
    return new Promise(async(resolve, reject) => {
        const psql = "INSERT INTO anniversary " +
                    "(employee_id, anniversary_type_id, date) " +
                    "VALUES ($1, $2, NOW()) ";
        const values = [employeeId, anniversaryTypeId]

        pool.connect()
        .then(client => {
            return client
            .query(psql, values)
            .then(res => {
                client.release()
                return resolve(res.rows)
            })
            .catch(err => {
                client.release()
                return reject(err.stack)
            })
        })
    })
}

// updating the anniversary
function update (id, employeeId, anniversaryTypeId) {
    return new Promise(async(resolve, reject) => {
        const psql ="UPDATE anniversary " +
                    "SET employee_id = $2, " +
                    "anniversary_type_id = $3 " +
                    "WHERE years = $1";
        const values = [id, employeeId, anniversaryTypeId]

        pool.connect()
        .then(client => {
            return client
            .query(psql, values)
            .then(res => {
                client.release()
                return resolve(res.rows)
            })
            .catch(err => {
                client.release()
                return reject(err.stack)
            })
        })
    })
}


// deleting the anniversay
function deleteAnniversary (id) {
    return new Promise(async(resolve, reject) => {
        const psql =  "DELETE " +
                    "FROM anniversary " +
                    "WHERE id = $1 ";
        const values = [id]

        pool.connect()
        .then(client => {
            return client
            .query(psql, values)
            .then(res => {
                client.release()
                return resolve(res.rows)
            })
            .catch(err => {
                client.release()
                return reject(err.stack)
            })
        })
    })
}

module.exports = {findAll, find, exists, findByYears, insert, update, deleteAnniversary}