/*
|-----------------------------------------------|
|SOFTWARE ENGINEER: Robin B. Mkuwira            |
|PROJECT NAME: HR Application                   |
|DATE: 02/06/2021                               |
|                                               |
|Description:                                   |
|Below is the code for the dependant relation   |
|model that is used to read, write, update and |
|delete the dependant relation table in the     |
|HR application database system.                |
|-----------------------------------------------|
*/

// getting the psql database connection
var pool = require("../database/connection");

// finding all the dependant relations
function findAll() {

    return new Promise(async(resolve, reject) => {
        const psql= "SELECT * " +
                    "FROM dependant_relation";

        pool.connect()
        .then(client => {
            return client
            .query(psql)
            .then(res => {
                client.release()
                return resolve(res.rows)
            })
            .catch(err => {
                client.release()
                return reject(err.stack)
            })
        })
    })
}


// finding a specific dependant relation
function find(id) {
    return new Promise(async(resolve, reject) => {
        const psql= "SELECT * " +
                    "FROM dependant_relation " +
                    "WHERE id = $1 ";
        const values = [id]

        pool.connect()
        .then(client => {
            return client
            .query(psql, values)
            .then(res => {
                client.release()
                return resolve(res.rows)
            })
            .catch(err => {
                client.release()
                return reject(err.stack)
            })
        })
    })
}



// finding a specific dependant relation
function search(searchText) {
    return new Promise(async(resolve, reject) => {
        const psql= "SELECT * " +
                    "FROM dependant_relation " +
                    "WHERE relation_type ILIKE $1 ";
                    console.log(searchText)
        const values = [ '%' + searchText +  '%']

        pool.connect()
        .then(client => {
            return client
            .query(psql, values)
            .then(res => {
                client.release()
                return resolve(res.rows)
            })
            .catch(err => {
                client.release()
                return reject(err.stack)
            })
        })
    })
}


// inserting a dependant_relation
function insert(relationType) {
    return new Promise(async(resolve, reject) => {
        const psql = "INSERT INTO dependant_relation " +
                    "(relation_type)" +
                    "VALUES ($1) ";
        const values = [relationType]

        pool.connect()
        .then(client => {
            return client
            .query(psql, values)
            .then(res => {
                client.release()
                return resolve(res.rows)
            })
            .catch(err => {
                client.release()
                return reject(err.stack)
            })
        })
    })
}


function deleteRelationType(id) {
    return new Promise(async(resolve, reject) => {
        const psql= "DELETE " +
                    "FROM relation_type " +
                    "WHERE id = $1 ";
        const values = [id]

        pool.connect()
        .then(client => {
            return client
            .query(psql, values)
            .then(res => {
                client.release()
                return resolve(res.rows)
            })
            .catch(err => {
                client.release()
                return reject(err.stack)
            })
        })
    })

}


module.exports = {findAll, find, search, insert, deleteRelationType}