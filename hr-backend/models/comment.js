/*
|----------------------------------------------|
|SOFTWARE ENGINEER: Robin B. Mkuwira           |
|PROJECT NAME: HR Application                  |
|DATE: 02/06/2021                              |
|                                              |
|Description:                                  |
|Below is the code for the comment model that  |
|is used to read, write, update and delete the |
|comment table in the HR application database  |
|system.                                       |
|----------------------------------------------|
*/

// getting the psql database connection
var pool = require("../database/connection");

// finding all the comments
function findAll() {
    return new Promise(async(resolve, reject) => {
        const psql= "SELECT * " +
                    "FROM comment";

        pool.connect()
        .then(client => {
            client
            .query(psql)
            .then(res => {
                client.release()
                return resolve(res.rows)
            })
            .catch(err => {
                client.release()
                return reject(err.stack)
            })
        })
    })
}


// finding a specific comment by id
function find(id) {

    return new Promise(async(resolve, reject) => {
        const psql= "SELECT * " +
                    "FROM comment " +
                    "WHERE id = $1 ";
        const values = [contractId]

        pool.connect()
        .then(client => {
            return client
            .query(psql, values)
            .then(res => {
                client.release()
                return resolve(res.rows)
            })
            .catch(err => {
                client.release()
                return reject(err.stack)
            })
        })
    })
}


// finding a specific comment by the employee id
function findByEmployeeId() {
    return new Promise(async(resolve, reject) => {
        const psql= "SELECT * " +
                    "FROM comment " +
                    "WHERE employee_id = $1 ";

        const values = [contractId]

        pool.connect()
        .then(client => {
            return client
            .query(psql, values)
            .then(res => {
                client.release()
                return resolve(res.rows[0])
            })
            .catch(err => {
                client.release()
                return reject(err.stack)
            })
        })
    })

}








// find all active contract to update
function findActive() {
    return new Promise(async(resolve, reject) => {
        const psql= "SELECT * " +
                    "FROM contract " +
                    "WHERE end_date > NOW() AND (description != 'Active' OR description IS NULL)";

        pool.connect()
        .then(client => {
            return client
            .query(psql)
            .then(res => {
                client.release()
                return resolve(res.rows)
            })
            .catch(err => {
                client.release()
                return reject(err.stack)
            })
        })
    })
}








// inserting a comment
function insert(employeeId, pipId, comment, from) {
    return new Promise(async(resolve, reject) => {
        const psql = "INSERT INTO comment " +
                     "( employee_id, pip_id, comment, comment_from, date) " +
                     "VALUES ($1, $2, $3, $4, NOW()) ";
        const values = [employeeId, pipId, comment, from]

        pool.connect()
        .then(client => {
            return client
            .query(psql, values)
            .then(res => {
                client.release()
                return resolve(res.rows[0])
            })
            .catch(err => {
                client.release()
                return reject(err.stack)
            })
        })
    })
}

// updating a comment
function update(id, employeeId, pipId, comment, from){

    return new Promise(async(resolve, reject) => {
        const psql ="UPDATE comment " +
                    "SET employee_id = $2, " +
                    "pip_id = $3, " +
                    "comment = $4, " +
                    "comment_from = $5 " +
                    "WHERE id = $1 ";
        const values = [id , employeeId, pipId, comment, from]

        pool.connect()
        .then(client => {
            return client
            .query(psql, values)
            .then(res => {
                client.release()
                return resolve(res)
            })
            .catch(err => {
                client.release()
                return reject(err)
            })
        })
    })

}





function deleteComment() {
    return new Promise(async(resolve, reject) => {
        const psql= "DELETE " +
                    "FROM comment " +
                    "WHERE id = $1 ";
        const values = [contractId]

        pool.connect()
        .then(client => {
            return client
            .query(psql, values)
            .then(res => {
                client.release()
                return resolve(res.rows[0])
            })
            .catch(err => {
                client.release()
                return reject(err.stack)
            })
        })
    })

}


module.exports = {findAll, find, findByEmployeeId, findActive, insert, update, deleteComment}