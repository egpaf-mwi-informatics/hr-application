/*
|--------------------------------------------------|
|SOFTWARE ENGINEER: Robin B. Mkuwira               |
|PROJECT NAME: HR Application                      |
|DATE: 02/06/2021                                  |
|                                                  |
|Description:                                      |
|Below is the code for the dependabts notification |
| model that is used to read, write, update and    |
|delete the dependents notification table in the   |
|HR application database system.                   |
|--------------------------------------------------|
*/

// getting the psql database connection
var pool = require("../database/connection");

// finding all the dependents notifications
function findAll() {

    return new Promise(async(resolve, reject) => {
        const psql= "SELECT * " +
                    "FROM dependant_notification";

        pool.connect()
        .then(client => {
            return client
            .query(psql)
            .then(res => {
                client.release()
                return resolve(res.rows)
            })
            .catch(err => {
                client.release()
                return reject(err.stack)
            })
        })
    })
}



// finding specific dependant
function exists(dependantId) {

    return new Promise(async(resolve, reject) => {

        const psql= "SELECT * " +
                    "FROM dependant_notification " +
                    "WHERE dependant_id = $1";
        const values = [dependantId]
        pool.connect()
        .then(client => {
            client
            .query(psql, values)
            .then(res => {
                client.release()
                // return res.rows
                return resolve(res.rows)
            })
            .catch(err => {
                client.release()
                return reject(err)
            })
        })
    })
}




// finding a specific dependant
function find(id) {
    return new Promise(async(resolve, reject) => {
        const psql= "SELECT * " +
                    "FROM dependant_notification " +
                    "WHERE id = $1 ";
        const values = [id]

        pool.connect()
        .then(client => {
            return client
            .query(psql, values)
            .then(res => {
                client.release()
                return resolve(res.rows)
            })
            .catch(err => {
                client.release()
                return reject(err.stack)
            })
        })
    })
}


// inserting a dependant notification
function insert(dependantId) {
    return new Promise(async(resolve, reject) => {
        const psql = "INSERT INTO dependant_notification " +
                    "(dependant_id, notification_date)" +
                    "VALUES ($1, NOW()) ";
        const values = [dependantId]

        pool.connect()
        .then(client => {
            return client
            .query(psql, values)
            .then(res => {
                client.release()
                return resolve(res.rows)
            })
            .catch(err => {
                client.release()
                return reject(err.stack)
            })
        })
    })
}


function deleteDependantNotification(id) {
    return new Promise(async(resolve, reject) => {
        const psql= "DELETE " +
                    "FROM dependant_notification " +
                    "WHERE id = $1 ";
        const values = [id]

        pool.connect()
        .then(client => {
            return client
            .query(psql, values)
            .then(res => {
                client.release()
                return resolve(res.rows)
            })
            .catch(err => {
                client.release()
                return reject(err.stack)
            })
        })
    })

}


module.exports = {findAll, find, insert, exists, deleteDependantNotification}