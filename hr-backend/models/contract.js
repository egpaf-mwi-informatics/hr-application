/*
|----------------------------------------------|
|SOFTWARE ENGINEER: Robin B. Mkuwira           |
|PROJECT NAME: HR Application                  |
|DATE: 02/06/2021                              |
|                                              |
|Description:                                  |
|Below is the code for the contract model that |
|is used to read, write, update and delete the |
|contract table in the HR application database |
|system.                                       |
|----------------------------------------------|
*/

// getting the psql database connection
var pool = require("../database/connection");

// finding all the contracts
function findAll() {
    return new Promise(async(resolve, reject) => {
        const psql= "SELECT * " +
                    "FROM contract";

        pool.connect()
        .then(client => {
            client
            .query(psql)
            .then(res => {
                client.release()
                return resolve(res.rows)
            })
            .catch(err => {
                client.release()
                return reject(err.stack)
            })
        })
    })
}


// finding a specific contract by id
function find(contractId) {

    return new Promise(async(resolve, reject) => {
        const psql= "SELECT * " +
                    "FROM contract " +
                    "WHERE contract_id = $1 ";
        const values = [contractId]

        pool.connect()
        .then(client => {
            return client
            .query(psql, values)
            .then(res => {
                client.release()
                return resolve(res.rows)
            })
            .catch(err => {
                client.release()
                return reject(err.stack)
            })
        })
    })
}


// finding a specific contract by the employee id
function findByEmployeeId(employeeId) {
    return new Promise(async(resolve, reject) => {
        const psql= "SELECT * " +
                    "FROM contract " +
                    "WHERE employee_id = $1 ";

        const values = [employeeId]

        pool.connect()
        .then(client => {
            return client
            .query(psql, values)
            .then(res => {
                client.release()
                return resolve(res.rows)
            })
            .catch(err => {
                client.release()
                return reject(err.stack)
            })
        })
    })

}



// finding the years spent by an employee at egpaf
function findAnniversaryYears () {
    return new Promise(async(resolve, reject) => {
        const psql= "SELECT employee_id, date_part('year', MAX(NOW())::date) - date_part('year', MIN(start_date)::date) AS duration, " +
                    "TO_CHAR(MIN(start_date)::DATE,'dd Month yyyy') AS first_contract_start_date, " +
                    "TO_CHAR(MAX(end_date)::DATE,'dd Month yyyy') AS recent_contract_end_date " +
                    "FROM contract " +
                    "GROUP BY employee_id " + 
                    "HAVING (DATE_PART('year', MAX(NOW())::DATE) - DATE_PART('year', MIN(start_date)::DATE)) IN ( " +
                    "   SELECT years " +
                    "   FROM anniversary_type " +
                    ") " +
                    "AND EXTRACT (YEAR FROM MAX(end_date)) >= EXTRACT(YEAR FROM CURRENT_DATE) " +
                    "ORDER BY duration DESC ";

        const values = []

        pool.connect()
        .then(client => {
            return client
            .query(psql, values)
            .then(res => {
                client.release()
                return resolve(res.rows)
            })
            .catch(err => {
                client.release()
                return reject(err.stack)
            })
        })
    })

}



// find all contract the contract that have expired to update
function findExpired() {
    return new Promise(async(resolve, reject) => {
        const psql= "SELECT * " +
                    "FROM contract " +
                    "WHERE end_date < NOW() AND (description != 'Expired' OR description IS NULL)";

        pool.connect()
        .then(client => {
            return client
            .query(psql)
            .then(res => {
                client.release()
                return resolve(res.rows)
            })
            .catch(err => {
                client.release()
                return reject(err.stack)
            })
        })
    })
}





// find all active contract to update
function findActive() {
    return new Promise(async(resolve, reject) => {
        const psql= "SELECT * " +
                    "FROM contract " +
                    "WHERE end_date > NOW() AND (description != 'Active' OR description IS NULL)";

        pool.connect()
        .then(client => {
            return client
            .query(psql)
            .then(res => {
                client.release()
                return resolve(res.rows)
            })
            .catch(err => {
                client.release()
                return reject(err.stack)
            })
        })
    })
}








// inserting a contract
function insert(employeeId, startDate, endDate, status, sectionId, positionId, officeId, contId) {
    return new Promise(async(resolve, reject) => {
        const psql = "INSERT INTO contract " +
                     "( employee_id, start_date, end_date, status, section_id, position_id, office_id, cont_id) " +
                     "VALUES ($1, $2, $3, $4, $5, $6, $7, $8) ";
        const values = [employeeId, startDate, endDate, status, sectionId, positionId, officeId, contId]

        pool.connect()
        .then(client => {
            return client
            .query(psql, values)
            .then(res => {
                client.release()
                return resolve(res.rows[0])
            })
            .catch(err => {
                client.release()
                return reject(err.stack)
            })
        })
    })
}

// updating a contract
function update(contractId, employeeId, startDate, endDate, status, sectionId, positionId, officeId, contId){

    return new Promise(async(resolve, reject) => {
        const psql ="UPDATE contract " +
                    "SET employee_id = $2, " +
                    "start_date = $3, " +
                    "end_date = $4, " +
                    "status = $5, " +
                    "section_id = $6, " +
                    "position_id = $7, " +
                    "office_id = $8, " +
                    "cont_id = $9 " +
                    "WHERE contract_id = $1 ";
        const values = [contractId , employeeId, startDate, endDate, status, sectionId, positionId, officeId, contId]

        pool.connect()
        .then(client => {
            return client
            .query(psql, values)
            .then(res => {
                client.release()
                return resolve(res)
            })
            .catch(err => {
                client.release()
                return reject(err)
            })
        })
    })

}





// updating a contract
function updateDescription(contractId, description){
    return new Promise(async(resolve, reject) => {
        const psql ="UPDATE contract " +
                    "SET description = $2 " +
                    "WHERE contract_id = $1 ";
        const values = [contractId, description]

        pool.connect()
        .then(client => {
            return client
            .query(psql, values)
            .then(res => {
                client.release()
                return resolve(res)
            })
            .catch(err => {
                client.release()
                return reject(err)
            })
        })
    })

}



function deleteContract() {
    return new Promise(async(resolve, reject) => {
        const psql= "DELETE " +
                    "FROM contract " +
                    "WHERE contract_id = $1 ";
        const values = [contractId]

        pool.connect()
        .then(client => {
            return client
            .query(psql, values)
            .then(res => {
                client.release()
                return resolve(res.rows[0])
            })
            .catch(err => {
                client.release()
                return reject(err.stack)
            })
        })
    })

}


module.exports = {findAll, find, findByEmployeeId, findAnniversaryYears, findExpired, findActive, insert, updateDescription, update, deleteContract}