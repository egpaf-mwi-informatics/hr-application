// getting the psql database connection
var pool = require("../database/connection");


// finding all the employee distributions
function findAll() {

    return new Promise(async(resolve, reject) => {
        const psql= "SELECT * " +
                    "FROM employee_distribution";

        pool.connect()
        .then(client => {
            return client
            .query(psql)
            .then(res => {
                client.release()
                return resolve(res.rows)
            })
            .catch(err => {
                client.release()
                return reject(err.stack)
            })
        })
    })
}


// finding a specific employee distribution
function find(employeeId, groupId) {
    return new Promise(async(resolve, reject) => {
        const psql = "SELECT * " +
                    "FROM employee_ditribution " +
                    "WHERE group_id = $1 AND employee_id = $2";
        const values = [employeeId, groupId]

        pool.connect()
        .then(client => {
            return client
            .query(psql, values)
            .then(res => {
                client.release()
                return resolve(res.rows)
            })
            .catch(err => {
                client.release()
                return reject(err.stack)
            })
        })
    })
}



// finding a specific employee by id
function findByEmployeeId(employeeId) {

    return new Promise(async(resolve, reject) => {
        const psql = "SELECT * " +
                    "FROM employee " +
                    "WHERE employee_id = $1 ";
        const values = [employeeId]

        pool.connect()
        .then(client => {
            return client
            .query(psql, values)
            .then(res => {
                client.release()
                return resolve(res.rows[0])
            })
            .catch(err => {
                client.release()
                return reject(err.stack)
            })
        })
    })

}


// inserting an employee
function insert(groupId, employeeId) {
    
    return new Promise(async(resolve, reject) => {
        const psql = "INSERT INTO employee_distribution " +
                    "(group_id, employee_id) " +
                    "VALUES ($1, $2) ";
        const values = [groupId, employeeId]

        pool.connect()
        .then(client => {
            return client
            .query(psql, values)
            .then(res => {
                client.release()
                return resolve(res)
            })
            .catch(err => {
                client.release()
                return reject(err)
            })
        })
    })
}

// updating an employee
function update(groupId, employeeId){
    return new Promise(async(resolve, reject) => {
        const psql ="UPDATE employee_distribution " +
                    "SET group_id = $1 " +
                    "employee_id = $2 " +
                    "WHERE group_id = $1 AND employee_id = $2";
        const values = [ groupId, employeeId]

        pool.connect()
        .then(client => {
            return client
            .query(psql, values)
            .then(res => {
                client.release()
                return resolve(res.rows)
            })
            .catch(err => {
                client.release()
                return reject(err.stack)
            })
        })
    })
}


// deleting an employe
function deleteEmployeeDistribution(employeeId) {
    return new Promise(async(resolve, reject) => {
        const psql =  "DELETE " +
                       "FROM employee_distribution " +
                       "WHERE employee_id = $1 ";
        const values = [employeeId]

        pool.connect()
        .then(client => {
            return client
            .query(psql, values)
            .then(res => {
                client.release()
                return resolve(res)
            })
            .catch(err => {
                client.release()
                return reject(err.stack)
            })
        })
    })
}

module.exports = {findAll, find, findByEmployeeId, insert, update, deleteEmployeeDistribution}