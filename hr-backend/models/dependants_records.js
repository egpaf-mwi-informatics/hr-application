/*
|-----------------------------------------------|
|SOFTWARE ENGINEER: Robin B. Mkuwira            |
|PROJECT NAME: HR Application                   |
|DATE: 02/06/2021                               |
|                                               |
|Description:                                   |
|Below is the code for the dependants_records   |
|model that is used to read, write, update and  |
|delete the dependants table in the HR          |
|application database system.                   |
|-----------------------------------------------|
*/

// getting the psql database connection
var pool = require("../database/connection");

// finding all the dependant recocords
function findAll() {

    return new Promise(async(resolve, reject) => {
        const psql= "SELECT * " +
                    "FROM dependant_records";

        pool.connect()
        .then(client => {
            return client
            .query(psql)
            .then(res => {
                client.release()
                return resolve(res.rows)
            })
            .catch(err => {
                client.release()
                return reject(err.stack)
            })
        })
    })
}


// finding a specific dependant record by the national id
function find(nationalId) {
    return new Promise(async(resolve, reject) => {
        const psql= "SELECT * " +
                    "FROM dependant_records " +
                    "WHERE national_id_no = $1 ";
        const values = [nationalId]

        pool.connect()
        .then(client => {
            return client
            .query(psql, values)
            .then(res => {
                client.release()
                return resolve(res.rows)
            })
            .catch(err => {
                client.release()
                return reject(err.stack)
            })
        })
    })
}


// finding a specific contract by the employee id
function findByEmployeeId(employeeId) {
    return new Promise(async(resolve, reject) => {
        const psql= "SELECT * " +
                    "FROM dependant_records " +
                    "WHERE employeeId = $1 ";

        const values = [employeeId]

        pool.connect()
        .then(client => {
            return client
            .query(psql, values)
            .then(res => {
                client.release()
                return resolve(res.rows)
            })
            .catch(err => {
                client.release()
                return reject(err.stack)
            })
        })
    })
}

// inserting a dependant record
function insert(nationalId, firstname, lastname, dateOfBirth, employeeId, id, relation_type, age) {
    return new Promise(async(resolve, reject) => {
        const psql = "INSERT INTO dependants_records " +
                    "(national_id_no, first_name, last_name, date_of_birth, employee_id, id, relation_type, age)" +
                    "VALUES ($1, $2, $3, $4, $5, $6, $7, $8) ";
        const values = [nationalId, firstname, lastname, dateOfBirth, employeeId, id, relation_type, age]

        pool.connect()
        .then(client => {
            return client
            .query(psql, values)
            .then(res => {
                client.release()
                return resolve(res)
            })
            .catch(err => {
                client.release()
                return reject(err)
            })
        })
    })
}

// updating a dependent record
function update(nationalId, firstname, lastname, dateOdBirth, employeeId){

    return new Promise(async(resolve, reject) => {
        const psql ="UPDATE dependant " +
                    "SET first_name = $2, " +
                    "last_name = $3, " +
                    "date_of_birth = $4, " +
                    "employee_id = $5 " +  
                    "WHERE national_id_no = $1 ";
        const values = [nationalId, firstname, lastname, dateOdBirth, employeeId]

        pool.connect()
        .then(client => {
            return client
            .query(psql, values)
            .then(res => {
                client.release()
                return resolve(res.rows)
            })
            .catch(err => {
                client.release()
                return reject(err.stack)
            })
        })
    })

}
function deleteDependant(id) {
    return new Promise(async(resolve, reject) => {
        const psql= "DELETE " +
                    "FROM dependant " +
                    "WHERE id = $1 ";
        const values = [id]

        pool.connect()
        .then(client => {
            return client
            .query(psql, values)
            .then(res => {
                client.release()
                return resolve(res.rows)
            })
            .catch(err => {
                client.release()
                return reject(err.stack)
            })
        })
    })

}


module.exports = {findAll, find, findByEmployeeId, insert, update, deleteDependant}