/*
|----------------------------------------------|
|SOFTWARE ENGINEER: Robin B. Mkuwira           |
|PROJECT NAME: HR Application                  |
|DATE: 01/11/2021                              |
|                                              |
|Description:                                  |
|Below is the code for the status model that   |
|is used to read, write, update and delete the |
|status table in the HR application database   |
|system.                                       |
|----------------------------------------------|
*/

// getting the psql database connection
var pool = require("../database/connection");

// finding all the statuses
function findAll() {
    return new Promise(async(resolve, reject) => {
        const psql= "SELECT * " +
                    "FROM status";

        pool.connect()
        .then(client => {
            client
            .query(psql)
            .then(res => {
                client.release()
                return resolve(res.rows)
            })
            .catch(err => {
                client.release()
                return reject(err.stack)
            })
        })
    })
}


// finding a specific status by id
function find(id) {

    return new Promise(async(resolve, reject) => {
        const psql= "SELECT * " +
                    "FROM status " +
                    "WHERE id = $1 ";
        const values = [id]

        pool.connect()
        .then(client => {
            return client
            .query(psql, values)
            .then(res => {
                client.release()
                return resolve(res.rows)
            })
            .catch(err => {
                client.release()
                return reject(err.stack)
            })
        })
    })
}


// finding a specific contract by the project name
function findByName(name) {
    return new Promise(async(resolve, reject) => {
        const psql= "SELECT * " +
                    "FROM status " +
                    "WHERE status = $1 ";

        const values = [name]

        pool.connect()
        .then(client => {
            return client
            .query(psql, values)
            .then(res => {
                client.release()
                return resolve(res.rows)
            })
            .catch(err => {
                client.release()
                return reject(err)
            })
        })
    })

}











// inserting a status
function insert(status) {
    return new Promise(async(resolve, reject) => {
        const psql = "INSERT INTO contract " +
                     "(status) " +
                     "VALUES ($1) ";
        const values = [status]

        pool.connect()
        .then(client => {
            return client
            .query(psql, values)
            .then(res => {
                client.release()
                return resolve(res.rows[0])
            })
            .catch(err => {
                client.release()
                return reject(err.stack)
            })
        })
    })
}

// updating a status
function update(id, name){

    return new Promise(async(resolve, reject) => {
        const psql ="UPDATE status " +
                    "SET status = $2 " +
                    "WHERE id = $1 ";
        const values = [id , name]

        pool.connect()
        .then(client => {
            return client
            .query(psql, values)
            .then(res => {
                client.release()
                return resolve(res)
            })
            .catch(err => {
                client.release()
                return reject(err)
            })
        })
    })

}




function deleteStatus(id) {
    return new Promise(async(resolve, reject) => {
        const psql= "DELETE " +
                    "FROM status " +
                    "WHERE id = $1 ";
        const values = [id]

        pool.connect()
        .then(client => {
            return client
            .query(psql, values)
            .then(res => {
                client.release()
                return resolve(res.rows[0])
            })
            .catch(err => {
                client.release()
                return reject(err.stack)
            })
        })
    })

}


module.exports = {findAll, find, findByName, insert, update, deleteStatus}