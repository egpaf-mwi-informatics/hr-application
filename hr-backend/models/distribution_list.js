// getting the psql database connection
var pool = require("../database/connection");


// finding all the distribution lists
function findAll() {
    return new Promise(async(resolve, reject) => {

        const psql= "SELECT * " +
                    "FROM distribution_list";
        pool.connect()
        .then(client => {
            return client
            .query(psql)
            .then(res => {
                client.release()
                return resolve(res.rows[0])
            })
            .catch(err => {
                client.release()
                return reject(err.stack)
            })
        })
    })
}



// finding all the employees
function search(searchText) {

    return new Promise(async(resolve, reject) => {

        const psql= "SELECT * " +
                    "FROM distribution_list " +
                    "WHERE group_name  ILIKE $1 OR group_email ILIKE $1 OR group_description ILIKE $1 ";

        const values = ['%' + searchText + '%']
        pool.connect()
        .then(client => {
            client
            .query(psql, values)
            .then(res => {
                client.release()
                // return res.rows
                return resolve(res.rows)
            })
            .catch(err => {
                client.release()
                return reject(err.stack)
            })
        })
    })
}




// finding a specific a distribution by the group id
function find(group_id) {

    return new Promise(async(resolve, reject) => {
        const psql = "SELECT * " +
                    "FROM distribution_list " +
                    "WHERE group_id = $1 ";
        const values = [group_id]

        pool.connect()
        .then(client => {
            return client
            .query(psql, values)
            .then(res => {
                client.release()
                return resolve(res.rows)
            })
            .catch(err => {
                client.release()
                return reject(err.stack)
            })
        })
    })
}



// finding a specific employee by id
function findByGroupName(groupName) {

    return new Promise(async(resolve, reject) => {
        const psql = "SELECT * " +
                    "FROM distribution_list " +
                    "WHERE group_name = $1 ";
        const values = [groupName]

        pool.connect()
        .then(client => {
            return client
            .query(psql, values)
            .then(res => {
                client.release()
                return resolve(res.rows)
            })
            .catch(err => {
                client.release()
                return reject(err.stack)
            })
        })
    })

}


// inserting an distribution list
function insert(groupId, groupName, groupEmail, groupDescription) {
    return new Promise(async(resolve, reject) => {
        const psql = "INSERT INTO distribution_list " +
                    "(group_id, group_name, group_email, group_description) " +
                    "VALUES ($1, $2, $3, $4) ";
        const values = [groupId, groupName, groupEmail, groupDescription]

        pool.connect()
        .then(client => {
            return client
            .query(psql, values)
            .then(res => {
                client.release()
                return resolve(res.rows)
            })
            .catch(err => {
                client.release()
                return reject(err.stack)
            })
        })
    })
}

// updating an employee
function update(groupId, groupName, groupEmail, groupDescription){
    return new Promise(async(resolve, reject) => {
        const psql ="UPDATE distribution_list " +
                    "SET group_name = $1, " +
                    "group_email = $3 ," +
                    "group_description = $4 " +
                    "WHERE = group_id = $2";
        const values = [ groupName, groupId, groupEmail, groupDescription ]

        pool.connect()
        .then(client => {
            return client
            .query(psql, values)
            .then(res => {
                client.release()
                return resolve(res.rows)
            })
            .catch(err => {
                client.release()
                return reject(err.stack)
            })
        })
    })
}


// deleting an employe
function deleteDistributionList(group_id) {
    return new Promise(async(resolve, reject) => {
        const psql =  "DELETE " +
                    "FROM distribution_list " +
                    "WHERE group_id = $1 ";
        const values = [groupId]

        pool.connect()
        .then(client => {
            return client
            .query(psql, values)
            .then(res => {
                client.release()
                return resolve(res.rows)
            })
            .catch(err => {
                client.release()
                return reject(err.stack)
            })
        })
    })
}

module.exports = {findAll, search, find, findByGroupName, insert, update, deleteDistributionList}