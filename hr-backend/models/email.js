// getting nodemailer for sending emails
const nodemailer = require('nodemailer')


// sending emails to one or more users
function sendEmail(subject, message, email, cc) {

    return new Promise(async (resolve, reject) => {

      var transporter = nodemailer.createTransport({
          host: process.env.SUPPORT_EMAIL_HOST, // email hostname
          port: process.env.SUPPORT_EMAIL_PORT, //SMTP port
          secureConnection: false,
          auth: {
            user:  process.env.SUPPORT_EMAIL,
            pass:  process.env.SUPPORT_EMAIL_PASSWORD
          }
        });
                  
        var mailOptions = {
          from: process.env.SUPPORT_EMAIL,
          to: email,
          cc: cc,
          subject: subject,
          text: message
        };  
  
        await transporter.sendMail(mailOptions, function(error, info){
            if (error) {
              console.log(error);
              return reject(error);
            } else {
              return resolve(info);
            }
        });
    });
  }




// sending emails to one or more users
function sendHTMLEmail(subject, emailHTML, email, cc){

  return new Promise(async (resolve, reject) => {

    var transporter = nodemailer.createTransport({
        pool: true,
        host: process.env.SUPPORT_EMAIL_HOST, // email hostname
        port: process.env.SUPPORT_EMAIL_PORT, //SMTP port
        secureConnection: false,
        auth: {
          user:  process.env.SUPPORT_EMAIL,
          pass:  process.env.SUPPORT_EMAIL_PASSWORD
        }
      });
      
      var mailOptions = {
      from: process.env.SUPPORT_EMAIL,
      to: email,
      cc: cc,
      subject: subject,
      html: emailHTML
      };  

      await transporter.sendMail(mailOptions, function(error, info){
          if (error) {
            return reject(error);
          } else {
            return resolve(info);
          }
      });
  });
}




// sending the new hire email to HQ
function sendNewHireHTMLEmail(subject, emailHTML, email, filename, path){

  return new Promise(async (resolve, reject) => {

    var transporter = nodemailer.createTransport({
        pool: true,
        host: process.env.SUPPORT_EMAIL_HOST, // email hostname
        port: process.env.SUPPORT_EMAIL_PORT, //SMTP port
        secureConnection: false,
        auth: {
          user:  process.env.SUPPORT_EMAIL,
          pass:  process.env.SUPPORT_EMAIL_PASSWORD
        }
      });
      
      var mailOptions = {
                        from: process.env.SUPPORT_EMAIL,
                        to: email,
                        subject: subject,
                        html: emailHTML,
                        attachments: [
                          {
                            filename: filename,
                            path: path
                          }
                        ]
                      };  

      await transporter.sendMail(mailOptions, function(error, info){
          if (error) {
            return reject(error);
          } else {
            return resolve(info);
          }
      });
  });
}







  module.exports = { sendEmail, sendHTMLEmail, sendNewHireHTMLEmail }