// getting the psql database connection
var pool = require("../database/connection");




// finding all the birthday_month
function findAll() {

    return new Promise(async(resolve, reject) => {
        const psql= "SELECT * " +
                    "FROM birthday_month";

        pool.connect()
        .then(client => {
            return client
            .query(psql)
            .then(res => {
                client.release()
                return resolve(res.rows)
            })
            .catch(err => {
                client.release()
                return reject(err.stack)
            })
        })
    })
}


// finding a birthday month by its id
function find(id) {

    return new Promise(async(resolve, reject) => {
        const psql = "SELECT * " +
                    "FROM birthday_month " +
                    "WHERE id = $1 ";
        const values = [id]

        pool.connect()
        .then(client => {
            return client
            .query(psql, values)
            .then(res => {
                client.release()
                return resolve(res.rows)
            })
            .catch(err => {
                client.release()
                return reject(err.stack)
            })
        })
    })
}



// finding a birthday month type by the employee id
function findByEmployeeId(employeeId) {
    return new Promise(async(resolve, reject) => {
        const psql = "SELECT * " +
                    "FROM birthday_month " +
                    "WHERE employee_id = $1 ";

        const values = [employeeId]

        pool.connect()
        .then(client => {
            return client
            .query(psql, values)
            .then(res => {
                client.release()
                return resolve(res.rows)
            })
            .catch(err => {
                client.release()
                return reject(err.stack)
            })
        })
    })
}




// inserting the birthday month
function insert(employeeId) {
    return new Promise(async(resolve, reject) => {
        const psql = "INSERT INTO birthday_month " +
                    "(employee_id,  date) " +
                    "VALUES ($1, NOW()) ";
        const values = [employeeId]

        pool.connect()
        .then(client => {
            return client
            .query(psql, values)
            .then(res => {
                client.release()
                return resolve(res.rows)
            })
            .catch(err => {
                client.release()
                return reject(err.stack)
            })
        })
    })
}

// updating the birthday month
function update (id, employeeId) {
    return new Promise(async(resolve, reject) => {
        const psql ="UPDATE birthday_month " +
                    "SET employee_id = $2 " +
                    "WHERE years = $1";
        const values = [id, employeeId]

        pool.connect()
        .then(client => {
            return client
            .query(psql, values)
            .then(res => {
                client.release()
                return resolve(res.rows)
            })
            .catch(err => {
                client.release()
                return reject(err.stack)
            })
        })
    })
}


// deleting the birthday month
function deleteBirthdayMonth (id) {
    return new Promise(async(resolve, reject) => {
        const psql =  "DELETE " +
                    "FROM birthday_month " +
                    "WHERE id = $1 ";
        const values = [id]

        pool.connect()
        .then(client => {
            return client
            .query(psql, values)
            .then(res => {
                client.release()
                return resolve(res.rows)
            })
            .catch(err => {
                client.release()
                return reject(err.stack)
            })
        })
    })
}

module.exports = {findAll, find, findByEmployeeId, insert, update, deleteBirthdayMonth}