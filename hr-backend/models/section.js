/*
|-----------------------------------------------|
|SOFTWARE ENGINEER: Robin B. Mkuwira            |
|PROJECT NAME: HR Application                   |
|DATE: 02/06/2021                               |
|                                               |
|Description:                                   |
|Below is the code for the sections             |
| model that is used to read, write, update and |
|delete the sections table in the HR application|
|database system.                               |
|-----------------------------------------------|
*/

// getting the psql database connection
var pool = require("../database/connection");

// finding all the sections
function findAll() {

    return new Promise(async(resolve, reject) => {
        const psql= "SELECT * " +
                    "FROM section";

        pool.connect()
        .then(client => {
            return client
            .query(psql)
            .then(res => {
                client.release()
                return resolve(res.rows)
            })
            .catch(err => {
                client.release()
                return reject(err.stack)
            })
        })
    })
}


// finding a specific section
function find(id) {
    return new Promise(async(resolve, reject) => {
        const psql= "SELECT * " +
                    "FROM section " +
                    "WHERE id = $1 ";
        const values = [id]

        pool.connect()
        .then(client => {
            return client
            .query(psql, values)
            .then(res => {
                client.release()
                return resolve(res.rows)
            })
            .catch(err => {
                client.release()
                return reject(err.stack)
            })
        })
    })
}


// inserting a section
function insert(name, departmentId) {
    return new Promise(async(resolve, reject) => {
        const psql = "INSERT INTO section " +
                    "(name, dept_id)" +
                    "VALUES ($1, $2) ";
        const values = [name, departmentId]

        pool.connect()
        .then(client => {
            return client
            .query(psql, values)
            .then(res => {
                client.release()
                return resolve(res.rows)
            })
            .catch(err => {
                client.release()
                return reject(err.stack)
            })
        })
    })
}




// inserting a section
function update(name, departmentId, id) {
    return new Promise(async(resolve, reject) => {
        const psql = "UPDATE section " +
                     "SET name = ? , " +
                     "dept_id = ? " +
                     "WHERE id = ? ";
        const values = [name, departmentId, id]

        pool.connect()
        .then(client => {
            return client
            .query(psql, values)
            .then(res => {
                client.release()
                return resolve(res.rows)
            })
            .catch(err => {
                client.release()
                return reject(err.stack)
            })
        })
    })
}


function deleteSection(id) {
    return new Promise(async(resolve, reject) => {
        const psql= "DELETE " +
                    "FROM section " +
                    "WHERE id = $1 ";
        const values = [id]

        pool.connect()
        .then(client => {
            return client
            .query(psql, values)
            .then(res => {
                client.release()
                return resolve(res.rows)
            })
            .catch(err => {
                client.release()
                return reject(err.stack)
            })
        })
    })

}


module.exports = {findAll, find, insert, update, deleteSection}