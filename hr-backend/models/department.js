// getting the psql database connection
var pool = require("../database/connection");




// finding all the employees
function search(searchText) {

    return new Promise(async(resolve, reject) => {

        const psql= "SELECT * " +
                    "FROM department " +
                    "WHERE department_name  ILIKE $1 ";
        const values = ['%' + searchText + '%']
        pool.connect()
        .then(client => {
            client
            .query(psql, values)
            .then(res => {
                client.release()
                // return res.rows
                return resolve(res.rows)
            })
            .catch(err => {
                client.release()
                return reject(err.stack)
            })
        })
    })
}

// finding all the departments
function findAll() {
    return new Promise(async(resolve, reject) => {
        const psql= "SELECT * " +
                    "FROM department";

        pool.connect()
        .then(client => {
            return client
            .query(psql)
            .then(res => {
                client.release()
                return resolve(res.rows)
            })
            .catch(err => {
                client.release()
                return reject(err.stack)
            })
        })
    })
}


// finding a specific a distribution by the department id
function find(departmentId) {

    return new Promise(async(resolve, reject) => {
        const psql = "SELECT * " +
                    "FROM department " +
                    "WHERE department_id = $1 ";
        const values = [departmentId]

        pool.connect()
        .then(client => {
            return client
            .query(psql, values)
            .then(res => {
                client.release()
                return resolve(res.rows)
            })
            .catch(err => {
                client.release()
                return reject(err.stack)
            })
        })
    })

}




// finding a specific a distribution by the department id
function findByName(departmentName) {

    return new Promise(async(resolve, reject) => {
        const psql = "SELECT * " +
                    "FROM department " +
                    "WHERE department_name = $1 ";
        const values = [departmentName]

        pool.connect()
        .then(client => {
            return client
            .query(psql, values)
            .then(res => {
                client.release()
                return resolve(res.rows)
            })
            .catch(err => {
                client.release()
                return reject(err.stack)
            })
        })
    })

}





// inserting a department
function insert(departmentId, departmentName) {
    return new Promise(async(resolve, reject) => {
        const psql = "INSERT INTO department " +
                    "(department_id, group_name) " +
                    "VALUES ($1, $2) ";
        const values = [departmentId, departmentName]

        pool.connect()
        .then(client => {
            return client
            .query(psql, values)
            .then(res => {
                client.release()
                return resolve(res.rows[0])
            })
            .catch(err => {
                client.release()
                return reject(err.stack)
            })
        })
    })
}

// updating the department
function update(departmentId, departmentName){
    return new Promise(async(resolve, reject) => {
        const psql ="UPDATE department " +
                    "SET department_name = $1 " +
                    "WHERE = department_id = $2";
        const values = [ departmentId, departmentName ]

        pool.connect()
        .then(client => {
            return client
            .query(psql, values)
            .then(res => {
                client.release()
                return resolve(res.rows[0])
            })
            .catch(err => {
                client.release()
                return reject(err.stack)
            })
        })
    })
}


// deleting the department
function deleteDepartment(departmentId) {
    return new Promise(async(resolve, reject) => {
        const psql =  "DELETE " +
                    "FROM department " +
                    "WHERE department_id = $1 ";
        const values = [departmentId]

        pool.connect()
        .then(client => {
            return client
            .query(psql, values)
            .then(res => {
                client.release()
                return resolve(res.rows[0])
            })
            .catch(err => {
                client.release()
                return reject(err.stack)
            })
        })
    })
}

module.exports = {search, findAll, find, findByName, insert, update, deleteDepartment}