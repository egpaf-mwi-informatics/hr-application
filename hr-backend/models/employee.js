// getting the bcrypt library
const bcrypt = require("bcrypt");
const saltRounds = 10;
// getting the psql database connection
var pool = require("../database/connection");

// generates a random birthday message
function birthdayMessageGenerator (fullname) {
    const regards = "\n\n" +
                    "Regards,\n" +
                    "Elizabeth Glaser Pediatric Aids Foundation."

    const messages = [
        "Happy Birthday " + fullname + ", \n\n" +
        "Wishing you a great birthday and a memorable year. From all of us." + regards,

        "Happy Birthday " + fullname + ", \n\n" +
        "Enjoy your special day." + regards,

        "Happy Birthday " + fullname + ", \n\n" +
        "Have the best birthday ever!" + regards,

        "Happy Birthday " + fullname + ", \n\n" +
        "The day is all yours, have fun!" + regards,

        "Happy Birthday " + fullname + ", \n\n" +
        "We raise the glass to your health, happiness, and joy. " +
        "May this birthday start a year of achievements and adventures. We know you want them! So Happy Birthday from all of us, and have a great year." + regards,

        "Happy Birthday " + fullname + ", \n\n" +
        "Wishing you much happiness on your special day. Have an unforgettable birthday." + regards,

        "Happy Birthday " + fullname + ", \n\n" +
        "Wishing you a happy birthday, a wonderful year and success in all you do." + regards,

        "Happy Birthday " + fullname + ", \n\n" +
        "We appreciate all of your hard work in the past year. Wishing you a happy and relaxing birthday!" + regards
    ]

    const size = messages.length
    const message = messages[Math.floor(Math.random() * size)]

    return message
}

// finding all the employees by the between end dates
function startDateFilter(startDate, endDate) {

    return new Promise(async(resolve, reject) => {

        const psql = "SELECT associate_id, first_name, last_name, position_name, office_name, department_name, email, sex, start_date, end_date, contract_id, position_id, office.office_id AS office_id, department.department_id AS department_id " +
                     "FROM employee_tbl " +
                     "JOIN contract " +
                     "ON (employee_tbl.associate_id = contract.employee_id) " +
                     "JOIN sections " +
                     "ON (sections.id = contract.section_id) " +
                     "JOIN department " +
                     "ON (department.department_id = sections.dept_id ) " +
                     "JOIN positions " +
                     "ON (positions.id = contract.position_id) " +
                     "JOIN office " +
                     "ON (office.office_id = contract.office_id) " +
                     "WHERE end_date BETWEEN $1 AND $2 ";

        const values = [startDate, endDate]
        pool.connect()
        .then(client => {
            client
            .query(psql, values)
            .then(res => {
                client.release()
                // return res.rows
                return resolve(res.rows)
            })
            .catch(err => {
                client.release()
                return reject(err.stack)
            })
        })
    })
}




// finding all the employees by the between end dates
function findByEmployeePosition(positionName) {

    return new Promise(async(resolve, reject) => {

        const psql = "SELECT associate_id, first_name, last_name, position_name, office_name, department_name, email, sex, start_date, end_date, contract_id, position_id, office.office_id AS office_id, department.department_id AS department_id " +
                     "FROM employee_tbl " +
                     "JOIN contract " +
                     "ON (employee_tbl.associate_id = contract.employee_id) " +
                     "JOIN sections " +
                     "ON (sections.id = contract.section_id) " +
                     "JOIN department " +
                     "ON (department.department_id = sections.dept_id ) " +
                     "JOIN positions " +
                     "ON (positions.id = contract.position_id) " +
                     "JOIN office " +
                     "ON (office.office_id = contract.office_id) " +
                     "WHERE position_name = $1 ";

        const values = [positionName]
        pool.connect()
        .then(client => {
            client
            .query(psql, values)
            .then(res => {
                client.release()
                // return res.rows
                return resolve(res.rows)
            })
            .catch(err => {
                client.release()
                return reject(err.stack)
            })
        })
    })
}















// finding all the employees by the between end dates
function startDateProbationFilter(startDate, endDate, contractId) {

    return new Promise(async(resolve, reject) => {

        const psql = "SELECT associate_id, first_name, last_name, position_name, office_name, department_name, email, sex, start_date, end_date, contract_id, position_id, office.office_id AS office_id, department.department_id AS department_id " +
                     "FROM employee_tbl " + 
                     "JOIN contract " +
                     "ON (employee_tbl.associate_id = contract.employee_id) " + 
                     "JOIN sections " +
                     "ON (sections.id = contract.section_id) " +
                     "JOIN department " +
                     "ON (department.department_id = sections.dept_id ) " +
                     "JOIN positions " +
                     "ON (positions.id = contract.position_id) " + 
                     "JOIN office " +
                     "ON (office.office_id = contract.office_id) " +
                     "WHERE (end_date BETWEEN $1 AND $2) AND contract_id = $3 ";

        const values = [startDate, endDate, contractId]
        pool.connect()
        .then(client => {
            client
            .query(psql, values)
            .then(res => {
                client.release()
                // return res.rows
                return resolve(res.rows)
            })
            .catch(err => {
                client.release()
                return reject(err.stack)
            })
        })
    })
}








// finding all the employees by the date and probation
function probationFilter(startDate, endDate) {

    return new Promise(async(resolve, reject) => {

        const psql = "SELECT associate_id, first_name, last_name, position_name, office_name, department_name, email, sex, start_date, end_date, contract_id, position_id, office.office_id AS office_id, department.department_id AS department_id " +
                     "FROM employee_tbl " + 
                     "JOIN contract " +
                     "ON (employee_tbl.associate_id = contract.employee_id) " +
                     "JOIN sections " +
                     "ON (sections.id = contract.section_id) " +
                     "JOIN department " +
                     "ON (department.department_id = sections.dept_id ) " +
                     "JOIN positions " +
                     "ON (positions.id = contract.position_id) " + 
                     "JOIN office " +
                     "ON (office.office_id = contract.office_id) " +
                     "WHERE end_date BETWEEN $1 AND $2 ";

        const values = [startDate, endDate]
        pool.connect()
        .then(client => {
            client
            .query(psql, values)
            .then(res => {
                client.release()
                // return res.rows
                return resolve(res.rows)
            })
            .catch(err => {
                client.release()
                return reject(err.stack)
            })
        })
    })
}



// finding all the employees by their ending contract
function endContractFilter(startDate, endDate) {

    return new Promise(async(resolve, reject) => {

        const psql = "SELECT associate_id, first_name, last_name, position_name, office_name, department_name, email, sex, start_date, end_date, contract_id, position_id, office.office_id AS office_id, department.department_id AS department_id " + 
                     "FROM employee_tbl " + 
                     "JOIN contract " +
                     "ON (employee_tbl.associate_id = contract.employee_id) " + 
                     "JOIN sections " +
                     "ON (sections.id = contract.section_id) " +
                     "JOIN department " +
                     "ON (department.department_id = sections.dept_id ) " +
                     "JOIN positions " +
                     "ON (positions.id = contract.position_id) " + 
                     "JOIN office " +
                     "ON (office.office_id = contract.office_id) " +
                     "WHERE end_date BETWEEN $1 AND $2 ";

        const values = [startDate, endDate]
        pool.connect()
        .then(client => {
            client
            .query(psql, values)
            .then(res => {
                client.release()
                // return res.rows
                return resolve(res.rows)
            })
            .catch(err => {
                client.release()
                return reject(err.stack)
            })
        })
    })
}






// finding all the employees
function search(searchText) {

    return new Promise(async(resolve, reject) => {

        const psql= "SELECT associate_id, first_name, last_name, position_name, office_name, department_name, email, sex, start_date, end_date, contract_id, position_id, office.office_id AS office_id, department.department_id AS department_id " +
                    "FROM employee_tbl " +
                    "JOIN contract " +
                    "ON (employee_tbl.associate_id = contract.employee_id) " + 
                    "JOIN sections " +
                    "ON (sections.id = contract.section_id) " +
                    "JOIN department " +
                    "ON (department.department_id = sections.dept_id ) " +
                    "JOIN positions " +
                    "ON (positions.id = contract.position_id) " + 
                    "JOIN office " +
                    "ON (office.office_id = contract.office_id) " +
                    "WHERE first_name  ILIKE $1 OR  last_name ILIKE $1 OR email ILIKE $1 OR associate_id ILIKE $1 ";
        const values = ['%' + searchText + '%']
        pool.connect()
        .then(client => {
            client
            .query(psql, values)
            .then(res => {
                client.release()
                // return res.rows
                return resolve(res.rows)
            })
            .catch(err => {
                client.release()
                return reject(err.stack)
            })
        })
    })
}





// finding all the employees
function findByIdFilter(employeeId) {

    return new Promise(async(resolve, reject) => {

        const psql= "SELECT associate_id, first_name, last_name, position_name, office_name, department_name, email, sex, start_date, end_date, contract_id, position_id, office.office_id AS office_id, department.department_id AS department_id " +
                    "FROM employee_tbl " +
                    "JOIN contract " +
                    "ON (employee_tbl.associate_id = contract.employee_id) " + 
                    "JOIN sections " +
                    "ON (sections.id = contract.section_id) " +
                    "JOIN department " +
                    "ON (department.department_id = sections.dept_id ) " +
                    "JOIN positions " +
                    "ON (positions.id = contract.position_id) " + 
                    "JOIN office " +
                    "ON (office.office_id = contract.office_id) " +
                    "WHERE employee_tbl.associate_id = $1 ";
        const values = [employeeId]
        pool.connect()
        .then(client => {
            client
            .query(psql, values)
            .then(res => {
                client.release()
                // return res.rows
                return resolve(res.rows)
            })
            .catch(err => {
                client.release()
                return reject(err.stack)
            })
        })
    })
}


// finding the employees data for the HQ email
function findByIdHQEmail(employeeId) {

    return new Promise(async(resolve, reject) => {

        const psql= "SELECT associate_id, first_name, last_name, position_name, office_name, department_name, email, sex, TO_CHAR(start_date::DATE,'dd Month yyyy') AS start_date, end_date, contract_id, position_id, office.office_id AS office_id, department.department_id AS department_id " +
                    "FROM employee_tbl " +
                    "JOIN contract " +
                    "ON (employee_tbl.associate_id = contract.employee_id) " +
                    "JOIN sections " +
                    "ON (sections.id = contract.section_id) " +
                    "JOIN department " +
                    "ON (department.department_id = sections.dept_id ) " +
                    "JOIN positions " +
                    "ON (positions.id = contract.position_id) " +
                    "JOIN office " +
                    "ON (office.office_id = contract.office_id) " + 
                    "WHERE end_date > NOW() AND employee_tbl.associate_id = $1 " +
                    "ORDER BY end_date DESC";
        const values = [employeeId]
        pool.connect()
        .then(client => {
            client
            .query(psql, values)
            .then(res => {
                client.release()
                // return res.rows
                return resolve(res.rows)
            })
            .catch(err => {
                client.release()
                return reject(err.stack)
            })
        })
    })
}







// finding all the employees
function findAllEmployees() {

    return new Promise(async(resolve, reject) => {

        const psql= "SELECT * " +
                    "FROM employee_tbl " +
                    "ORDER BY first_name ASC";

        pool.connect()
        .then(client => {
            client
            .query(psql)
            .then(res => {
                client.release()
                // return res.rows
                return resolve(res.rows)
            })
            .catch(err => {
                client.release()
                return reject(err.stack)
            })
        })
    })
}








// finding all the employees
function findAll() {

    return new Promise(async(resolve, reject) => {

        const psql= "SELECT associate_id, first_name, last_name, position_name, office_name, department_name, email, sex, start_date, end_date, contract_id, position_id, office.office_id AS office_id, department.department_id AS department_id " +
                    "FROM employee_tbl " + 
                    "JOIN contract " +
                    "ON (employee_tbl.associate_id = contract.employee_id) " +
                    "JOIN office " +
                    "ON (contract.office_id = office.office_id) " + 
                    "JOIN positions " +
                    "ON (positions.id = contract.position_id) " +
                    "JOIN sections " +
                    "ON (sections.id = contract.section_id) " +
                    "JOIN department " +
                    "ON (department.department_id = sections.dept_id ) " +
                    "ORDER BY first_name ASC";

        pool.connect()
        .then(client => {
            client
            .query(psql)
            .then(res => {
                client.release()
                // return res.rows
                return resolve(res.rows)
            })
            .catch(err => {
                client.release()
                return reject(err.stack)
            })
        })
    })
}





// finding all the employee who the current date is their birthday
function findTodayBirthdays() {

    return new Promise(async(resolve, reject) => {

        const psql= "SELECT * " +
                    "FROM employee_tbl " +
                    "WHERE to_char(date_of_birth, 'dd.mm') = to_char(NOW(), 'dd.mm')";

        pool.connect()
        .then(client => {
            client
            .query(psql)
            .then(res => {
                client.release()
                // return res.rows
                return resolve(res.rows)
            })
            .catch(err => {
                client.release()
                return reject(err)
            })
        })
    })
}





// finding all the employee who the current date is their birthday
function exists(employeeId) {

    return new Promise(async(resolve, reject) => {

        const psql= "SELECT * " +
                    "FROM employee_tbl " +
                    "WHERE to_char(date_of_birth, 'dd.mm') = to_char(NOW(), 'dd.mm') " +
                    "AND associate_id = $1";
        const values = [employeeId]
        pool.connect()
        .then(client => {
            client
            .query(psql, values)
            .then(res => {
                client.release()
                // return res.rows
                return resolve(res.rows)
            })
            .catch(err => {
                client.release()
                return reject(err)
            })
        })
    })
}













// finding all the employees
function findNew() {

    return new Promise(async(resolve, reject) => {

        const psql= "SELECT associate_id, first_name, last_name, email, sex, contract_id, start_date, end_date, office_name, department_name, position_name " +
                    "FROM employee_tbl " + 
                    "JOIN contract " +
                    "ON (employee_tbl.associate_id = contract.employee_id) " +
                    "JOIN  office " +
                    "ON (contract.office_id = office.office_id) " +
                    "JOIN sections " +
                    "ON (sections.id = contract.section_id) " +
                    "JOIN department " +
                    "ON (department.department_id = sections.dept_id ) " +
                    "JOIN positions " +
                    "ON (contract.position_id = positions.id) " +
                    "WHERE associate_id NOT IN ( " +
                    "    SELECT employee_id " +
                    "    FROM employee_distribution " + 
                    ")";

        pool.connect()
        .then(client => {
            client
            .query(psql)
            .then(res => {
                client.release()
                // return res.rows
                return resolve(res.rows)
            })
            .catch(err => {
                client.release()
                return reject(err.stack)
            })
        })
    })
}



// finding the employee whose contract is about to expire in a month time
function findContractToExpire() {

    return new Promise(async(resolve, reject) => {

        const psql= `SELECT contract.contract_id AS id, first_name, last_name, start_date, end_date, position_id, TO_CHAR(end_date::DATE,'dd Mon yyyy') AS exact_expiry_month, status, department_name, office_name, email, description, TO_CHAR(contract.end_date, 'Month') AS "expiry_month" ` +
                    "FROM contract " +
                    "JOIN employee_tbl " + 
                    "ON (employee_tbl.associate_id = contract.employee_id) " + 
                    "JOIN sections " +
                    "ON (contract.section_id = sections.id) " + 
                    "JOIN department " +
                    "ON (department.department_id = sections.dept_id) " + 
                    "JOIN office " +
                    "ON (office.office_id = contract.office_id) " +
                    "WHERE  EXTRACT(MONTH FROM end_date) = (EXTRACT(MONTH FROM CURRENT_DATE) + 1) " +
                    "AND EXTRACT(YEAR FROM end_date) = EXTRACT(YEAR FROM  CURRENT_DATE) " +
                    "AND description ILIKE '%Active%' ";

        pool.connect()
        .then(client => {
            client
            .query(psql)
            .then(res => {
                client.release()
                // return res.rows
                return resolve(res.rows)
            })
            .catch(err => {
                client.release()
                return reject(err.stack)
            })
        })
    })
}




// finding the employee per department chart
function findEmployeeDepartmentChart() {

    return new Promise(async(resolve, reject) => {

        const psql= "SELECT department_name AS label, count(associate_id) AS count " +
                    "FROM employee_tbl " +
                    "JOIN contract " +
                    "ON (employee_tbl.associate_id = contract.employee_id) " +
                    "JOIN sections " +
                    "ON (sections.id = contract.section_id) " +
                    "JOIN department " +
                    "ON (department.department_id = sections.dept_id ) " +
                    "GROUP BY department_name";

        pool.connect()
        .then(client => {
            client
            .query(psql)
            .then(res => {
                client.release()
                // return res.rows
                return resolve(res.rows)
            })
            .catch(err => {
                client.release()
                return reject(err.stack)
            })
        })
    })
}


// finding the employee per department chart
function findEmployeeDepartmentStackedChart(officeName, departmentName) {

    return new Promise(async(resolve, reject) => {

        const psql= "SELECT * " +
                    "FROM employee_tbl " +
                    "JOIN contract " +
                    "ON (employee_tbl.associate_id = contract.employee_id) " +
                    "JOIN office " +
                    "ON (contract.office_id = office.office_id) " +
                    "JOIN sections " +
                    "ON (sections.id = contract.section_id) " +
                    "JOIN department " +
                    "ON (department.department_id = sections.dept_id ) " +
                    "WHERE office_name = $1 AND department_name = $2 ";
        
        const values = [officeName, departmentName]

        pool.connect()
        .then(client => {
            client
            .query(psql, values)
            .then(res => {
                client.release()
                return resolve(res.rows)
            })
            .catch(err => {
                client.release()
                return reject(err.stack)
            })
        })
    })
}





// findind the employee per department chart
function findEmployeeDistrictChart() {

    return new Promise(async(resolve, reject) => {

        const psql= "SELECT office_name AS label, count(associate_id) AS count " +
                    "FROM employee_tbl " +
                    "JOIN contract " +
                    "ON (employee_tbl.associate_id = contract.employee_id) " +
                    "JOIN office " +
                    "ON (contract.office_id = office.office_id) " +
                    "GROUP BY office_name ";

        pool.connect()
        .then(client => {
            client
            .query(psql)
            .then(res => {
                client.release()
                // return res.rows
                return resolve(res.rows)
            })
            .catch(err => {
                client.release()
                return reject(err.stack)
            })
        })
    })
}


// findind the employee per department chart
function findGenderDepartmentChart() {

    return new Promise(async(resolve, reject) => {

        const psql= "SELECT sex AS label, count(associate_id) AS count, department_name " +
                    "FROM employee_tbl " +
                    "JOIN contract " +
                    "ON (employee_tbl.associate_id = contract.employee_id) " +
                    "JOIN sections " +
                    "ON (sections.id = contract.section_id) " +
                    "JOIN department " +
                    "ON (department.department_id = sections.dept_id ) " +
                    "GROUP BY sex, department_name";

        pool.connect()
        .then(client => {
            client
            .query(psql)
            .then(res => {
                client.release()
                // return res.rows
                return resolve(res.rows[0])
            })
            .catch(err => {
                client.release()
                return reject(err.stack)
            })
        })
    })
}




// findind the employee per department chart
function findGenderOrganizationalChart() {

    return new Promise(async(resolve, reject) => {

        const psql= "SELECT sex AS label, count(associate_id) AS count " +
                    "FROM employee_tbl " +
                    "GROUP BY sex ";

        pool.connect()
        .then(client => {
            client
            .query(psql)
            .then(res => {
                client.release()
                // return res.rows
                return resolve(res.rows)
            })
            .catch(err => {
                client.release()
                return reject(err.stack)
            })
        })
    })
}


// finding the employee per department chart
function findDepartmentExpiredContractChart() {

    return new Promise(async(resolve, reject) => {

        const psql= "SELECT department_name AS label, COUNT(DISTINCT(associate_id)) AS count " +
                    "FROM employee_tbl " +
                    "JOIN contract " +
                    "ON (employee_tbl.associate_id = contract.employee_id) " +
                    "JOIN sections " +
                    "ON (sections.id = contract.section_id) " +
                    "JOIN department " +
                    "ON (department.department_id = sections.dept_id ) " +
                    "WHERE end_date < CURRENT_DATE AND contract_id IN ( " +
                    "    SELECT MAX(contract_id) AS contract_id " +
                    "    FROM contract " +
                    "    WHERE description ILIKE '%Expired%' " + 
                    "    GROUP BY employee_id	 " +
                    ") " +
                    "GROUP BY department_name";

        pool.connect()
        .then(client => {
            client
            .query(psql)
            .then(res => {
                client.release()
                // return res.rows
                return resolve(res.rows)
            })
            .catch(err => {
                client.release()
                return reject(err.stack)
            })
        })
    })
}


// finding a specific employee by id
function find(associateId) {

    return new Promise(async(resolve, reject) => {
        const psql = "SELECT * " +
                    "FROM employee_tbl " +
                    "WHERE associate_id = $1 ";
        const values = [associateId]
        pool.connect()
        .then(client => {
             client
            .query(psql, values)
            .then(res => {
                client.release()
                return resolve(res.rows)
            })
            .catch(err => {
                client.release()
                return reject(err.stack)
            })
        })
    })
}



// finding all the birthdays in that month
function findBirthdayMonth () {

    return new Promise(async(resolve, reject) => {
        const psql= "SELECT associate_id, first_name, last_name, email, sex, " +
                    "TO_CHAR(date_of_birth::DATE,'dd Month') AS date_of_birth, attempt, masm_id " +
                    "FROM employee_tbl " +
                    "JOIN contract " +
                    "ON (employee_tbl.associate_id = contract.employee_id) " +
                    "WHERE  EXTRACT(MONTH from date_of_birth) = (EXTRACT(MONTH FROM CURRENT_DATE)) " +
                    "AND TO_CHAR(CURRENT_DATE, 'YYYY-MM') NOT IN ( " +
                    "    SELECT TO_CHAR(date, 'YYYY-MM') " + 
                    "    FROM birthday_month " +
                    ") " +
                    "AND contract.description ILIKE '%Active%' " +
                    "ORDER BY date_of_birth ASC";

        pool.connect()
        .then(client => {
            return client
            .query(psql)
            .then(res => {
                client.release()
                return resolve(res.rows)
            })
            .catch(err => {
                client.release()
                return reject(err.stack)
            })
        })
    })
}





// finding a specific employee by id
function findHR() {

    return new Promise(async(resolve, reject) => {
        // first_name, last_name, email, position_name, description, status, end_date 
        const psql = "SELECT DISTINCT email, associate_id, first_name, last_name  " +
                     "FROM employee_tbl " +
                     "JOIN contract " +
                     "ON (employee_tbl.associate_id = contract.employee_id) " +
                     "JOIN positions " +
                     "ON (contract.position_id = positions.id) " +
                     "WHERE (position_name ILIKE '%Senior HR & Administration Officer%' " +
                     "OR position_name ILIKE '%Senior HR & Administration Manager%' " +
                     "OR position_name ILIKE '%HR & Administration Officer%' " +
                     ") ";
                     // "AND description != 'Expired'";

        pool.connect()
        .then(client => {
             client
            .query(psql)
            .then(res => {
                client.release()
                return resolve(res.rows)
            })
            .catch(err => {
                client.release()
                return reject(err.stack)
            })
        })
    })
}



// finding a specific employee by id
function findByEmail(email) {

    return new Promise(async(resolve, reject) => {
        const psql = "SELECT associate_id, first_name, last_name, email, sex, date_of_birth, contract_id, position_id, position_name " +
                     "FROM employee_tbl " +
                     "JOIN contract " +
                     "ON (employee_tbl.associate_id = contract.employee_id) " +
                     "JOIN positions " +
                     "ON (positions.id = contract.position_id) " +
                     "WHERE email = $1 ";
        const values = [email]

        pool.connect()
        .then(client => {
            return client
            .query(psql, values)
            .then(res => {
                client.release()
                return resolve(res.rows)
            })
            .catch(err => {
                client.release()
                return reject(err.stack)
            })
        })
    })

}




// finding a specific employee by id
function findByStatus(status) {

    return new Promise(async(resolve, reject) => {

        const psql = "SELECT employee_id, MAX(end_date) " +
                     "FROM contract " +
                     "JOIN status " +
                     "ON(status.id = contract.status) " +
                     "WHERE status.status ILIKE $1 " +
                     "GROUP BY employee_id";

        const values = ["'%" + status + "%'"]

        pool.connect()
        .then(client => {
            return client
            .query(psql, values)
            .then(res => {
                client.release()
                return resolve(res.rows)
            })
            .catch(err => {
                client.release()
                return reject(err.stack)
            })
        })
    })

}



// finding a specific employee by id
function findByPip() {

    return new Promise(async(resolve, reject) => {

        const psql = "SELECT employee_id, MAX(end_date) " +
                     "FROM contract " +
                     "JOIN status " +
                     "ON(status.id = contract.status) " +
                     "WHERE contract_id IN ( " +
                     "       SELECT contract_id " +
                     "      FROM pip " +
                     "   ) " +
                     "GROUP BY employee_id";


        pool.connect()
        .then(client => {
            return client
            .query(psql)
            .then(res => {
                client.release()
                return resolve(res.rows)
            })
            .catch(err => {
                client.release()
                return reject(err.stack)
            })
        })
    })

}































function findByDates(startDate, endDate) {

    return new Promise(async(resolve, reject) => {

        const psql = "SELECT employee_id, MAX(end_date) " +
                     "FROM contract " +
                     "JOIN status " +
                     "ON(status.id = contract.status) " +
                     "WHERE end_date BETWEEN $1 AND $2 " +
                     "GROUP BY employee_id";

        const values = [startDate, endDate]

        pool.connect()
        .then(client => {
            return client
            .query(psql, values)
            .then(res => {
                client.release()
                return resolve(res.rows)
            })
            .catch(err => {
                client.release()
                return reject(err.stack)
            })
        })
    })

}




function findByStartDateBetween(startDate, endDate, statusId) {

    return new Promise(async(resolve, reject) => {

        const psql = "SELECT employee_id, MAX(end_date) " +
                     "FROM contract " +
                     "JOIN status " +
                     "ON(status.id = contract.status) " +
                     "WHERE start_date BETWEEN $1 AND $2 AND status.id = $3  " +
                     "GROUP BY employee_id";

        const values = [startDate, endDate, statusId]

        pool.connect()
        .then(client => {
            return client
            .query(psql, values)
            .then(res => {
                client.release()
                return resolve(res.rows)
            })
            .catch(err => {
                client.release()
                return reject(err.stack)
            })
        })
    })

}


// inserting an employee
function insert(associateId, email, dateOfBirth, sex, empPassword, firstname, lastname, masmId) {
    return new Promise(async(resolve, reject) => {
        const psql = "INSERT INTO employee_tbl " +
                    "(associate_id, email, date_of_birth, sex, emp_password, first_name, last_name, masm_id) " +
                    "VALUES ($1, $2, $3, $4, $5, $6, $7, $8) ";
        const values = [associateId, email, dateOfBirth, sex, empPassword, firstname, lastname, masmId]

        pool.connect()
        .then(client => {
            return client
            .query(psql, values)
            .then(res => {
                client.release()
                return resolve(res)
            })
            .catch(err => {
                client.release()
                return reject(err)
            })
        })
    })
}


//specific employee login
function login(email, password){

    return new Promise( async (resolve, reject) => {
  
      const psql = "SELECT * " +
                  "FROM employee_tbl " +
                  "WHERE email = $1 ";
      const values = [email]

      pool.connect()
      .then(client => {
          return client
          .query(psql, values)
          .then( async res =>  {
                var emptyResults = [];
                var size = res.rows.length;
                // now validating the fetched password
                if(size != 0){
                    var hashedPassword = res.rows[0].emp_password;
                    if (hashedPassword == null || hashedPassword == undefined ) {
                        client.release()
                        return resolve(emptyResults);
                    } else {
                        var aMatch = await bcrypt.compare(password, hashedPassword);
                        if(aMatch){
                            client.release()
                            return resolve(res.rows);
                        }else{
                            client.release()
                            return resolve(emptyResults);
                        }
                    }
                }else{
                    return resolve(emptyResults);
                }
          })
          .catch(err => {
              console.log(err)
              client.release()
              return reject(err)
          })
      })




    });
  
  }

//updating an employee's password
function updatePassword(password, associateId){
   
    return new Promise(async(resolve, reject) => {
  
        const hashedPassword = await bcrypt.hash(password, saltRounds);
        const psql = "UPDATE employee_tbl " +
                     "SET emp_password = $1 " +
                     "WHERE associate_id = $2";

        const values = [hashedPassword, associateId]
        pool.connect()
        .then(client => {
            return client
            .query(psql, values)
            .then(res => {
                client.release()
                return resolve(res)
            })
            .catch(err => {
                client.release()
                return reject(err)
            })
        })
  
    });
  }


// updating an employee
function update(associateId, email, dateOfBirth, sex, empPassword, firstname, lastname, masmId){
    return new Promise( async (resolve, reject) => {
        const psql ="UPDATE employee_tbl " +
                    "SET email = $1, " +
                    "date_of_birth = $2, " +
                    "sex = $3, " +
                    "emp_password = $4, " +
                    "first_name = $5, " +
                    "last_name = $6, " +
                    "masm_id = $7 " +
                    "WHERE associate_id = $7";
        const values = [ email, dateOfBirth, sex, empPassword, firstname, lastname, associateId, masmId]

        pool.connect()
        .then(client => {
            return client
            .query(psql, values)
            .then(res => {
                client.release()
                return resolve(res)
            })
            .catch(err => {
                client.release()
                return reject(err)
            })
        })
    })
}




// updating an employee
function updateEmail(email, associateId){
    return new Promise( async (resolve, reject) => {
        const psql ="UPDATE employee_tbl " +
                    "SET email = $1 " +
                    "WHERE associate_id = $2";
        const values = [ email, associateId]

        pool.connect()
        .then(client => {
            return client
            .query(psql, values)
            .then(res => {
                client.release()
                return resolve(res)
            })
            .catch(err => {
                client.release()
                return reject(err)
            })
        })
    })
}






// deleting an employe
function deleteEmployee(associate_id) {
    const psql =  "DELETE " +
                  "FROM employee " +
                  "WHERE associate_id = $1 ";
    const values = [associate_id]

    pool.connect()
    .then(client => {
        return client
        .query(psql, values)
        .then(res => {
            client.release()
            console.log(res.rows[0])
        })
        .catch(err => {
            client.release()
            console.log(err.stack)
        })
    })
}


// finding all the employees
function findMatrix(email) {
    const axios = require("axios")
    const qs = require('qs')

    return new Promise(async(resolve, reject) => {
        // get the manager from the other systems
        // access details
        const URL = "https://www.ihss.pedaids.org/egpaf-360-api/login"
        axios.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded';
        const postData = {
                            email: process.env.SUPPORT_EMAIL,
                            password: process.env.SUPPORT_EMAIL_PASSWORD
                         }

        // the matrix detaills
        const matrixURL = "https://www.ihss.pedaids.org/egpaf-360-api/findEmployeeMatrix"
        
        axios.post(URL, qs.stringify(postData))
        .then((response) => {
            const token = response.data.token
            const escapedEmail = email.replace(/'/g, '')

            axios.defaults.headers.common['Authorization'] = `Bearer ${token}`;
            axios.get(matrixURL, { params: { searchText: escapedEmail } })
            .then((response) => {
                const matrix = response.data
                return resolve(matrix)
            })
            .catch((err) => {
                console.log('error here')
                return resolve('ok')
            })
        })
        .catch((err) => {
            return reject(err)
        })
    })
}

module.exports = { birthdayMessageGenerator, findMatrix, search, probationFilter, endContractFilter, findByPip, exists, startDateFilter, startDateProbationFilter, findByDates, findByStartDateBetween, findAll, findTodayBirthdays, findNew, findByIdHQEmail, findByStatus, findContractToExpire, findEmployeeDepartmentChart, findEmployeeDistrictChart, findByEmployeePosition, findGenderDepartmentChart, findGenderOrganizationalChart, findDepartmentExpiredContractChart, findEmployeeDepartmentStackedChart, findBirthdayMonth, find, findAllEmployees, findByIdFilter, findHR, findByEmail, login, insert, update, updatePassword, updateEmail, deleteEmployee}