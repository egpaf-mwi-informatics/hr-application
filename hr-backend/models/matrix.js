/*
|----------------------------------------------|
|SOFTWARE ENGINEER: Robin B. Mkuwira           |
|PROJECT NAME: HR Application                  |
|DATE: 02/06/2021                              |
|                                              |
|Description:                                  |
|Below is the code for the matrix model that   |
|is used to read, write, update and delete the |
|matrix table in the HR application database   |
|system.                                       |
|----------------------------------------------|
*/

// getting the psql database connection
var pool = require("../database/connection");

// finding all the matrices
function findAll() {

    return new Promise(async(resolve, reject) => {
        const psql= "SELECT * " +
                    "FROM matrix";

        pool.connect()
        .then(client => {
            return client
            .query(psql)
            .then(res => {
                client.release()
                return resolve(res.rows)
            })
            .catch(err => {
                client.release()
                return reject(err.stack)
            })
        })
    })
}


// finding a specific matrix by id
function find(id) {
    return new Promise(async(resolve, reject) => {
        const psql= "SELECT * " +
                    "FROM matrix " +
                    "WHERE id = $1 ";
        const values = [id]

        pool.connect()
        .then(client => {
            return client
            .query(psql, values)
            .then(res => {
                client.release()
                return resolve(res.rows)
            })
            .catch(err => {
                client.release()
                return reject(err.stack)
            })
        })
    })
}


// finding a specific matrix by id
function updateMatrixPositions(id, positionName) {

    return new Promise(async(resolve, reject) => {
        const psql= "UPDATE matrix " +
                    "SET position_id = $2" +
                    "WHERE position_id ILIKE $1 ";
        const values = ['%' + positionName + '%', id]

        pool.connect()
        .then(client => {
            return client
            .query(psql, values)
            .then(res => {
                client.release()
                return resolve(res.rows)
            })
            .catch(err => {
                client.release()
                return reject(err.stack)
            })
        })
    })
}
























// finding a specific contract by the employee position
function findByEmployeePosition(positionId) {
    return new Promise(async(resolve, reject) => {
        const psql= "SELECT * " +
                    "FROM matrix " +
                    "WHERE position_id = $1 ";

        const values = [positionId]

        pool.connect()
        .then(client => {
            return client
            .query(psql, values)
            .then(res => {
                client.release()
                return resolve(res.rows[0])
            })
            .catch(err => {
                client.release()
                return reject(err.stack)
            })
        })
    })

}

// inserting a matrix
function insert(matrixId, position, matrix) {
    return new Promise(async(resolve, reject) => {
        const psql = "INSERT INTO contract " +
                    "(id, emp_position, matrix)" +
                    "VALUES ($1, $2, $3) ";
        const values = [matrixId, position, matrix]

        pool.connect()
        .then(client => {
            return client
            .query(psql, values)
            .then(res => {
                client.release()
                return resolve(res.rows)
            })
            .catch(err => {
                client.release()
                return reject(err.stack)
            })
        })
    })
}

// updating the matrix
function update(id, position, matrix){

    return new Promise(async(resolve, reject) => {
        const psql ="UPDATE matrix " +
                    "SET position = $2, " +
                    "matrix = $3 " 
                    "WHERE = id = $1 ";
        const values = [id , position, matrix ]

        pool.connect()
        .then(client => {
            return client
            .query(psql, values)
            .then(res => {
                client.release()
                return resolve(res.rows)
            })
            .catch(err => {
                client.release()
                return reject(err.stack)
            })
        })
    })
}
function deleteMatrix(id) {
    return new Promise(async(resolve, reject) => {
        const psql= "DELETE " +
                    "FROM matrix " +
                    "WHERE id = $1 ";
        const values = [id]

        pool.connect()
        .then(client => {
            return client
            .query(psql, values)
            .then(res => {
                client.release()
                return resolve(res.rows)
            })
            .catch(err => {
                client.release()
                return reject(err.stack)
            })
        })
    })

}


module.exports = {findAll, find, findByEmployeePosition, insert, update, updateMatrixPositions, deleteMatrix}