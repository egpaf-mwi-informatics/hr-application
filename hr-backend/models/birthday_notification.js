/*
|-----------------------------------------------|
|SOFTWARE ENGINEER: Robin B. Mkuwira            |
|PROJECT NAME: HR Application                   |
|DATE: 02/06/2021                               |
|                                               |
|Description:                                   |
|Below is the code for the birthday notification|
| model that is used to read, write, update and |
|delete the birthday notification table in the  |
|HR application database system.                |
|-----------------------------------------------|
*/

// getting the psql database connection
var pool = require("../database/connection");





// finding all the birthday notifications
function findAll() {

    return new Promise(async(resolve, reject) => {
        const psql= "SELECT * " +
                    "FROM birthday_notification";

        pool.connect()
        .then(client => {
            return client
            .query(psql)
            .then(res => {
                client.release()
                return resolve(res.rows)
            })
            .catch(err => {
                client.release()
                return reject(err.stack)
            })
        })
    })
}



// finding all the employee who the current date is their birthday
function exists(employeeId) {

    return new Promise(async(resolve, reject) => {

        const psql= "SELECT * " +
                    "FROM birthday_notification " +
                    "WHERE to_char(notification_date, 'dd.mm.yyyy') = to_char(NOW(), 'dd.mm.yyyy') " +
                    "AND employee_id = $1";
        const values = [employeeId]
        pool.connect()
        .then(client => {
            client
            .query(psql, values)
            .then(res => {
                client.release()
                // return res.rows
                return resolve(res.rows)
            })
            .catch(err => {
                client.release()
                return reject(err)
            })
        })
    })
}




// finding a specific birthday notification
function find(id) {
    return new Promise(async(resolve, reject) => {
        const psql= "SELECT * " +
                    "FROM birtday_notification " +
                    "WHERE id = $1 ";
        const values = [id]

        pool.connect()
        .then(client => {
            return client
            .query(psql, values)
            .then(res => {
                client.release()
                return resolve(res.rows)
            })
            .catch(err => {
                client.release()
                return reject(err.stack)
            })
        })
    })
}


// inserting a birthday notification
function insert(employeeId) {
    return new Promise(async(resolve, reject) => {
        const psql = "INSERT INTO birthday_notification " +
                    "(employee_id, notification_date)" +
                    "VALUES ($1, NOW()) ";
        const values = [employeeId]

        pool.connect()
        .then(client => {
            return client
            .query(psql, values)
            .then(res => {
                client.release()
                return resolve(res.rows)
            })
            .catch(err => {
                client.release()
                return reject(err.stack)
            })
        })
    })
}


function deleteBirthdayNotification(id) {
    return new Promise(async(resolve, reject) => {
        const psql= "DELETE " +
                    "FROM birthday_notification " +
                    "WHERE id = $1 ";
        const values = [id]

        pool.connect()
        .then(client => {
            return client
            .query(psql, values)
            .then(res => {
                client.release()
                return resolve(res.rows)
            })
            .catch(err => {
                client.release()
                return reject(err.stack)
            })
        })
    })

}


module.exports = {findAll, find, insert, exists, deleteBirthdayNotification}