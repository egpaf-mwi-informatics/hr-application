/*
|---------------------------------------|
|Software Engineer: Robin B. MKuwira    |
|Date Created: 03/02/2021               |
|Project Name: HR Application           |
|                                       |
|The birthday notifications route is    |
|used to access the methods in the      |
|birtday notifications model            |
|---------------------------------------|
*/

const jwt = require('jsonwebtoken');
const express = require("express");
const router = express.Router();
const birthdayNotification = require("../models/birthday_notification");
const bodyParser = require("body-parser");
const urlencodedParser = bodyParser.urlencoded({ extended: false});


//fetching all of the birthday notifications
router.get("/findAll", async (req, res)=>{
    try{
        let results = await birthdayNotification.findAll();
        res.json(results);
    }catch(error){
        console.log(error);
        res.sendStatus(500);
    }
 });


//fetching a specific birthday notification
router.post("/find", urlencodedParser ,async (req, res) => {
    try{
        const id = req.body.id;
        const results = await birthdayNotification.find(id);
        res.json(results);
    }catch(error){
        console.log(error);
        res.sendStatus(500);
    }
});





module.exports = router;