/*
|-----------------------------------------|
|Software Engineer: Robin B. MKuwira      |
|Date Created: 03/02/2021                 |
|Project Name: HR Application             |
|                                         |
|The pip records route is used to access  |
|the methods in the pip model             |
|-----------------------------------------|
*/

const jwt = require('jsonwebtoken');
const express = require("express");
const router = express.Router();

const pip = require("../models/pip");

const bodyParser = require("body-parser");
const urlencodedParser = bodyParser.urlencoded({ extended: false});


//fetching all of the pips
router.get("/findAll", async (req, res)=>{
    try{
        let results = await pip.findAll();
        res.json(results);
    }catch(error){
        console.log(error);
        res.sendStatus(500);
    }
 });


//fetching a specific pip
router.post("/find", urlencodedParser ,async (req, res) => {
    try{
        const id = req.body.id;
        const results = await pip.find(id);
        res.json(results);
    }catch(error){
        console.log(error);
        res.sendStatus(500);
    }
});


//fetching a specific pip
router.post("/findByContractId", urlencodedParser ,async (req, res) => {
    try{
        const contractId = req.body.contractId;
        const results = await pip.findByContractId(contractId);
        res.json(results);
    }catch(error){
        console.log(error);
        res.sendStatus(500);
    }
});



//fetching a specific pip
router.post("/insert", urlencodedParser ,async (req, res) => {
    try{
        const priorities = req.body.pip.priorities;
        const pipComments = req.body.pip.pipComments;
        const startDate = req.body.pip.pipStartDate;
        const endDate = req.body.pip.pipEndDate
        const status = req.body.progress
        const contractId = req.body.contractId

        console.log(req.body)
        const results = await pip.insert( {priorities: priorities}, pipComments, startDate, endDate, contractId, status)
        res.json(results);
        // res.sendStatus(200)
    }catch(error){
        console.log(error);
        res.sendStatus(500);
    }
});



module.exports = router;