/*
|---------------------------------------|
|Software Engineer: Robin B. Mkuwira    |
|Date Created: 03/02/2021               |
|Project Name: HR Application           |
|                                       |
|The matrix route is used to access the |
|methods in the matrix model            |
|---------------------------------------|
*/

const jwt = require('jsonwebtoken');
const express = require("express");
const router = express.Router();
const matrix = require("../models/matrix");
const position = require("../models/position");
const employee = require("../models/employee");
const pip = require("../models/pip");
const bodyParser = require("body-parser");
const urlencodedParser = bodyParser.urlencoded({ extended: false});


//fetching all of the employees
router.get("/findAll", async (req, res)=>{
    try{
        let results = await matrix.findAll();
        res.json(results);
    }catch(error){
        console.log(error);
        res.sendStatus(500);
    }
 });


  // finding a mtrix by the position id
router.post("/findByPositionId", async (req, res)=>{
    try{
        const positionId = req.body.positionId;
        const results = await matrix.findByEmployeePosition(positionId)
        res.json(results);
    }catch(error){
        console.log(error);
        res.sendStatus(500);
    }
});




 // checking if the employee is a supervirsor
router.post("/findSupervisor", async (req, res)=>{
    try{
        const positionId = req.body.positionId;
        const employeePosition = await position.find(positionId)
        const positionName = employeePosition[0].position_name

        // getting all the matrices
        const results = await matrix.findAll()
        const size = results.length
        var isSupervirsor = false
        // check if this user is a supervisor
        for (var i = 0; i < size; i++) {
            var thisMatrix = results[i].matrix.matrix
            var matrixSize = thisMatrix.length
            // starting from the second position
            for (var k = 1; k < matrixSize; k++) {
                var thisPosition = thisMatrix[k].position
                if (positionName == thisPosition) {
                    isSupervirsor = true
                    break
                }
                // console.log(thisPosition)
            }

            if (isSupervirsor) {
                break
            }
            
        }

        var supervisorResults = {
            positionId: positionId,
            positionName: positionName,
            supervirsor: isSupervirsor
        }

        res.json(supervisorResults);
    }catch(error){
        console.log(error);
        res.sendStatus(500);
    }
});



// find the employee's supervised by this employee
router.post("/findSupervisedEmployees", async (req, res)=>{
    try{
        const positionId = req.body.positionId;
        const employeePosition = await position.find(positionId)
        const positionName = employeePosition[0].position_name

        // getting all the matrices
        const results = await matrix.findAll()
        const size = results.length
        var isSupervirsor = false
        var reportingMatrix = []

        // check if this user is a supervisor
        for (var i = 0; i < size; i++) {
            var thisMatrix = results[i].matrix.matrix
            var matrixSize = thisMatrix.length
            // starting from the second position
            for (var k = 1; k < matrixSize; k++) {
                var thisPosition = thisMatrix[k].position
                if (positionName == thisPosition ) {
                    reportingMatrix.push(thisMatrix)
                    isSupervirsor = true
                    break
                }
            }
            
        }


        var surbordinatePositions = []
        // adding the positions to the supervised positions matrix
        const reportingMatrixSize = reportingMatrix.length
        for (var j = 0; j < reportingMatrixSize; j++) {
            const thisReportingMatrix = reportingMatrix[j]
            const thisReportingMatrixSize = thisReportingMatrix.length

            //getting the positions
            for (var l = 0; l < thisReportingMatrixSize; l++) {
                var thisPosition = thisReportingMatrix[l].position
                if (positionName == thisPosition) {
                    break
                }else {
                    if (!surbordinatePositions.includes(thisPosition)) {
                        surbordinatePositions.push(thisPosition)
                    }
                }
            }

        }

        var employeeDetails = []

        // getting all the surbordinates
        const surbordinateSize = surbordinatePositions.length
        for (var m = 0; m < surbordinateSize; m++) {
            var thePosition = surbordinatePositions[m]
            var thisEmployees = await employee.findByEmployeePosition(thePosition)
            var employeeDataSize = thisEmployees.length

            for (var i = 0; i < employeeDataSize; i++) {
                var thisEmployee = thisEmployees[i]
                var contractId = thisEmployee.contract_id
                var thisPip = await pip.findByContractId(contractId)
                var pipSize = thisPip.length
                var thePip = null
        
                if (pipSize > 0) {
                    thePip = thisPip
                }
                
                thisEmployee.pip = thePip
                employeeDetails.push(thisEmployee)
            }
        }

        var supervisorResults = {
            positionId: positionId,
            positionName: positionName,
            supervirsor: isSupervirsor,
            subordinates: employeeDetails
        }

        res.json(supervisorResults);
    }catch(error){
        console.log(error);
        res.sendStatus(500);
    }
});


//fetching a specific contract
router.post("/find", urlencodedParser ,async (req, res) => {
    try{
        const id = req.body.id;
        const results = await matrix.find(id);
        res.json(results);
    }catch(error){
        console.log(error);
        res.sendStatus(500);
    }
});





// add the employee position id
router.get("/updatePositions", async(req, res) => {
    try{
        const positions = require('../models/position')
        const AllPositions = await positions.findAll()
        const size = AllPositions.length

        for (var i = 0; i < size; i++) {
            // updating the matrix with the ids
            const id = AllPositions[i].id
            const positionName = AllPositions[i].position_name
            await matrix.updateMatrixPositions(id, positionName)
        }

        // await 
        res.json(AllPositions)

    } catch(error) {
        res.sendStatus(200)
    }
})

// find if a particular employee's supervirsor
router.post("/findEmployeeSupervisor", async (req, res)=>{
    try{
        const positionId = req.body.positionId;
        const thisMatrix = await matrix.findByEmployeePosition(positionId)
        const theMatrix = thisMatrix.matrix.matrix
        const size = theMatrix.length
        var managerName = ""

        if (size > 1) {
            // get the second position thats the supervirsor
            managerName = theMatrix[1].position
        } else {
            // this is the country director
        }
        console.log(managerName)
        
        const manager = await employee.findByEmployeePosition(managerName)
        res.json(manager);
    }catch(error){
        console.log(error);
        res.sendStatus(500);
    }
 });


module.exports = router;