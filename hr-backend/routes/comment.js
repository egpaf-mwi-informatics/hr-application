/*
|---------------------------------------|
|Software Engineer: Robin B. MKuwira    |
|Date Created: 03/02/2021               |
|Project Name: HR Application           |
|                                       |
|The contract route is used to access   |
|the methods in the contract model      |
|---------------------------------------|
*/

const jwt = require('jsonwebtoken');
const express = require("express");
const router = express.Router();

const comment = require("../models/comment");

const bodyParser = require("body-parser");
const urlencodedParser = bodyParser.urlencoded({ extended: false});


//fetching all of the comments
router.get("/findAll", async (req, res)=>{
    try{
        let results = await comment.findAll();
        res.json(results);
    }catch(error){
        console.log(error);
        res.sendStatus(500);
    }
 });




//fetching a specific comment
router.post("/find", urlencodedParser ,async (req, res) => {
    try{
        const id = req.body.id;
        const results = await comment.find(id);
        res.json(results);
    }catch(error){
        console.log(error);
        res.sendStatus(500);
    }
});


//fetching a specific comment by the employee id
router.post("/findByEmploeeId", urlencodedParser ,async (req, res) => {
    try{
        const employeeId = req.body.employeeId;
        const results = await comment.findByEmployeeId(employeeId)
        res.json(results);
    }catch(error){
        console.log(error);
        res.sendStatus(500);
    }
});




// inserting a comment
router.post("/insert", urlencodedParser ,async (req, res) => {
    try{

        const employeeId = req.body.employeeId;
        const pipId = req.body.pipId
        const comment = req.body.comment
        const from = req.body.comment_from

        const results = await comment.insert(employeeId, pipId, comment, from);
        res.json(results);
    }catch(error){
        console.log(error);
        res.sendStatus(500);
    }
});




// updating a comment
router.post("/update", urlencodedParser ,async (req, res) => {
    try{
        const id = req.bosy.id        
        const employeeId = req.body.employeeId;
        const pipId = req.body.pipId
        const comment = req.body.comment
        const from = req.body.comment_from

        const results = await comment.update(id, employeeId, pipId, comment, from)
        res.json(results);
    }catch(error){
        console.log(error);
        res.sendStatus(500);
    }
});





module.exports = router;