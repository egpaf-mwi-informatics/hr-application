/*
|---------------------------------------|
|Software Engineer: Robin B. MKuwira    |
|Date Created: 03/02/2021               |
|Project Name: HR Application           |
|                                       |
|The contract route is used to access   |
|the methods in the contract model      |
|---------------------------------------|
*/

const jwt = require('jsonwebtoken');
const express = require("express");
const router = express.Router();

const contract = require("../models/contract");

const bodyParser = require("body-parser");
const urlencodedParser = bodyParser.urlencoded({ extended: false});


//fetching all of the employees
router.get("/findAll", async (req, res)=>{
    try{
        let results = await contract.findAll();
        res.json(results);
    }catch(error){
        console.log(error);
        res.sendStatus(500);
    }
 });




 //fetching all of the employees
router.get("/contractUpdate", async (req, res)=>{
    try{
        // update the expired contracts status
        let expiredContracts = await contract.findExpired(); 
        const expiredContractsSize = expiredContracts.length

        for (var i = 0; i < expiredContractsSize; i++) {
            var contractId = expiredContracts[i].contract_id
            // updating the contract to expired
            await contract.updateDescription(contractId, "Expired")
        }


        // update the active contracts status
        let activeContracts = await contract.findActive(); 
        const activeContractsSize = activeContracts.length
        for (var i = 0; i < activeContractsSize; i++) {
            var contractId = activeContracts[i].contract_id
            // updating the contract to expired
            await contract.updateDescription(contractId, "Active")
        }

        res.sendStatus(200)
    }catch(error){
        console.log(error);
        res.sendStatus(500);
    }
 });





//fetching a specific contract
router.post("/find", urlencodedParser ,async (req, res) => {
    try{
        const id = req.body.id;
        const results = await contract.find(id);
        res.json(results);
    }catch(error){
        console.log(error);
        res.sendStatus(500);
    }
});




//fetching a specific contract
router.post("/insert", urlencodedParser ,async (req, res) => {
    try{
        const status = req.body.status;
        const employeeId = req.body.employeeId;
        const startDate = req.body.startDate
        const endDate = req.body.endDate
        const sectionId = req.body.sectionId
        const positionId = req.body.positionId
        const officeId = req.body.officeId
        const contId = req.body.contId

        const results = await contract.insert(employeeId, startDate, endDate, status, sectionId, positionId, officeId, contId);
        res.json(results);
    }catch(error){
        console.log(error);
        res.sendStatus(500);
    }
});




//fetching a specific contract
router.post("/update", urlencodedParser ,async (req, res) => {
    try{
        const contractId = req.body.contractId;
        const employeeId = req.body.associateId;
        const startDate = req.body.startDate
        const endDate = req.body.endDate
        const status = req.body.status
        const departmentId = req.body.departmentId
        const positionId = req.body.positionId
        const officeId = req.body.officeId
        const contId = req.body.contId

        const results = await contract.update(contractId, employeeId, startDate, endDate, status, departmentId, positionId, officeId, contId)
        console.log(results)
        res.json(results);
    }catch(error){
        console.log(error);
        res.sendStatus(500);
    }
});





module.exports = router;