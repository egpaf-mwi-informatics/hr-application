/*
|---------------------------------------|
|Software Engineer: Robin B. MKuwira    |
|Date Created: 03/02/2021               |
|Project Name: HR Application           |
|                                       |
|The admin route is used to access the  |
|methods in the employee model          |
|---------------------------------------|
*/

const jwt = require('jsonwebtoken');
const express = require("express");
const router = express.Router();
const employee = require("../models/employee");
const dependant = require("../models/dependants")
const contractModel = require("../models/contract")
const office = require("../models/office")
const department = require("../models/department")
const email = require("../models/email")
const contractNotification = require("../models/contract_notification")
const birthdayNotification = require("../models/birthday_notification")
const anniversary = require("../models/anniversary")
const anniversaryType = require("../models/anniversary_type")
const birthdayMonth = require("../models/birthday_month")
const matrix = require("../models/matrix")
const bodyParser = require("body-parser");
const qs = require('qs');
const e = require('express');
const urlencodedParser = bodyParser.urlencoded({ extended: false});


//fetching all of the employee's data after a reload
router.post("/reload", async (req, res)=>{
    try{
        const associateId = req.body.associateId
        const [, token] = req.headers.authorization.split(" ")
        // console.log(associateId, token)
        try{
            await jwt.verify(token , process.env.JWT_SECRET_KEY)
            let results = await employee.findByIdFilter(associateId)
            // console.log(results)
            res.json(results[0]);
        } catch(error) {
            console.log(error)
            res.json([{
                message: error.message,
                name: error.name,
                expiredAt: error.expiredAt
            }]);
        }
    }catch(error){
        console.log(error);
        res.sendStatus(500);
    } 

});




// its used to send end of probation emails
// to: -HR -supervisor -employee 
router.post("/probationEmails", async (req, res)=>{
    try{
        // search for employees whose probation has expired and send them email
        const subject = 'Test email'
        const emailMessage = "This is a test email"
        const thisEmail = "robinmkuwira@hotmail.com"
        const result = email.sendEmail(subject, emailMessage, thisEmail)
        res.json(result)
    }catch(error){
        console.log(error);
        res.sendStatus(500);
    }
 });



// its used to send end of contract emails
// to -HR -supervisor & -employee
router.get("/endContractCheck", async (req, res)=>{
    try{
        const results = await employee.findContractToExpire()
        const size = results.length

        if (size > 0) {
            const monthToExpire = results[0].expiry_month

            var heading =  "Dear All, " +
                            "<br> <br>" +
                            `The contracts of the following employees will expire in ${monthToExpire} next month.<br>`; 

            var subject = 'Contract Expiry'
            var hrEmailContent =`<p>${heading}<\p>` + 
                                "<table sytle='font-family: Arial, Helvetica, sans-serif; width: 100%; border: 1px solid #ddd;' >" +
                                "<tr>" +
                                    "<th style='border: 1px solid #ddd; padding: 8px; border-collapse: collapse; '>No</th>" +
                                    "<th style='border: 1px solid #ddd; padding: 8px; border-collapse: collapse; '>Employee Name</th>" +
                                    "<th style='border: 1px solid #ddd; padding: 8px; border-collapse: collapse; '>Department</th>" +
                                    "<th style='border: 1px solid #ddd; padding: 8px; border-collapse: collapse; '>Duty Station</th>" +
                                    "<th style='border: 1px solid #ddd; padding: 8px; border-collapse: collapse; '>Email</th>" +
                                    "<th style='border: 1px solid #ddd; padding: 8px; border-collapse: collapse; '>Contract Expiry Date</th>" + 
                                "</tr>";
            
            var endContractCount = 0

            for (var i = 0; i < size; i++) {
                var result = results[i]
                var requestId = result.id

                // check if the request is already in the database
                var notified = await contractNotification.findByContractId(requestId)
                var notifiedSize = notified.length

                if (notifiedSize == 0) {
                    endContractCount +=1
                    var firstname = result.first_name
                    var surname = result.last_name
                    var fullname = firstname + " " + surname
                    var department = result.department_name
                    var dutyStaion = result.office_name
                    var expiryMonth = result.expiry_month
                    var exactExpiryMonth = result.exact_expiry_month
                    var positionId = result.position_id

                    var employeeEmail = result.email
                    var emailMessage = "Dear " + fullname + ",\n" +
                                    `Your contract is going to expire in ${expiryMonth} next month.`;

                    // send email to the employee
                    // await email.sendEmail(subject, emailMessage, employeeEmail)
                    console.log('sent ->', employeeEmail, emailMessage)


                    // send an email the employee's supervirsor

                    // finding the employee's supervirsor
                    const thisMatrix = await matrix.findByEmployeePosition(positionId)
                    
                    if (thisMatrix != null && thisMatrix != undefined) {
                        const theMatrix = thisMatrix.matrix.matrix
                        const size = theMatrix.length
                        
                        var managerialPosition = ""

                        if (size > 1) {
                            // get the second position thats the supervirsor
                            managerialPosition = theMatrix[1].position
                        } else {
                            // this is the country director
                        }

                        const employeeMatrix = await employee.findMatrix(employeeEmail);
                        if (matrix.length > 0) {
                            const manager = employeeMatrix[1]

                            if (manager.length != 0) {
                                const managerEmail = manager.email
                                const firstname = manager.firstname
                                const surname = manager.surname
                                var managerFullname = firstname + " " + surname

                                // creating the manager's email
                                var managerMessage = "Dear " + managerFullname + ",\n" +
                                                    `Please note that the contract of an employee that you supervise ${fullname} is going to expire in ${expiryMonth} next month.`;

                                // send email to the employee's manager
                                await email.sendEmail(subject, managerMessage, managerEmail)
                            }
                        }
                    }
                

                    // batch message for the the senior HR officers
                    hrEmailContent += "<tr>" +
                                        `<td style='border: 1px solid #ddd; padding: 8px; border-collapse: collapse; '>${(i+1)}</td>` +
                                        `<td style='border: 1px solid #ddd; padding: 8px; border-collapse: collapse; '>${fullname}</td>` +
                                        `<td style='border: 1px solid #ddd; padding: 8px; border-collapse: collapse; '>${department}</td>` +
                                        `<td style='border: 1px solid #ddd; padding: 8px; border-collapse: collapse; '>${dutyStaion}</td>` +
                                        `<td style='border: 1px solid #ddd; padding: 8px; border-collapse: collapse; '>${employeeEmail}</td>` +
                                        `<td style='border: 1px solid #ddd; padding: 8px; border-collapse: collapse; '>${exactExpiryMonth}</td>` +
                                    "</tr>";
                    
                    // add in the notified list
                    await contractNotification.insert(requestId)
                }

            }

            if (endContractCount > 0) {
                // sending an email to the senior HR officers
                const HRManagers = await employee.findHR()
                const managersSize = HRManagers.length
                const managerEmails = []

                // looping through the HR managers data
                for (var k = 0; k < managersSize; k++ ) {
                    var managerEmail = HRManagers[k].email
                    managerEmails.push(managerEmail)
                }

                await email.sendHTMLEmail(subject, hrEmailContent, managerEmails[0], managerEmails.slice(1))
            }
        }
        
        res.json(results)
    }catch(error){
        console.log(error);
        res.sendStatus(500);
    }
 });


 router.get("/birthdayCheck", async (req, res)=>{
    try{
        // search for employees who the current date is
        // their birthday
        const birthdayData = await employee.findTodayBirthdays()
        const size = birthdayData.length
        const subject = 'Happy Birthday !!!'

        for (var i = 0; i < size; i++) {
            var employeeId = birthdayData[i].associate_id
            var exists = await birthdayNotification.exists(employeeId)
            var existSize = exists.length

            if (existSize == 0) {
                var firstname = birthdayData[i].first_name 
                var surname = birthdayData[i].last_name
                var fullname = firstname + " " + surname
                var thisEmail = birthdayData[i].email

                const emailMessage = employee.birthdayMessageGenerator(fullname);

                // insert the birthday notification
                await birthdayNotification.insert(employeeId)

                // send the email
                await email.sendEmail(subject, emailMessage, thisEmail)
            }
        }

        res.json(birthdayData)
        // res.sendStatus(200)
    }catch(error){
        console.log(error);
        res.sendStatus(500);
    }
 });



// its used to search for employee details
router.get("/filter", async (req, res)=>{
    try{
        const startDate = req.query.startDate
        const endDate = req.query.endDate
        const onProbation = req.query.onProbation
        const onPip = req.query.onPip
        const startedBetween = req.query.newHire
        const terminatedBetween = req.query.terminatedContracts
        var results = []
        var status = ''

        console.log(req.query)

        if (onProbation == 'true') {
            // get employees who are on probation
            status = "Probation"
            var data = await employee.findByStatus(status)
            var dataSize = data.length
            
            // looping through the data to get the employees
            for (var i = 0; i < dataSize; i++) {
                var employeeId = data[i].employee_id
                var thisEmployee = await employee.findByIdFilter(employeeId)
                results.push(thisEmployee[0])
            }

        } else if (startDate != null && endDate != null) {
            var data = await employee.findByDates(startDate, endDate)
            var dataSize = data.length
            // looping through the data to get the employees
            for (var i = 0; i < dataSize; i++) {
                var employeeId = data[i].employee_id
                var thisEmployee = await employee.findByIdFilter(employeeId)
                results.push(thisEmployee[0])
            }
        } else if (onPip == 'true') {
            // get employees who are on probation
            var data = await employee.findByPip()
            var dataSize = data.length
            // looping through the data to get the employees
            for (var i = 0; i < dataSize; i++) {
                var employeeId = data[i].employee_id
                var thisEmployee = await employee.findByIdFilter(employeeId)
                results.push(thisEmployee[0])
            }

        } else if ( startedBetween == 'true'  && startDate != null && endDate != null) {
            // joined @ certain time
            var startedStatusId = 2
            var data = await employee.findByStartDateBetween(startDate, endDate, startedStatusId)
            var dataSize = data.length
            // looping through the data to get the employees
            for (var i = 0; i < dataSize; i++) {
                var employeeId = data[i].employee_id
                var thisEmployee = await employee.findByIdFilter(employeeId)
                results.push(thisEmployee[0])
            }
        } else if ( terminatedBetween == 'true'  && startDate != null && endDate != null) {
            // joined @ certain time
            var terminatedStatusId = 5
            var data = await employee.findByTerminatedDateBetween(startDate, endDate, terminatedStatusId)
            var dataSize = data.length
            // looping through the data to get the employees
            for (var i = 0; i < dataSize; i++) {
                var employeeId = data[i].employee_id
                var thisEmployee = await employee.findByIdFilter(employeeId)
                results.push(thisEmployee[0])
            }
        }


        res.json(results)
    }catch(error){
        console.log(error);
        res.sendStatus(500);
    }
 });




// its used to search for employee details
router.get("/search", async (req, res)=>{
    try{
        const searchText = req.query.searchText;
        let results = await employee.search(searchText);
        res.json(results);
    }catch(error){
        console.log(error);
        res.sendStatus(500);
    }
 });




//fetching the employee per department chart
router.get("/findEmployeeDepartmentChart", async (req, res)=>{
    try{
        let results = await employee.findEmployeeDepartmentChart();
        res.json(results);
    }catch(error){
        console.log(error);
        res.sendStatus(500);
    }
 });


//fetching the employees per district chart data
router.get("/findEmployeeDistrictChart", async (req, res)=>{
    try{
        let results = await employee.findEmployeeDistrictChart();
        res.json(results);
    }catch(error){
        console.log(error);
        res.sendStatus(500);
    }
 });



 //fetching the employees per district chart data
router.get("/findEmployeeDistrictStackedChart", async (req, res)=>{
    try{
        const offices = await office.findAll();
        const departments = await department.findAll();
        const officeSize = offices.length
        const departmentSize = departments.length
        var officeNames = []
        var results = []


        // getting the office names
        for (var j = 0; j < officeSize; j++) {
            // get the office name
            const officeName = offices[j].office_name
            officeNames.push(officeName)
        }



        for (var i = 0; i < departmentSize; i++) {
            // get the department's name
            const departmentName = departments[i].department_name
            var newCount = [] 
            for (var k = 0; k < officeSize; k++) {
                // get the office name
                const officeName = offices[k].office_name
                
                const result = await employee.findEmployeeDepartmentStackedChart(officeName, departmentName);
                var count = result.length
                newCount.push(count)
            }
            var data = {  name: departmentName,
                          data: newCount
                        }
            results.push(data)
        }
        var dataResults = {
            series: results,
            categories: officeNames
        }

        res.json(dataResults);
    }catch(error){
        console.log(error);
        res.sendStatus(500);
    }
 });



//fetching the employee department chart
router.get("/findGenderDepartmentChart", async (req, res)=>{
    try{
        let results = await employee.findGenderDepartmentChart();
        res.json(results);
    }catch(error){
        console.log(error);
        res.sendStatus(500);
    }
 });


 //fetching the employee department chart
router.get("/findGenderOrganizationalChart", async (req, res)=>{
    try{
        let results = await employee.findGenderOrganizationalChart();
        res.json(results);
    }catch(error){
        console.log(error);
        res.sendStatus(500);
    }
 });




//fetching the employee department chart
router.get("/findDepartmentExpiredContractChart", async (req, res)=>{
    try{
        let results = await employee.findDepartmentExpiredContractChart();
        res.json(results);
    }catch(error){
        console.log(error);
        res.sendStatus(500);
    }
 });



//fetching all of the employees
router.get("/findAll", async (req, res)=>{
    try{
        let results = await employee.findAll();
        res.json(results);
    }catch(error){
        console.log(error);
        res.sendStatus(500);
    }
});



//fetching all of the employees
router.get("/anniversaryCheck", async (req, res)=>{
    try{
        var results = []
        const data = await contractModel.findAnniversaryYears();
        const size = data.length

        var heading =  "Dear All, " +
                        "<br> <br>" +
                        `The table below contains the annivesary details of the following employees:<br>`; 

            var subject = 'Employee Anniversary'
            var hrEmailContent =`<p>${heading}<\p>` + 
                                "<table sytle='font-family: Arial, Helvetica, sans-serif; width: 100%; border: 1px solid #ddd;' >" +
                                "<tr>" +
                                    "<th style='border: 1px solid #ddd; padding: 8px; border-collapse: collapse; '>No</th>" +
                                    "<th style='border: 1px solid #ddd; padding: 8px; border-collapse: collapse; '>Employee</th>" +
                                    "<th style='border: 1px solid #ddd; padding: 8px; border-collapse: collapse; '>Email</th>" +
                                    "<th style='border: 1px solid #ddd; padding: 8px; border-collapse: collapse; '>Anniversary Years</th>" +
                                    "<th style='border: 1px solid #ddd; padding: 8px; border-collapse: collapse; '>Start Date</th>" +
                                    "<th style='border: 1px solid #ddd; padding: 8px; border-collapse: collapse; '>End Date</th>" + 
                                "</tr>";


        // loop through the employees
        for (let i = 0; i < size; i++) {
            const employeeId = data[i].employee_id
            const durationYears = data[i].duration
            
            // check if the anniverssay is already there
            const employeeAnniversary = await anniversary.exists(employeeId, durationYears)
            const existSize = employeeAnniversary.length


            if (existSize == 0) {
                // find the employee
                const employeDetails = await employee.find(employeeId)
                const firstname = employeDetails[0].first_name
                const surname = employeDetails[0].last_name
                const employeeEmail = employeDetails[0].email

                // adding the employee details to the results
                data[i].firstname = firstname
                data[i].surname = surname
                data[i].email = employeeEmail

                
                const employeeStartDate =data[i].first_contract_start_date
                const employeeContractEndDate = data[i].recent_contract_end_date
                const fullname = firstname + ' ' + surname

                // find the anniversary type
                const employeeAnniversaryType = await anniversaryType.findByYears(durationYears)
                const anniversaryTypeId = employeeAnniversaryType[0].id
                
                // insert the anniversary
                await anniversary.insert(employeeId, anniversaryTypeId)
                

                // batch message for the the senior HR officers
                hrEmailContent += "<tr>" +
                                    `<td style='border: 1px solid #ddd; padding: 8px; border-collapse: collapse; '>${(i+1)}</td>` +
                                    `<td style='border: 1px solid #ddd; padding: 8px; border-collapse: collapse; '>${fullname}</td>` +
                                    `<td style='border: 1px solid #ddd; padding: 8px; border-collapse: collapse; '>${employeeEmail}</td>` +
                                    `<td style='border: 1px solid #ddd; padding: 8px; border-collapse: collapse; '>${durationYears}</td>` +
                                    `<td style='border: 1px solid #ddd; padding: 8px; border-collapse: collapse; '>${employeeStartDate}</td>` +
                                    `<td style='border: 1px solid #ddd; padding: 8px; border-collapse: collapse; '>${employeeContractEndDate}</td>` +
                                "</tr>";


                // add the data into the results []
                results.push(data[i])
            }

        }

        hrEmailContent += "</table>" +
                          "<p>Please <b style='font-size: 14px;'>Do Not Respond</b> to This Email | Thank You.</p>"

        // get the HR officer's 
        const hrEmployees = await employee.findHR()
        const hrEmployeesSize = hrEmployees.length
        const hrEmails = []

        for (let k = 0; k < hrEmployeesSize; k++) {
            const hrEmail = hrEmployees[k].email

            // push the hr email
            hrEmails.push(hrEmail)
        }


        if (results.length > 0) {
            await email.sendHTMLEmail(subject, hrEmailContent, hrEmails[0], hrEmails.slice(1))
            // await email.sendHTMLEmail(subject, hrEmailContent, 'rmkuwira@pedaids.org', [])
        }

        res.json(results);
    }catch(error){
        console.log(error);
        res.sendStatus(500);
    }
});



//fetching all of the employees
router.get("/birthdayMonthCheck", async (req, res)=>{
    try{
        var results = []
        const data = await employee.findBirthdayMonth();
        const size = data.length

        var heading =  "Dear All, " +
                        "<br> <br>" +
                        `The table below contains the employee birthday details for this month:<br>`; 

            var subject = 'Employee Birthdays'
            var hrEmailContent =`<p>${heading}<\p>` + 
                                "<table sytle='font-family: Arial, Helvetica, sans-serif; width: 100%; border: 1px solid #ddd;' >" +
                                "<tr>" +
                                    "<th style='border: 1px solid #ddd; padding: 8px; border-collapse: collapse; '>No</th>" +
                                    "<th style='border: 1px solid #ddd; padding: 8px; border-collapse: collapse; '>Employee</th>" +
                                    "<th style='border: 1px solid #ddd; padding: 8px; border-collapse: collapse; '>Email</th>" +
                                    "<th style='border: 1px solid #ddd; padding: 8px; border-collapse: collapse; '>Date of Birth</th>" +
                                "</tr>";


        // loop through the employees
        for (let i = 0; i < size; i++) {
            const employeeId = data[i].associate_id
            const firstname = data[i].first_name
            const surname = data[i].last_name
            const employeeEmail = data[i].email
            const dateOfBirth = data[i].date_of_birth

            const fullname = firstname + ' ' + surname

                
            // insert the anniversary
            await birthdayMonth.insert(employeeId)
                

                // batch message for the the senior HR officers
                hrEmailContent += "<tr>" +
                                    `<td style='border: 1px solid #ddd; padding: 8px; border-collapse: collapse; '>${(i+1)}</td>` +
                                    `<td style='border: 1px solid #ddd; padding: 8px; border-collapse: collapse; '>${fullname}</td>` +
                                    `<td style='border: 1px solid #ddd; padding: 8px; border-collapse: collapse; '>${employeeEmail}</td>` +
                                    `<td style='border: 1px solid #ddd; padding: 8px; border-collapse: collapse; '>${dateOfBirth}</td>` +
                                "</tr>";


                // add the data into the results []
                results.push(data[i])

        }

        hrEmailContent += "</table>" +
                          "<p>Please <b style='font-size: 14px;'>Do Not Respond</b> to This Email | Thank You.</p>"

        
        // get the HR officer's 
        const hrEmployees = await employee.findHR()
        const hrEmployeesSize = hrEmployees.length
        const hrEmails = []

        for (let k = 0; k < hrEmployeesSize; k++) {
            const hrEmail = hrEmployees[k].email

            // push the hr email
            hrEmails.push(hrEmail)
        }

        if (results.length > 0) {
            await email.sendHTMLEmail(subject, hrEmailContent, hrEmails[0], hrEmails.slice(1))
        }
        res.json(results);
    }catch(error){
        console.log(error);
        res.sendStatus(500);
    }
 });



//fetching all of the new employees
router.get("/findNew", async (req, res)=>{
    try{
        let results = await employee.findNew();
        console.log(results)
        res.json(results);
    }catch(error){
        console.log(error);
        res.sendStatus(500);
    }
 });


//fetching a specific employee
router.post("/find", urlencodedParser ,async (req, res) => {
    try{
        const associateId = req.body.associateId;
        const results = await employee.find(associateId);
        res.json(results);
    }catch(error){
        console.log(error);
        res.sendStatus(500);
    }
});



//fetching a specific employee
router.post("/findByEmail", urlencodedParser ,async (req, res) => {
    try{
        const email = req.body.email;
        const results = await employee.findByEmail(email);
        res.json(results);
    }catch(error){
        console.log(error);
        res.sendStatus(500);
    }
});



//login a specific employee using ldap or caching mechanisms
router.post("/login", urlencodedParser, async (req, res)=>{
    try{         
        var loginStatus = ''
        const email = req.body.email;
        const password = req.body.password;
        var results = {}
        const axios = require('axios');
        // const ldapURL = "http://127.0.0.1:5000/"
        const ldapURL = "http://hr-ldap:5000/"
        const postData = { 
                            'email': email, 
                            'password': password 
                        }

        axios.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded';
        await axios.post(ldapURL, qs.stringify(postData))
            .then( async(response) => {
                loginStatus = response.data
                if (loginStatus == 'True') {
                    const employeeData = await employee.findByEmail(email)
                    const associateId = employeeData[0].associate_id
                    const firstname = employeeData[0].first_name
                    const lastname = employeeData[0].last_name
                    const position = employeeData[0].position_id
                    const positionName = employeeData[0].position_name
                    const contractId = employeeData[0].contract_id
                    

                    // adding an updated user password
                    var token = jwt.sign({ 
                        exp: Math.floor(Date.now()/1000) + (60 * 60 * 12),
                        user: 'data' 
                    }, 
                    process.env.JWT_SECRET_KEY );
                    // update the employee password
                    // the response data
                    results = {
                        data: {
                                associateId: associateId,
                                firstname: firstname,
                                lastname: lastname,
                                email: email,
                                positionId: position,
                                positionName: positionName,
                                contractId: contractId
                            },
                        token: token,
                        message: 'Success, the submitted credentials are valid.',
                        status: 200
                    }
                    await employee.updatePassword(password, associateId)
                    // 
                } else {
                    results = {
                        data: {},
                        token: '',
                        message: 'Sorry, the submitted credentials are wrong.',
                        status: 401
                    }
                }
            })
            .catch( async(error) => {
                // ldap is down login using local db
                console.log(error)
                if ( error.response == undefined || error.response.status == 404) {
                    results = await employee.login(email, password)
                    // check if the results are empty
                    if (results.length > 0) {
                        // finding the employee's details
                        const employeeData = await employee.findByEmail(email)
                        const associateId = employeeData[0].associate_id
                        const firstname = employeeData[0].first_name
                        const lastname = employeeData[0].last_name
                        const position = employeeData[0].position_id
                        const positionName = employeeData[0].position_name
                        const contractId = employeeData[0].contract_id

                        // jwt token sign in
                        var token = jwt.sign({
                            exp: Math.floor(Date.now()/1000) + (60 * 60 * 12),
                            user: 'data'
                        },
                        process.env.JWT_SECRET_KEY);


                        results = {
                            data: {
                                    associateId: associateId,
                                    firstname: firstname,
                                    lastname: lastname,
                                    email: email,
                                    positionId: position,
                                    positionName: positionName,
                                    contractId: contractId
                                },
                            token: token,
                            message: 'Success, the submitted credentials are valid.',
                            status: 200
                        }
   
                    } else {
                        results = {
                            data: {},
                            token: '',
                            message: 'This user does not exists in the system..',
                            status: 401
                        }
                    }
                }
            })
        // console.log(results)
        res.json(results)
    }catch(error){
        console.log(error)
        results = {
            data: {},
            token: '',
            message: error,
            status: 500
        }
        res.json(results);
    }
});


router.post("/update", urlencodedParser, async (req, res) => {
    try{
        console.log(req.body)
        const associateId = req.body.associateId;
        const firstname = req.body.firstName;
        const lastname = req.body.lastName;
        const email = req.body.email;
        const dateOfBirth = req.body.dateOfBirth
        const sex = req.body.sex;
        const masmId = req.body.masmId
        const empPassword = null

        // inserting the employee into the database
        const results = await employee.update(associateId, email, dateOfBirth, sex, empPassword, firstname, lastname, masmId)

        res.json(results)
        // res.sendStatus(200)
    }catch(error){
        console.log(error)
        res.json(error);
    }

});












// inserting the user's details
router.post("/insert", urlencodedParser, async (req, res) => {
    try{
        // console.log(req.body)
        const associateId = req.body.associateId;
        const firstname = req.body.firstName;
        const lastname = req.body.lastName;
        const email = req.body.email;
        const dateOfBirth = req.body.dateOfBirth
        const sex = req.body.sex;
        const masmId = req.body.masmId
        const empPassword = null

        // inserting the employee into the database
        const results = await employee.insert(associateId, email, dateOfBirth, sex, empPassword, firstname, lastname, masmId)

        const dependants = req.body.dependants

        // chack if there are no dependants 
        if (dependants != undefined && dependants != null ) {
            // looping through the dependents list
            const size = dependants.length
            for (var i = 0; i < size; i++) {
                var dependent = dependants[i]
                // now insert it into the database
                var nationalId = dependent.nationalId
                var dependentFirstname = dependent.firstName
                var dependentLastname = dependent.lastName
                var dependentDateOfBirth = dependent.dateOfBirth
                await dependant.insert(nationalId, dependentFirstname, dependentLastname, dependentDateOfBirth, associateId)
            }
        }

        // getting the contract
        const contract = req.body.contract
        const startDate = contract.startDate
        const endDate = contract.endDate
        const status = contract.status
        const positionId = contract.positionId
        const departmentId = contract.departmentId
        const officeId = contract.officeId
        
        // inserting the contract
        await contractModel.insert(associateId, startDate, endDate, status, departmentId, positionId, officeId)
        console.log(results)
        res.json(results)
        // res.sendStatus(200)
    }catch(error){
        console.log(error)
        res.json(error);
    }

});






// inserting the user's details
router.post("/fileImport", urlencodedParser, async (req, res) => {
    try{
        const data = req.body.data
        var dataSize = data.length
        // console.log(req.body)
        // loop through the uploaded data
        for (var i = 1; i < dataSize; i++) {
            // getting the data
            var rowData = data[i]

            // getting the required fields
            // employee details
            var associateId = rowData[0]
            var firstname = rowData[1]
            var lastname = rowData[2]
            var email = rowData[4]
            var section = rowData[6]
            var sex = rowData[9]
            var masmId = req.body.masmId
            var dateOfBirth = rowData[16]
            var empPassword = null

            // position details
            var positionName = rowData[3]
            // project details
            var project = rowData[7]

            // department details
            var departmentName = rowData[5]

            // office details
            var officeName = rowData[15]

            // contract details
            var startDate = rowData[13]
            var endDate = rowData[14]

            // check if th e employee is already available in the system
            var employeeCheck = await employee.find(associateId)
            var employeeCheckSize = employeeCheck.length

            // adding an employee that does not exist
            if (employeeCheckSize == 0) {
                console.log('--->', firstname, lastname)
                var date = new Date(dateOfBirth)
                var dateText = date.toISOString(date)
                console.log('--->', dateText)
                // if the employee is not available add his details
                // await employee.insert(associateId, email, dateOfBirth, sex, empPassword, firstname, lastname, masmId)                
            }

            // find the office id by the office name
            var officeResults = await office.findByName(officeName)
            var officeId = officeResults[0].office_id

            // getting the department
            var departmentResults = await department.findByName(departmentName)
            var departmentId = departmentResults[0].department_id;

            // inserting the contract
            // await contractModel.insert(associateId, startDate, endDate, status, departmentId, positionId, officeId)

        }

        res.sendStatus(200)
    }catch(error){
        console.log(error)
        res.json(error);
    }

});








module.exports = router;