/*
|----------------------------------------|
|Software Engineer: Robin B. MKuwira     |
|Date Created: 03/02/2021                |
|Project Name: HR Application            |
|                                        |
|The dependant route is used to access   |
|the methods in the dependant model      |
|----------------------------------------|
*/

const jwt = require('jsonwebtoken');
const express = require("express");
const router = express.Router();
const dependant = require("../models/dependants");
const dependantRecord = require("../models/dependants_records");
const email = require("../models/email");
const dependantNotification = require('../models/dependant_notification');
const employee = require("../models/employee")
const bodyParser = require("body-parser");
const urlencodedParser = bodyParser.urlencoded({ extended: false});


//fetching all of the dependants
router.get("/findAll", async (req, res)=>{
    try{
        let results = await dependant.findAll();
        res.json(results);
    }catch(error){
        console.log(error);
        res.sendStatus(500);
    }
 });



// its used to send end of contract emails
// to -HR -supervisor & employee
router.post("/dependentsCheck", async (req, res)=>{
    try{
        const results = await dependant.findToExpireNextMonth()
        const size = results.length
        var monthToExpire = ''
        if (size > 0) {
            monthToExpire = results[0].expiry_month
        }
        
        var heading =  "Dear All, " +
                          "<br> <br>" +
                           `The ages of the following dependants will exceed 21 in ${monthToExpire}.` +
                            "<br>" +
                            "Please remove them from the MASM medical cover.<br><br><br>"; 

        var subject = 'Dependants MASM Support'
        var hrEmailContent =`<p>${heading}<\p>` + 
                            "<table sytle='font-family: Arial, Helvetica, sans-serif; width: 100%; border: 1px solid #ddd;' >" +
                              "<tr>" +
                                 "<th style='border: 1px solid #ddd; padding: 8px; border-collapse: collapse; '>No</th>" +
                                 "<th style='border: 1px solid #ddd; padding: 8px; border-collapse: collapse; '>Employee Name</th>" +
                                 "<th style='border: 1px solid #ddd; padding: 8px; border-collapse: collapse; '>Dependant Name</th>" +
                                 "<th style='border: 1px solid #ddd; padding: 8px; border-collapse: collapse; '>Department</th>" +
                                 "<th style='border: 1px solid #ddd; padding: 8px; border-collapse: collapse; '>Duty Station</th>" +
                                 "<th style='border: 1px solid #ddd; padding: 8px; border-collapse: collapse; '>Email</th>" +
                              "</tr>";
        
        var endContractCount = 0
    
        for (var i = 0; i < size; i++) {
            var result = results[i]
            var dependantId = result.id

            // check if the request is already in the database
            var notified = await dependantNotification.exists(dependantId)
            var notifiedSize = notified.length

            if (notifiedSize == 0) {
                endContractCount +=1
                var firstname = result.first_name
                var surname = result.last_name
                var fullname = firstname + " " + surname
                var department = result.department_name
                var dutyStation = result.office_name
                var expiryMonth = result.expiry_month
                var employeeFirstname = result.employee_firstname
                var employeeSurname = result.employee_surname
                var employeeName = employeeFirstname + " " + employeeSurname

                var employeeEmail = result.email
                var emailMessage = "Dear " + employeeName + ",\n\n" +
                                `Your dependant ${fullname}'s MASM medical cover will be terminated in ${expiryMonth} \nbecause the dependant's age will exceed 21 next month.`;
                                

                // send email to the employee
                await email.sendEmail(subject, emailMessage, employeeEmail)
                
                
                // batch message for the the senior HR officers
                hrEmailContent += "<tr>" +
                                    `<td style='border: 1px solid #ddd; padding: 8px; border-collapse: collapse; '>${(i+1)}</td>` +
                                    `<td style='border: 1px solid #ddd; padding: 8px; border-collapse: collapse; '>${employeeName}</td>` +
                                    `<td style='border: 1px solid #ddd; padding: 8px; border-collapse: collapse; '>${fullname}</td>` +
                                    `<td style='border: 1px solid #ddd; padding: 8px; border-collapse: collapse; '>${department}</td>` +
                                    `<td style='border: 1px solid #ddd; padding: 8px; border-collapse: collapse; '>${dutyStation}</td>` +
                                    `<td style='border: 1px solid #ddd; padding: 8px; border-collapse: collapse; '>${employeeEmail}</td>` +
                                "</tr>";
                
                // add in the notified list
                await dependantNotification.insert(dependantId)
            }

        }

        if (endContractCount > 0) {
            // sending an email to the senior HR officers
            const HRManagers = await employee.findHR()
            const managersSize = HRManagers.length
            const managerEmails = []

            // looping through the HR managers data
            for (var k = 0; k < managersSize; k++ ) {
                var managerEmail = HRManagers[k].email
                managerEmails.push(managerEmail)
            }

            await email.sendHTMLEmail(subject, hrEmailContent, managerEmails[0], managerEmails.slice(1))
  
        }
        res.sendStatus(200)
    }catch(error){
        console.log(error);
        res.sendStatus(500);
    }
 });






//fetching all of the dependants
router.post("/removeNonSupported", async (req, res)=>{
    try{
        let results = await dependant.findNonSupported();
        var size = results.length
        // move to the dependants records table
        for (var i = 0; i < size; i++) {
            var id = results[i].id
            var nationalId = results[i].national_id_no
            var dateOfBirth = results[i].date_of_birth
            var employeeId = results[i].employee_id
            var relationType = results[i].relation_type
            var firstname = results[i].first_name
            var surname = results[i].last_name
            var age = results[i].age

            // insert it into the dependants records table
            await dependantRecord.insert(nationalId, firstname, surname, dateOfBirth, employeeId, id, relationType, age)

            // delete those records in the dependants table
            await dependant.deleteDependant(id)
        }


        res.sendStatus(200)
    }catch(error){
        console.log(error);
        res.sendStatus(500);
    }
 });


//fetching a specific dependant
router.post("/find", urlencodedParser ,async (req, res) => {
    try{
        const id = req.body.id;
        const results = await dependant.find(id);
        res.json(results);
    }catch(error){
        console.log(error);
        res.sendStatus(500);
    }
});



//fetching a specific dependant
router.post("/insert", urlencodedParser ,async (req, res) => {
    try{
        console.log(req.body)
        const employeeId = req.body.associateId;
        const dependants = req.body.dependants;
        var allResults = []

        if (dependants != undefined && dependants != null) {
            const size = dependants.length
            for (var i = 0; i < size; i++) {
                const nationalId = dependants[i].nationalId;
                const firstname = dependants[i].firstName;
                const lastname = dependants[i].lastName;
                const dateOfBirth = dependants[i].dateOfBirth;
                var results = await dependant. insert(nationalId, firstname, lastname, dateOfBirth, employeeId);
                allResults.push(results)
            }
        }
        res.json(allResults);
    }catch(error){
        console.log(error);
        res.sendStatus(500);
    }
});



module.exports = router;