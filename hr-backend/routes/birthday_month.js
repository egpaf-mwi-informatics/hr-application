/*
|---------------------------------------|
|Software Engineer: Robin B. Mkuwira    |
|Date Created: 26/09/2023               |
|Project Name: HR Application           |
|                                       |
|The birthday month route is used to    |
|access the methods in the department   |
|model.                                 |
|---------------------------------------|
*/

const express = require("express");
const router = express.Router();
const birthdayMonth = require("../models/birthday_month");
const bodyParser = require("body-parser");
const urlencodedParser = bodyParser.urlencoded({ extended: false});




//fetching all of the birthday months 
router.get("/findAll", async (req, res)=>{
    try{
        let results = await birthdayMonth.findAll();
        res.json(results);
    }catch(error){
        console.log(error);
        res.sendStatus(500);
    }
 });


//fetching a specific birthday month
router.post("/find", urlencodedParser ,async (req, res) => {
    try{
        const id = req.body.id;
        const results = await birthdayMonth.find(id);
        res.json(results);
    }catch(error){
        console.log(error);
        res.sendStatus(500);
    }
});


// delete a specific birthday month
router.post("/insert", urlencodedParser ,async (req, res) => {
    try{
        const employeeId = req.body.employeeId;
        
        const results = await birthdayMonth.insert(employeeId);
        res.json(results);
    }catch(error){
        console.log(error);
        res.sendStatus(500);
    }
});


// update a specific birthday month
router.put("/update", urlencodedParser ,async (req, res) => {
    try{
        const id = req.body.id
        const employeeId = req.body.employeeId;

        const results = await birthdayMonth.update(id, employeeId)
        res.json(results);
    }catch(error){
        console.log(error);
        res.sendStatus(500);
    }
});


// delete a specific birthday month
router.delete("/delete", urlencodedParser ,async (req, res) => {
    try{
        const id = req.body.id;
        const results = await birthdayMonth.deleteBirthdayMonth(id);
        
        res.json(results);
    }catch(error){
        console.log(error);
        res.sendStatus(500);
    }
});

module.exports = router;