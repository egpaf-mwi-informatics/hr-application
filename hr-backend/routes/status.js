/*
|---------------------------------------|
|Software Engineer: Robin B. Mkuwira    |
|Date Created: 01/11/2021               |
|Project Name: HR Application           |
|                                       |
|The status route is used to access     |
|the methods in the project model       |
|---------------------------------------|
*/

const jwt = require('jsonwebtoken');
const express = require("express");
const router = express.Router();

const status = require("../models/status");

const bodyParser = require("body-parser");
const urlencodedParser = bodyParser.urlencoded({ extended: false});


//fetching all of the statuses
router.get("/findAll", async (req, res)=>{
    try{
        let results = await status.findAll();
        res.json(results);
    }catch(error){
        console.log(error);
        res.sendStatus(500);
    }
 });


//fetching a specific status
router.post("/find", urlencodedParser ,async (req, res) => {
    try{
        const id = req.body.id;
        const results = await status.find(id);
        res.json(results);
    }catch(error){
        console.log(error);
        res.sendStatus(500);
    }
});



// inserting a specific project
router.post("/insert", urlencodedParser ,async (req, res) => {
    try{
        const name = req.body.name;

        const results = await status.insert(name);
        res.json(results);
    }catch(error){
        console.log(error);
        res.sendStatus(500);
    }
});






// updating a specific status
router.post("/update", urlencodedParser ,async (req, res) => {
    try{
        const id = req.body.id;
        const name = req.body.name;

        const results = await status.update(id, name);
        res.json(results);
    }catch(error){
        console.log(error);
        res.sendStatus(500);
    }
});



module.exports = router;