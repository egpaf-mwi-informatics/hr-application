/*
|---------------------------------------|
|Software Engineer: Robin B. MKuwira    |
|Date Created: 03/02/2021               |
|Project Name: HR Application           |
|                                       |
|The relation type route is that is used|
|to access the methods in the contract  |
|notifications model                    |
|---------------------------------------|
*/

const jwt = require('jsonwebtoken');
const express = require("express");
const router = express.Router();
const dependantRelation= require("../models/dependant_relation");
const bodyParser = require("body-parser");
const urlencodedParser = bodyParser.urlencoded({ extended: false});


//fetching all of the relation types
router.get("/findAll", async (req, res)=>{
    try{
        let results = await dependantRelation.findAll();
        res.json(results);
    }catch(error){
        console.log(error);
        res.sendStatus(500);
    }
 });



// its used to search for employee details
router.get("/search", async (req, res)=>{
    try{
        const searchText = req.query.searchText;
        let results = await dependantRelation.search(searchText);
        res.json(results);
    }catch(error){
        console.log(error);
        res.sendStatus(500);
    }
 });



//fetching a specific relation type
router.post("/find", urlencodedParser ,async (req, res) => {
    try{
        const id = req.body.id;
        const results = await dependantRelation.find(id);
        res.json(results);
    }catch(error){
        console.log(error);
        res.sendStatus(500);
    }
});





module.exports = router;