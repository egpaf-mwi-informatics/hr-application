/*
|---------------------------------------|
|Software Engineer: Robin B. MKuwira    |
|Date Created: 03/02/2021               |
|Project Name: HR Application           |
|                                       |
|The contract project route is          |
|used to access the methods in the      |
|contract project model                 |
|---------------------------------------|
*/

const jwt = require('jsonwebtoken');
const express = require("express");
const router = express.Router();
const contractProject = require("../models/contract_project");
const bodyParser = require("body-parser");
const urlencodedParser = bodyParser.urlencoded({ extended: false});


//fetching all of the contract projects
router.get("/findAll", async (req, res)=>{
    try{
        let results = await contractProject.findAll();
        res.json(results);
    }catch(error){
        console.log(error);
        res.sendStatus(500);
    }
 });


//fetching a specific contract project
router.post("/find", urlencodedParser ,async (req, res) => {
    try{
        const id = req.body.id;
        const results = await contractProjectfind(id);
        res.json(results);
    }catch(error){
        console.log(error);
        res.sendStatus(500);
    }
});





module.exports = router;