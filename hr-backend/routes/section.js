/*
|---------------------------------------|
|Software Engineer: Robin B. Mkuwira    |
|Date Created: 03/02/2021               |
|Project Name: HR Application           |
|                                       |
|The section route is used to access    |
|the methods in the section model       |
|---------------------------------------|
*/

const jwt = require('jsonwebtoken');
const express = require("express");
const router = express.Router();

const section = require("../models/section");

const bodyParser = require("body-parser");
const urlencodedParser = bodyParser.urlencoded({ extended: false});


//fetching all of the sections
router.get("/findAll", async (req, res)=>{
    try{
        let results = await section.findAll();
        res.json(results);
    }catch(error){
        console.log(error);
        res.sendStatus(500);
    }
 });


//fetching a specific section
router.post("/find", urlencodedParser ,async (req, res) => {
    try{
        const id = req.body.id;
        const results = await section.find(id);
        res.json(results);
    }catch(error){
        console.log(error);
        res.sendStatus(500);
    }
});



// inserting a specific section
router.post("/insert", urlencodedParser ,async (req, res) => {
    try{
        const name = req.body.name;
        const departmentId = req.body.departmentId

        const results = await section(name, departmentId) ;
        res.json(results);
    }catch(error){
        console.log(error);
        res.sendStatus(500);
    }
});






// updating a specific section
router.post("/update", urlencodedParser ,async (req, res) => {
    try{
        const id = req.body.id;
        const name = req.body.name;
        const departmentId = req.body.departmentId

        const results = await section.update(name, departmentId, id);
        res.json(results);
    }catch(error){
        console.log(error);
        res.sendStatus(500);
    }
});



module.exports = router;