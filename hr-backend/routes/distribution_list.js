/*
|----------------------------------------|
|Software Engineer: Robin B. MKuwira     |
|Date Created: 03/02/2021                |
|Project Name: HR Application            |
|                                        |
|The distribution list route is used to  |
|access the methods in the employee model|
|----------------------------------------|
*/

const jwt = require('jsonwebtoken');
const express = require("express");
const router = express.Router();

const distributionList = require("../models/distribution_list");

const bodyParser = require("body-parser");
const urlencodedParser = bodyParser.urlencoded({ extended: false});



// its used to search for employee details
router.get("/search", async (req, res)=>{
    try{
        const searchText = req.query.searchText;
        let results = await distributionList.search(searchText);
        res.json(results);
    }catch(error){
        console.log(error);
        res.sendStatus(500);
    }
 });

//fetching all of the distribution list
router.get("/findAll", async (req, res)=>{
    try{
        let results = await distributionList.findAll();
        res.json(results);
    }catch(error){
        console.log(error);
        res.sendStatus(500);
    }
 });


//fetching a specific distribution list
router.post("/find", urlencodedParser ,async (req, res) => {
    try{
        const id = req.body.id;
        const results = await distributionList.find(id);
        res.json(results);
    }catch(error){
        console.log(error);
        res.sendStatus(500);
    }
});



//fetching a specific distribution list
router.post("/insert", urlencodedParser ,async (req, res) => {
    try{
        console.log(req.body)
        const groupId = req.body.grouupId;
        const groupName = req.body.groupName
        const groupEmail = req.body.groupEmail
        const groupDescription = req.body.groupDescription
        const results = await distributionList.insert(groupId, groupName, groupEmail, groupDescription)
        res.json(results);
    }catch(error){
        console.log(error);
        res.sendStatus(500);
    }
});






//fetching a specific distribution list
router.post("/update", urlencodedParser ,async (req, res) => {
    try{
        console.log(req.body)
        const groupId = req.body.grouupId;
        const groupName = req.body.groupName
        const groupEmail = req.body.groupEmail
        const groupDescription = req.body.groupDescription
        
        const results = await distributionList.update(groupId, groupName, groupEmail, groupDescription)
        res.json(results);
    }catch(error){
        console.log(error);
        res.sendStatus(500);
    }
});


module.exports = router;