/*
|---------------------------------------|
|Software Engineer: Robin B. Mkuwira    |
|Date Created: 03/02/2021               |
|Project Name: HR Application           |
|                                       |
|The department route is used to access |
|the methods in the department model    |
|---------------------------------------|
*/

const jwt = require('jsonwebtoken');
const express = require("express");
const router = express.Router();

const department = require("../models/department");

const bodyParser = require("body-parser");
const urlencodedParser = bodyParser.urlencoded({ extended: false});



// its used to search for employee details
router.get("/search", async (req, res)=>{
    try{
        const searchText = req.query.searchText;
        let results = await department.search(searchText);
        res.json(results);
    }catch(error){
        console.log(error);
        res.sendStatus(500);
    }
 });

//fetching all of the departments
router.get("/findAll", async (req, res)=>{
    try{
        let results = await department.findAll();
        res.json(results);
    }catch(error){
        console.log(error);
        res.sendStatus(500);
    }
 });


//fetching a specific admin
router.post("/find", urlencodedParser ,async (req, res) => {
    try{
        const id = req.body.id;
        const results = await department.find(id);
        res.json(results);
    }catch(error){
        console.log(error);
        res.sendStatus(500);
    }
});



module.exports = router;