/*
|-----------------------------------------|
|Software Engineer: Robin B. MKuwira      |
|Date Created: 03/02/2021                 |
|Project Name: HR Application             |
|                                         |
|The dependant record route is used to    |
|access the methods in the dependant model|
|-----------------------------------------|
*/

const jwt = require('jsonwebtoken');
const express = require("express");
const router = express.Router();

const dependant = require("../models/dependants_records");

const bodyParser = require("body-parser");
const urlencodedParser = bodyParser.urlencoded({ extended: false});


//fetching all of the dependants
router.get("/findAll", async (req, res)=>{
    try{
        let results = await dependant.findAll();
        res.json(results);
    }catch(error){
        console.log(error);
        res.sendStatus(500);
    }
 });


//fetching a specific dependant
router.post("/find", urlencodedParser ,async (req, res) => {
    try{
        const id = req.body.id;
        const results = await dependant.find(id);
        res.json(results);
    }catch(error){
        console.log(error);
        res.sendStatus(500);
    }
});



//fetching a specific dependant
router.post("/insert", urlencodedParser ,async (req, res) => {
    try{
        console.log(req.body)
        const employeeId = req.body.associateId;
        const dependants = req.body.dependants;
        var allResults = []

        if (dependants != undefined && dependants != null) {
            const size = dependants.length
            for (var i = 0; i < size; i++) {
                const nationalId = dependants[i].nationalId;
                const firstname = dependants[i].firstName;
                const lastname = dependants[i].lastName;
                const dateOfBirth = dependants[i].dateOfBirth;
                var results = await dependant. insert(nationalId, firstname, lastname, dateOfBirth, employeeId);
                allResults.push(results)
            }
        }
        res.json(allResults);
    }catch(error){
        console.log(error);
        res.sendStatus(500);
    }
});



module.exports = router;