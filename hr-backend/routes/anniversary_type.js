/*
|---------------------------------------|
|Software Engineer: Robin B. Mkuwira    |
|Date Created: 26/09/2023               |
|Project Name: HR Application           |
|                                       |
|The anniversary type route is used to  |
|access the methods in the department   |
|model                                  |
|---------------------------------------|
*/

const express = require("express");
const router = express.Router();
const anniversaryType = require("../models/anniversary_type");
const bodyParser = require("body-parser");
const urlencodedParser = bodyParser.urlencoded({ extended: false});




//fetching all of the anniversary types
router.get("/findAll", async (req, res)=>{
    try{
        let results = await anniversaryType.findAll();
        res.json(results);
    }catch(error){
        console.log(error);
        res.sendStatus(500);
    }
 });


//fetching a specific anniversary type
router.post("/find", urlencodedParser ,async (req, res) => {
    try{
        const id = req.body.id;
        const results = await anniversaryType.find(id);
        res.json(results);
    }catch(error){
        console.log(error);
        res.sendStatus(500);
    }
});


// delete a specific anniversary type
router.post("/insert", urlencodedParser ,async (req, res) => {
    try{
        const years = req.body.years;
        const results = await anniversaryType.insert(years);
        res.json(results);
    }catch(error){
        console.log(error);
        res.sendStatus(500);
    }
});


// update a specific anniversary type
router.put("/update", urlencodedParser ,async (req, res) => {
    try{
        const id = req.body.id
        const years = req.body.years;

        const results = await anniversaryType.update(id, years)
        res.json(results);
    }catch(error){
        console.log(error);
        res.sendStatus(500);
    }
});


// delete a specific anniversary type
router.delete("/delete", urlencodedParser ,async (req, res) => {
    try{
        const id = req.body.id;
        const results = await anniversaryType.deleteAnniversaryType(id);
        res.json(results);
    }catch(error){
        console.log(error);
        res.sendStatus(500);
    }
});

module.exports = router;