/*
|-------------------------------------------|
|Software Engineer: Robin B. MKuwira        |
|Date Created: 03/02/2021                   |
|Project Name: HR Application               |
|                                           |
|The admin route is used to access the      |
|methods in the employee distribution model |
|-------------------------------------------|
*/

const jwt = require('jsonwebtoken');
const express = require("express");
const router = express.Router();

const employeeDistribution = require("../models/employee_distribution");
const distributionList = require("../models/distribution_list")
const employee = require("../models/employee");
const matrix = require("../models/matrix");
const position = require("../models/position");
var emailModule = require("../models/email");
const bodyParser = require("body-parser");
const urlencodedParser = bodyParser.urlencoded({ extended: false});


//fetching all of the employee destributions
router.get("/findAll", async (req, res)=>{
    try{
        let results = await employeeDistribution.findAll();
        res.json(results);
    }catch(error){
        console.log(error);
        res.sendStatus(500);
    }
 });


//fetching a specific employee distribution
router.post("/find", urlencodedParser ,async (req, res) => {
    try{
        const id = req.body.id;
        const results = await employeeDistribution.find(id);
        res.json(results);
    }catch(error){
        console.log(error);
        res.sendStatus(500);
    }
});




//fetching a specific distribution list
router.post("/insert", urlencodedParser ,async (req, res) => {
    try{
        // getting the json-as-xlsx package
        let xlsx = require("json-as-xlsx")

        // getting the employee distribution array
        const employeeDistributionData = req.body
        const size = employeeDistributionData.length
        var HQEmailBatchData = []
        // console.log(req.body)

        var results = []
        for (var i = 0; i < size; i++) {
            var associateId = employeeDistributionData[i].associateId
            var email = employeeDistributionData[i].email
            var distributionListIds = employeeDistributionData[i].distributionListIds
            var idsSize = distributionListIds.length
            
            // console.log(associateId, email, distributionListIds)
            // getting the employees HQ data
            var HQEmployeeData = await employee.findByIdHQEmail(associateId)
            var positionId = HQEmployeeData[0].position_id
            var startDate = HQEmployeeData[0].start_date
            var employeeName = HQEmployeeData[0].first_name + " " + HQEmployeeData[0].last_name
            var salaried = "Yes"
            var hourly = ""
            var timeSheetApprover = "No"
            var title = HQEmployeeData[0].position_name
            var department = HQEmployeeData[0].department_name
            var country = "Malawi"
            var office = HQEmployeeData[0].office_name
            
            // get the employee's supervirsor
            var matrixData = await matrix.findByEmployeePosition(positionId)
            var supervirsorPosition = matrixData.matrix.matrix[1].position
            console.log(supervirsorPosition)
            var supervirsorData = await employee.findByEmployeePosition(supervirsorPosition)
            var thisSupervirsor = supervirsorData[0].first_name + " " + supervirsorData[0].last_name

            // check if the employee will approve timesheets
            const employeePosition = await position.find(positionId)
            const positionName = employeePosition[0].position_name
            
            // getting all the matrices
            const results = await matrix.findAll()
            const size = results.length
            var isSupervirsor = false
            // check if this user is a supervisor
            for (var l = 0; l < size; l++) {
                var thisMatrix = results[l].matrix.matrix
                var matrixSize = thisMatrix.length
                // starting from the second position
                for (var m = 1; m < matrixSize; m++) {
                    var thisPosition = thisMatrix[m].position
                    if (positionName == thisPosition) {
                        isSupervirsor = true
                        timeSheetApprover = "Yes"
                        break
                    }
                    // console.log(thisPosition)
                }

                if (isSupervirsor) {
                    break
                }
                
            }


            // the employee distribution list
            var employeeDistributionList = ""

            for (var k = 0; k < idsSize; k++) {
                var distributionListId = distributionListIds[k]

                // deleting the employees old email
                await employeeDistribution.deleteEmployeeDistribution(associateId) 
            
                // inserting the data in the distribution list
                var result = await employeeDistribution.insert(distributionListId, associateId)
                results.push(result)

                // getting the distribution list data
                var distributionListData = await distributionList.find(distributionListId)
                var distributionName = distributionListData[0].group_name 

                // add the distribution list text
                if (k == 0) {
                    employeeDistributionList += distributionName
                } else {
                    employeeDistributionList += ", " + distributionName
                }

            }

            // batching to be sent to the HQ
            var HQBatch = {
                startDate: startDate,
                employeeName: employeeName,
                salaried: salaried,
                hourly: hourly,
                timeSheetApprover: thisSupervirsor,
                email: email,
                title: title,
                department: department,
                country: country,
                associateId: associateId,
                approveOtherTimesheets: timeSheetApprover,
                office: office,
                distributionList: employeeDistributionList
            }

            
            // updating the employee's email
            await employee.updateEmail(email, associateId)

            // add the batch to the HQ email
            HQEmailBatchData.push(HQBatch)
        } 


        // preparing and sending the email to HQ
        var subject = 'EGPAF Malawi New Hire'
        
        var hrEmailContent ="<p>Dear Sir/Madam,</p>\n" +
                            "<p>Please find the attached document of the newly hired EGPAF Malawi employees." + 
                            "<table sytle='font-family: Arial, Helvetica, sans-serif; width: 100%; border: 1px solid #ddd;' >" +
                            "<tr>" +
                                "<th style='font-size: 0.85em; border: 1px solid #ddd; padding: 10px; border-collapse: collapse; '>Start Date</th>" +
                                "<th style='font-size: 0.85em; border: 1px solid #ddd; padding: 10px; border-collapse: collapse; '>Employee Name</th>" +
                                "<th style='font-size: 0.85em; border: 1px solid #ddd; padding: 10px; border-collapse: collapse; '>Full Time Salaried</th>" +
                                "<th style='font-size: 0.85em; border: 1px solid #ddd; padding: 10px; border-collapse: collapse; '>Full Time Hourly</th>" +
                                "<th style='font-size: 0.85em; border: 1px solid #ddd; padding: 10px; border-collapse: collapse; '>Employee Time Sheet Approver</th>" +
                                "<th style='font-size: 0.85em; border: 1px solid #ddd; padding: 10px; border-collapse: collapse; '>E-mail</th>" + 
                                "<th style='font-size: 0.85em; border: 1px solid #ddd; padding: 10px; border-collapse: collapse; '>Title</th>" +
                                "<th style='font-size: 0.85em; border: 1px solid #ddd; padding: 10px; border-collapse: collapse; '>Department</th>" +
                                "<th style='font-size: 0.85em; border: 1px solid #ddd; padding: 10px; border-collapse: collapse; '>Country</th>" +
                                "<th style='font-size: 0.85em; border: 1px solid #ddd; padding: 10px; border-collapse: collapse; '>Associate ID</th>" +
                                "<th style='font-size: 0.85em; border: 1px solid #ddd; padding: 10px; border-collapse: collapse; '>Time Sheet Approver</th>" +
                                "<th style='font-size: 0.85em; border: 1px solid #ddd; padding: 10px; border-collapse: collapse; '>Office City / Province</th>" +
                                "<th style='font-size: 0.85em; border: 1px solid #ddd; padding: 10px; border-collapse: collapse; '>Email Distribution Lists</th>" +       
                            "</tr>";

            // creating the variables for the excel sheet to be attached to the email
            const sheetName = "EGPAF Malawi New Hire"
            const columns = [   { label: "Employee Start Date" , value: "employeeStartDate" },
                                { label: "Employee Name" , value: "theEmployeeName" },
                                { label: "Full Time Salaried" , value: "theSalaried" },
                                { label: "Full Time Hourly" , value: "theHourly" },
                                { label: "Employee Time Sheet Approver" , value: "theTimeSheetApprover" },
                                { label: "E-mail" , value: "theEmail" },
                                { label: "Title" , value: "theTitle" },
                                { label: "Department" , value: "theDepartment" },
                                { label: "Country" , value: "theCountry" },
                                { label: "Associate ID" , value: "theAssociateId" },
                                { label: "Time Sheet Approver" , value: "theApproveOtherTimeSheets" },
                                { label: "Office City / Province" , value: "theOffice" },
                                { label: "Email Distribution Lists" , value: "theDistributionList" }
                            ] 
            var content = []

            var HQEmailBatchSize = HQEmailBatchData.length
            for (var x = 0; x < HQEmailBatchSize; x++) {
                var batch = HQEmailBatchData[x]
                var employeeStartDate = batch.startDate
                var theEmployeeName = batch.employeeName
                var theSalaried = batch.salaried
                var theHourly = batch.hourly
                var theEmail = batch.email
                var theTimeSheetApprover = batch.timeSheetApprover
                var theTitle = batch.title
                var theDepartment = batch.department
                var theCountry = batch.country
                var theAssociateId = batch.associateId
                var theApproveOtherTimeSheets = batch.approveOtherTimesheets
                var theOffice = batch.office
                var theDistributionList = batch.distributionList
                
                // adding the employee content to the excel sheet data
                var employeeContent = {
                                            employeeStartDate: employeeStartDate,
                                            theEmployeeName: theEmployeeName,
                                            theSalaried: theSalaried,
                                            theHourly: theHourly,
                                            theTimeSheetApprover: theTimeSheetApprover,
                                            theEmail: theEmail,
                                            theTitle: theTitle,
                                            theDepartment: theDepartment,
                                            theCountry: theCountry,
                                            theAssociateId: theAssociateId,
                                            theApproveOtherTimeSheets: theApproveOtherTimeSheets,
                                            theOffice: theOffice,
                                            theDistributionList: theDistributionList
                }
                content.push(employeeContent)

                
                hrEmailContent +="<tr>" +
                                    `<td style='font-size: 0.85em; border: 1px solid #ddd; padding: 10px; border-collapse: collapse; '>${employeeStartDate}</td>` +
                                    `<td style='font-size: 0.85em; border: 1px solid #ddd; padding: 10px; border-collapse: collapse; '>${theEmployeeName}</td>` +
                                    `<td style='font-size: 0.85em; border: 1px solid #ddd; padding: 10px; border-collapse: collapse; '>${theSalaried}</td>` +
                                    `<td style='font-size: 0.85em; border: 1px solid #ddd; padding: 10px; border-collapse: collapse; '>${theHourly}</td>` +
                                    `<td style='font-size: 0.85em; border: 1px solid #ddd; padding: 10px; border-collapse: collapse; '>${theTimeSheetApprover}</td>` +
                                    `<td style='font-size: 0.85em; border: 1px solid #ddd; padding: 10px; border-collapse: collapse; '>${theEmail}</td>` + 
                                    `<td style='font-size: 0.85em; border: 1px solid #ddd; padding: 10px; border-collapse: collapse; '>${theTitle}</td>` +
                                    `<td style='font-size: 0.85em; border: 1px solid #ddd; padding: 10px; border-collapse: collapse; '>${theDepartment}</td>` +
                                    `<td style='font-size: 0.85em; border: 1px solid #ddd; padding: 10px; border-collapse: collapse; '>${theCountry}</td>` +
                                    `<td style='font-size: 0.85em; border: 1px solid #ddd; padding: 10px; border-collapse: collapse; '>${theAssociateId}</td>` +
                                    `<td style='font-size: 0.85em; border: 1px solid #ddd; padding: 10px; border-collapse: collapse; '>${theApproveOtherTimeSheets}</td>` +
                                    `<td style='font-size: 0.85em; border: 1px solid #ddd; padding: 10px; border-collapse: collapse; '>${theOffice}</td>` +
                                    `<td style='font-size: 0.85em; border: 1px solid #ddd; padding: 10px; border-collapse: collapse; '>${theDistributionList}</td>` +       
                                "</tr>";
            }


            const path = require('path');
            const DOCUMENT_DIR = path.join("..", "hr-backend", "documents")

            var date = new Date()
            var filename = 'EGPAF-Malawi-New-Hire-' + date.getDate() + '-' + date.getMonth() + '-' + date.getFullYear()
            var filePath = path.join(DOCUMENT_DIR, filename)

        
            var excelFilesettings = {
                fileName: filePath,
                extraLenth: 3,
                writeOptions: {}
            }
            var data = [
                {
                    sheet: sheetName,
                    columns: columns,
                    content: content
                }
            ]
            


            // await xlsx(data, excelFilesettings)
            hrEmailContent += "</table>"
            // await emailModule.sendNewHireHTMLEmail(subject, hrEmailContent, 'rmkuwira@pedaids.org', filename + ".xlsx", filePath + ".xlsx")
            await emailModule.sendHTMLEmail(subject, hrEmailContent, 'rmkuwira@pedaids.org')
        
        // res.json(results)
        res.sendStatus(200)
    }catch(error){
        console.log(error);
        res.sendStatus(500);
    }
});



module.exports = router;