/*
|---------------------------------------|
|Software Engineer: Robin B. Mkuwira    |
|Date Created: 03/02/2021               |
|Project Name: HR Application           |
|                                       |
|The project route is used to access    |
|the methods in the project model       |
|---------------------------------------|
*/

const jwt = require('jsonwebtoken');
const express = require("express");
const router = express.Router();

const project = require("../models/project");

const bodyParser = require("body-parser");
const urlencodedParser = bodyParser.urlencoded({ extended: false});


//fetching all of the projects
router.get("/findAll", async (req, res)=>{
    try{
        let results = await project.findAll();
        res.json(results);
    }catch(error){
        console.log(error);
        res.sendStatus(500);
    }
 });


//fetching a specific project
router.post("/find", urlencodedParser ,async (req, res) => {
    try{
        const id = req.body.id;
        const results = await project.find(id);
        res.json(results);
    }catch(error){
        console.log(error);
        res.sendStatus(500);
    }
});



// inserting a specific project
router.post("/insert", urlencodedParser ,async (req, res) => {
    try{
        const name = req.body.name;

        const results = await project.insert(name);
        res.json(results);
    }catch(error){
        console.log(error);
        res.sendStatus(500);
    }
});






// updating a specific project
router.post("/update", urlencodedParser ,async (req, res) => {
    try{
        const id = req.body.id;
        const name = req.body.name;

        const results = await project.update(id, name);
        res.json(results);
    }catch(error){
        console.log(error);
        res.sendStatus(500);
    }
});



module.exports = router;