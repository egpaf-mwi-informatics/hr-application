/*
|---------------------------------------|
|Software Engineer: Robin B. Mkuwira    |
|Date Created: 26/09/2023               |
|Project Name: HR Application           |
|                                       |
|The anniversary route is used to access|
|the methods in the department model    |
|---------------------------------------|
*/

const express = require("express");
const router = express.Router();
const anniversary = require("../models/anniversary");
const bodyParser = require("body-parser");
const urlencodedParser = bodyParser.urlencoded({ extended: false});




//fetching all of the anniversary 
router.get("/findAll", async (req, res)=>{
    try{
        let results = await anniversary.findAll();
        res.json(results);
    }catch(error){
        console.log(error);
        res.sendStatus(500);
    }
 });


//fetching a specific anniversary
router.post("/find", urlencodedParser ,async (req, res) => {
    try{
        const id = req.body.id;
        const results = await anniversary.find(id);
        res.json(results);
    }catch(error){
        console.log(error);
        res.sendStatus(500);
    }
});


// delete a specific anniversary
router.post("/insert", urlencodedParser ,async (req, res) => {
    try{
        const employeeId = req.body.employeeId;
        const anniversaryTypeId = req.body.anniversaryTypeId;
        
        const results = await anniversary.insert(employeeId, anniversaryTypeId);
        res.json(results);
    }catch(error){
        console.log(error);
        res.sendStatus(500);
    }
});


// update a specific anniversary
router.put("/update", urlencodedParser ,async (req, res) => {
    try{
        const id = req.body.id
        const employeeId = req.body.employeeId;
        const anniversaryTypeId = req.body.anniversaryTypeId;

        const results = await anniversary.update(id, employeeId, anniversaryTypeId)
        res.json(results);
    }catch(error){
        console.log(error);
        res.sendStatus(500);
    }
});


// delete a specific anniversary
router.delete("/delete", urlencodedParser ,async (req, res) => {
    try{
        const id = req.body.id;
        const results = await anniversary.deleteAnniversary(id);
        
        res.json(results);
    }catch(error){
        console.log(error);
        res.sendStatus(500);
    }
});

module.exports = router;