/*
|---------------------------------------|
|Software Engineer: Robin B. MKuwira    |
|Date Created: 03/02/2021               |
|Project Name: HR Application           |
|                                       |
|The contract notifications route is    |
|used to access the methods in the      |
|contract notifications model           |
|---------------------------------------|
*/

const jwt = require('jsonwebtoken');
const express = require("express");
const router = express.Router();
const contractNotification = require("../models/contract_notification");
const bodyParser = require("body-parser");
const urlencodedParser = bodyParser.urlencoded({ extended: false});


//fetching all of the contract notifications
router.get("/findAll", async (req, res)=>{
    try{
        let results = await contractNotification.findAll();
        res.json(results);
    }catch(error){
        console.log(error);
        res.sendStatus(500);
    }
 });


//fetching a specific contract notification
router.post("/find", urlencodedParser ,async (req, res) => {
    try{
        const id = req.body.id;
        const results = await contractNotification.find(id);
        res.json(results);
    }catch(error){
        console.log(error);
        res.sendStatus(500);
    }
});





module.exports = router;